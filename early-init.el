;;; early-init.el --- Custom before init settings -*- lexical-binding: t -*-

;; Author: Aimé Bertrand
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2021-08-11
;; Keywords: emacs early init config
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; `early-init' file to load my settings before Emacs `init'.
;; Most of the appearance and faces settings for the initial Emacs frame.

;;; Code:


;;; CUSTOM GROUP
(defgroup timu ()
  "Custom group for my own libraries."
  :group 'emacs
  :prefix "timu-")

(defcustom timu-libraries-path
  (expand-file-name "libraries" user-emacs-directory)
  "Variable for the path to the directory with custom libraries."
  :type 'directory
  :group 'timu)


;;; SYSTEM INFO
(pcase system-type
  ('windows-nt
   (setenv "HOME" (getenv "OneDrive"))
   (setenv "HOMEPATH" (getenv "OneDrive"))
   (setq default-directory "~/")))


;;; APPEARANCE SETTINGS
(tool-bar-mode -1)
(scroll-bar-mode -1)
(pcase system-type
  ('gnu/linux (menu-bar-mode -1))
  ('windows-nt (menu-bar-mode -1)))

;; frame measurements (emacs-plus or emacsmacport)
(pcase system-type
  ('darwin
   (if (custom-variable-p 'mac-effective-appearance-change-hook)
       (customize-set-variable
        'initial-frame-alist
        '((top . 1) (left . 120) (width . 180) (height . 50)))
     (customize-set-variable
      'initial-frame-alist
      '((top . 0) (left . 0.4) (width . 0.6) (height . 0.6))))))

;; transparency
(set-frame-parameter (selected-frame) 'alpha '(100 . 100))

;; don't show default Emacs info
(customize-set-variable 'inhibit-startup-message t)

;; make frame resize exactly till the edges
(customize-set-variable 'frame-resize-pixelwise t)

;; frame title (emacs-plus or emacsmacport)
(pcase system-type
  ('darwin
   (if (custom-variable-p 'mac-effective-appearance-change-hook)
       (setq frame-title-format nil)
     (progn
       (setq frame-title-format nil)
       (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))))))


;;; NATIVE COMPILATION
(setq native-comp-async-report-warnings-errors 'silent)


;;; early-init.el ends here
