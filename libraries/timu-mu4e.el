;;; timu-mu4e.el --- Mu4e configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-02-14
;; Keywords: email tools helper mu4e
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my email config with mu4e.

;;; Code:


(require 'epa-file)
(require 'mu4e)
(require 'mu4e-thread-folding)


;;; DEFAULTS
(defgroup timu-mu4e ()
  "Customise group for the `timu-mu4e' Library."
  :group 'timu)

(defcustom timu-mu4e-path
  (expand-file-name "libraries/timu-mu4e.el" user-emacs-directory)
  "Variable for the path of the module `timu-mu4e'."
  :type 'file
  :group 'timu-mu4e)


;;; FUNCTIONS
(defun timu-mu4e-in-new-tab ()
  "Open `mu4e' in a new tab with `tab-bar-new-tab'."
  (interactive)
  (progn
    (tab-bar-new-tab)
    (mu4e)))

(defun timu-mu4e-quit ()
  "Quit `mu4e' and close the tab with `tab-bar-close-tab'."
  (interactive)
  (progn
    (mu4e-quit)
    (tab-bar-close-tab)))

(defun timu-mu4e-execute-no-confirm ()
  "Execute all without confirmation.
Use the argument NO-COMFIRM in the command `mu4e-mark-execute-all'."
  (interactive)
  (mu4e-mark-execute-all 'no-confirm))

(defun timu-mu4e-jump-to-maildir ()
  "Use completion to jump to a maildir."
  (interactive)
  (mu4e-search-maildir (mu4e-ask-maildir "Maildir: ")))

(defun timu-mu4e-attach-file ()
  "Attach a file to an email.
Use the built-in function `mml-attach-file'."
  (interactive)
  (let ((default-directory "~/"))
    (let ((file (read-file-name "Select a file to attach: ")))
      (mml-attach-file (expand-file-name file)))))

(defun timu-mu4e-view-just-save-all-attachments ()
  "Save files from the current Mu4e view buffer.
This applies to all MIME-parts that are \"attachment-like\" (have a filename),
regardless of their disposition.

This is a modified version of `mu4e-view-save-attachments'.
It does not use `mu4e--completing-read' to select files, but just selects all.

Also it always prompts for the directory to save to."
  (interactive)
  (let* ((parts (mu4e-view-mime-parts))
         (candidates  (seq-map
                       (lambda (fpart)
                         (let ((fname (plist-get fpart :filename)))
                           (when (and crm-separator (string-match-p crm-separator fname))
                             (mu4e-warn (concat  "File(s) match `crm-separator'; "
                                                 "use mu4e-view-mime-part-action instead")))
                           ;; (filename . annotation)
                           (cons fname fpart)))
                       (seq-filter
                        (lambda (part) (plist-get part :attachment-like))
                        parts)))
         (candidates (or candidates
                         (mu4e-warn "No attachments for this message")))
         (files (mapcar #'car candidates))
         (default-directory mu4e-attachment-dir)
         (custom-dir (read-directory-name
                                    "Save to directory: ")))
    ;; we have determined what files to save, and where.
    (seq-do (lambda (fname)
              (let* ((part (cdr (assoc fname candidates)))
                     (path (funcall mu4e-uniquify-save-file-name-function
                            (mu4e-join-paths
                             (or custom-dir (plist-get part :target-dir))
                             (plist-get part :filename))))
                     (handle (plist-get part :handle)))
                (when handle ;; completion may fail, and then there's no handle.
                  (mm-save-part-to-file handle path))))
            files)))

(defun timu-mu4e-get-mail ()
  "Select the Account before syncing.
This makes the syncing of mails more flexible."
  (interactive)
  (let ((mu4e-get-mail-command
         (concat
          "/opt/homebrew/bin/mbsync "
          (completing-read
           "Which Account: "
           '("icloud" "aimebertrand" "moclub" "--all")))))
    (mu4e-update-mail-and-index t)))

(defun timu-mu4e-message-insert-signature (&optional force)
  "Insert a signature at the end of the buffer.

Original command is `message-insert-signature'.
See https://macowners.club/posts/signature-above-cited-text-mu4e/ for reasons.

See the documentation for the `message-signature' variable for
more information.

If FORCE is 0 (or when called interactively), the global values
of the signature variables will be consulted if the local ones
are null."
  (interactive (list 0) message-mode)
  (let ((timu-message-signature timu-message-signature)
        (message-signature-file message-signature-file))
    ;; If called interactively and there's no signature to insert,
    ;; consult the global values to see whether there's anything they
    ;; have to say for themselves.  This can happen when using
    ;; `gnus-posting-styles', for instance.
    (when (and (null timu-message-signature)
               (null message-signature-file)
               (eq force 0))
      (setq timu-message-signature (default-value 'timu-message-signature)
            message-signature-file (default-value 'message-signature-file)))
    (let* ((signature
            (cond
             ((and (null timu-message-signature)
                   (eq force 0))
              (save-excursion
                (goto-char (point-max))
                (not (re-search-backward message-signature-separator nil t))))
             ((and (null timu-message-signature)
                   force)
              t)
             ((functionp timu-message-signature)
              (funcall timu-message-signature))
             ((listp timu-message-signature)
              (eval timu-message-signature t))
             (t timu-message-signature)))
           signature-file)
      (setq signature
            (cond ((stringp signature)
                   signature)
                  ((and (eq t signature) message-signature-file)
                   (setq signature-file
                         (if (and message-signature-directory
                                  ;; don't actually use the signature directory
                                  ;; if message-signature-file contains a path.
                                  (not (file-name-directory
                                        message-signature-file)))
                             (expand-file-name message-signature-file
                                               message-signature-directory)
                           message-signature-file))
                   (file-exists-p signature-file))))
      (when signature
        (goto-char (point-max))
        ;; Insert the signature.
        (unless (bolp)
          (newline))
        (when message-signature-insert-empty-line
          (newline))
        (insert "...... ")
        (newline)
        (if (eq signature t)
            (insert-file-contents signature-file)
          (insert signature))
        (goto-char (point-max))
        (or (bolp) (newline))))))

(defun timu-mu4e-message-insert-signature-at-point (pmode)
  "Function to insert signature at right point according to PMODE.
Uses `timu-mu4e-message-insert-signature'.
This is a modified version of `message-insert-signature'."
  (when pmode (message-goto-body))
  (interactive)
  (require 'message)
  (message-goto-body)
  (newline)
  (message-goto-body)
  (save-restriction
    (narrow-to-region (point) (point))
    (timu-mu4e-message-insert-signature))
  (message-goto-body))

(defun timu-get-mu4e-context ()
  "Extract context from `mu4e--search-last-query'."
  (if (string-match "/\\(.+?\\)/.*" mu4e--search-last-query)
      (match-string 1 mu4e--search-last-query) ""))

(defun timu-mu4e-switch-context ()
  "Switch context of the current maildir.
Uses `mu4e--search-last-query' and regex to get the context."
  (let ((new-context
         (timu-get-mu4e-context)))
    (if new-context
        (mu4e-context-switch t new-context)
      (mu4e-context-switch t "icloud"))))

(defun timu-mu4e-msmtp-select-account ()
  "Select the right account/context according to the from line."
  (if (message-mail-p)
      (save-excursion
        (let*
            ((from (save-restriction
                     (message-narrow-to-headers)
                     (message-fetch-field "from")))
             (account
              (cond
               ((string-match timu-personal-icloud-email from) "icloud")
               ((string-match timu-personal-aimebertrand-email from) "aimebertrand")
               ((string-match timu-personal-moclub-email from) "moclub"))))
          (setq message-sendmail-extra-arguments (list '"-a" account))))))


;;; MU4E CONFIGURATION
;;;; variables
(if timu-base-wsl-p
    (customize-set-variable 'mu4e-get-mail-command (concat (executable-find "mbsync") " work"))
  (customize-set-variable 'mu4e-get-mail-command
                          (concat (executable-find "mbsync") timu-personal-mbsync)))

(customize-set-variable 'mu4e-mu-binary (executable-find "mu"))
(customize-set-variable 'mu4e-attachment-dir (expand-file-name "~/tmp/Attachements/"))
(customize-set-variable 'mu4e-change-filenames-when-moving t) ; NEEDED FOR MBSYNC
(customize-set-variable 'mu4e-update-interval 120)
(customize-set-variable 'mu4e-view-show-addresses 't)
(customize-set-variable 'mu4e-confirm-quit nil)
(customize-set-variable 'mu4e-headers-visible-lines 20)
(customize-set-variable 'mu4e-hide-index-messages t)
(customize-set-variable 'message-citation-line-format "%N @ %Y-%m-%d %H:%M :\n")
(customize-set-variable 'message-citation-line-function 'message-insert-formatted-citation-line)
(customize-set-variable 'mu4e-headers-include-related nil)
(customize-set-variable 'mail-user-agent #'mu4e-user-agent)
(customize-set-variable 'message-mail-user-agent t)
(customize-set-variable 'gnus-dired-mail-mode #'mu4e-user-agent) ;; send files from dired
(customize-set-variable 'mu4e-completing-read-function #'completing-read)
(customize-set-variable 'mu4e-save-multiple-attachments-without-asking t)

(setq mu4e-headers-show-threads nil)
(setq org-mu4e-link-query-in-headers-mode nil)

(setq-default mu4e-update-minor-mode t)

;;;; buffers
(add-to-list 'display-buffer-alist
             '("\\*mu4e-update\\*"
               (display-buffer-below-selected)
               (window-height . 0.1)))

;;;; hooks & advices
(advice-add 'mu4e-mark-unmark-all :around #'timu-base-disable-yes-or-no-p)

;;;; move mail to trash but do not flag "trash":
;; Credit: https://github.com/djcb/mu/issues/1136
(setf (plist-get (alist-get 'trash mu4e-marks) :action)
      (lambda (docid msg target)
        (mu4e--server-move docid (mu4e--mark-check-target target) "+S-u-N")))

;;;; header view formatting
(setq mu4e-headers-thread-single-orphan-prefix '("─>" . "─▶"))
(setq mu4e-headers-thread-orphan-prefix '("┬>" . "┬▶ "))
(setq mu4e-headers-thread-connection-prefix '("│ " . "│ "))
(setq mu4e-headers-thread-first-child-prefix '("├>" . "├▶"))
(setq mu4e-headers-thread-child-prefix '("├>" . "├▶"))
(setq mu4e-headers-thread-last-child-prefix '("└>" . "╰▶"))


;;; MU4E BOOKMARKS
(customize-set-variable
 'mu4e-bookmarks
 '((:name "iCloud - Unread"
          :query "maildir:/icloud/INBOX flag:unread"
          :hide t
          :key ?I)
   (:name "iCloud"
          :query "maildir:/icloud/INBOX"
          :key ?i)
   (:name "Aimé Bertrand - Unread"
          :query "maildir:/aimebertrand/INBOX flag:unread"
          :hide t
          :key ?A)
   (:name "Aimé Bertrand"
          :query "maildir:/aimebertrand/INBOX"
          :key ?a)
   (:name "MoClub - Unread"
          :query "maildir:/moclub/INBOX flag:unread"
          :hide t
          :key ?M)
   (:name "MoClub"
          :query "maildir:/moclub/INBOX"
          :key ?m)
   (:name "Spam - All"
          :query "maildir:/icloud/Junk or maildir:/aimebertrand/Spam or maildir:/moclub/Spam"
          :key ?s)
   (:name "Trash - All"
          :query "maildir:\"/icloud/Deleted Messages\" or maildir:/aimebertrand/Trash or maildir:/moclub/Trash"
          :key ?b)
   (:name "Unread messages"
          :query "flag:unread AND NOT flag:trashed"
          :key ?u)
   (:name "Today's messages" :query "date:today..now" :key ?d)
   (:name "Last 7 days" :query "date:7d..now" :hide-unread t :key ?w)
   (:name "Messages with images" :query "mime:image/*" :key ?p)))


;;; MU4E HEADERS
(customize-set-variable 'mu4e-headers-fields
                        '((:flags . 6)
                          (:date . 25)
                          (:from . 40)
                          (:subject . nil)))

(customize-set-variable 'mu4e-headers-date-format "%Y-%m-%d %H:%M")

;;;; mu4e headers and view actions
(add-to-list 'mu4e-headers-actions
             '("in Browser" . mu4e-action-view-in-browser) t)
(add-to-list 'mu4e-view-actions
             '("in Browser" . mu4e-action-view-in-browser) t)


;;; MU4E CONTEXT
(setq mu4e-contexts
      `(,(make-mu4e-context
          :name "icloud"
          :enter-func
          (lambda () (mu4e-message (concat "Enter " timu-personal-icloud-email " context")))
          :leave-func
          (lambda () (mu4e-message (concat "Leave " timu-personal-icloud-email " context")))
          :match-func
          (lambda (msg)
            (when msg
              (mu4e-message-contact-field-matches msg
                                                  :to timu-personal-icloud-email)))
          :vars `((user-mail-address . ,timu-personal-icloud-email)
                  (user-full-name . ,timu-personal-long-name)
                  (timu-message-signature . ,timu-personal-icloud-sign)
                  (mu4e-drafts-folder . "/icloud/Drafts")
                  (mu4e-refile-folder . "/icloud/Archive")
                  (mu4e-sent-folder . "/icloud/Sent Messages")
                  (mu4e-trash-folder . "/icloud/Deleted Messages")))

        ,(make-mu4e-context
          :name "aimebertrand"
          :enter-func
          (lambda () (mu4e-message (concat "Enter " timu-personal-aimebertrand-email " context")))
          :leave-func
          (lambda () (mu4e-message (concat "Leave " timu-personal-aimebertrand-email " context")))
          :match-func
          (lambda (msg)
            (when msg
              (mu4e-message-contact-field-matches msg
                                                  :to timu-personal-aimebertrand-email)))
          :vars `((user-mail-address . ,timu-personal-aimebertrand-email)
                  (user-full-name . ,timu-personal-long-name)
                  (timu-message-signature . ,timu-personal-aimebertrand-sign)
                  (mu4e-drafts-folder . "/aimebertrand/Drafts")
                  (mu4e-refile-folder . "/aimebertrand/Archive")
                  (mu4e-sent-folder . "/aimebertrand/Sent")
                  (mu4e-trash-folder . "/aimebertrand/Trash")))

        ,(make-mu4e-context
          :name "moclub"
          :enter-func
          (lambda () (mu4e-message (concat "Enter " timu-personal-moclub-email " context")))
          :leave-func
          (lambda () (mu4e-message (concat "Leave " timu-personal-moclub-email " context")))
          :match-func
          (lambda (msg)
            (when msg
              (mu4e-message-contact-field-matches msg
                                                  :to timu-personal-moclub-email)))
          :vars `((user-mail-address . ,timu-personal-moclub-email)
                  (user-full-name . ,timu-personal-short-name)
                  (timu-message-signature . ,timu-personal-moclub-sign)
                  (mu4e-drafts-folder . "/moclub/Drafts")
                  (mu4e-refile-folder . "/moclub/Archive")
                  (mu4e-sent-folder . "/moclub/Sent")
                  (mu4e-trash-folder . "/moclub/Trash")))))

;; start with the first (default) context:
(customize-set-variable 'mu4e-context-policy 'pick-first)
;; ask for context if no context matches:
(customize-set-variable 'mu4e-compose-context-policy 'ask)


;;; MU4E SENDING
;; gpg encryptiom & decryption:
(epa-file-enable)
(customize-set-variable 'epa-pinentry-mode 'ask)
;; clear auth cache
(auth-source-forget-all-cached)
;; don't keep message buffers around:
(customize-set-variable 'message-kill-buffer-on-exit t)
;; send function:
(customize-set-variable 'send-mail-function 'message-send-mail-with-sendmail)
(customize-set-variable 'message-send-mail-function 'message-send-mail-with-sendmail)
;; send program:
(customize-set-variable 'sendmail-program (executable-find "msmtp"))
(customize-set-variable 'message-sendmail-envelope-from 'header)
;; chose from account before sending
(add-hook 'message-send-mail-hook #'timu-mu4e-msmtp-select-account)

(add-hook 'mu4e-compose-mode-hook #'corfu-mode)

(add-hook 'mu4e-compose-mode-hook
          (lambda ()
            (flyspell-mode)))

(add-hook 'mu4e-compose-mode-hook
          (lambda ()
            (save-excursion (message-add-header "Cc:\n"))
            (save-excursion (message-add-header "Bcc:\n"))))

(add-hook 'mu4e-headers-found-hook #'timu-mu4e-switch-context)


;;; MU4E SIGNATURE SETTINGS
(customize-set-variable 'message-signature nil)

;; add them to the hooks with appropriate input arguments
(add-hook 'mu4e-compose-mode-hook
          (lambda () (timu-mu4e-message-insert-signature-at-point t)) t)


;;; MU4E-COLUMN-FACES
(mu4e-column-faces-mode)


;;; MU4E-THREAD-FOLDING
(quelpa '(mu4e-thread-folding
          :fetcher git
          :url "https://github.com/rougier/mu4e-thread-folding"))

(customize-set-variable 'mu4e-thread-folding-root-unfolded-prefix-string "%2d ▼")
(customize-set-variable 'mu4e-thread-folding-root-folded-prefix-string "%2d ►")
(customize-set-variable 'mu4e-thread-folding-child-prefix-string "-")
(mu4e-thread-folding-mode 1)


;;; MU4E KEYBINDINGS
(keymap-set mu4e-headers-mode-map "M-r" #'timu-mu4e-get-mail)
(keymap-set mu4e-main-mode-map "M-r" #'timu-mu4e-get-mail)
(keymap-set mu4e-view-mode-map "M-r" #'timu-mu4e-get-mail)
(keymap-set mu4e-minibuffer-search-query-map "M-." #'abort-recursive-edit)

(evil-define-key 'normal mu4e-main-mode-map
  (kbd "q") #'timu-mu4e-quit
  (kbd "J") #'timu-mu4e-jump-to-maildir)
(evil-define-key 'normal mu4e-headers-mode-map
  (kbd "J") #'timu-mu4e-jump-to-maildir
  (kbd "x") #'timu-mu4e-execute-no-confirm)
(evil-define-key 'normal mu4e-search-minor-mode-map
  (kbd "J") #'timu-mu4e-jump-to-maildir)
(evil-define-key 'normal mu4e-view-mode-map
  (kbd "J") #'timu-mu4e-jump-to-maildir
  (kbd "x") #'timu-mu4e-execute-no-confirm)

(evil-define-key '(normal visual) mu4e-main-mode-map
  (kbd "j") #'mu4e-search-bookmark)

(evil-define-key '(normal visual) mu4e-compose-mode-map
  (kbd "gg") #'message-goto-body
  (kbd "G") #'message-goto-signature)

(keymap-set mu4e-compose-mode-map "M-s" #'message-send-and-exit)
(keymap-set mu4e-compose-mode-map "M-w" #'message-kill-buffer)
(keymap-set mu4e-compose-mode-map "M-a" #'timu-mu4e-attach-file)
(keymap-set message-mode-map "M-s" #'message-send-and-exit)
(keymap-set message-mode-map "M-w" #'message-kill-buffer)
(keymap-set message-mode-map "M-a" #'timu-mu4e-attach-file)

(evil-define-key 'normal mu4e-headers-mode-map
  (kbd "E") #'mu4e-compose-edit
  (kbd "F") #'mu4e-compose-forward
  (kbd "R") #'mu4e-compose-reply)

(evil-define-key 'normal mu4e-view-mode-map
  (kbd "E") #'mu4e-compose-edit
  (kbd "F") #'mu4e-compose-forward
  (kbd "R") #'mu4e-compose-reply)

(evil-define-key '(normal visual) mu4e-view-mode-map
  (kbd "ß") #'timu-mu4e-view-save-attachment
  (kbd "s") #'timu-mu4e-view-just-save-all-attachments
  (kbd "q") #'mu4e-view-quit)

;; I want to use my Meta Key (on macOS the CMD Key to move around windows).
;; This means rebinding few bindings in mu4e though.
(keymap-set mu4e-main-mode-map "M-<right>" nil)
(keymap-set mu4e-main-mode-map "M-<left>" nil)

(keymap-set mu4e-main-mode-map "M-<right>" #'windmove-right)
(keymap-set mu4e-main-mode-map "M-<left>" #'windmove-left)

(keymap-set mu4e-search-minor-mode-map "M-<right>" #'windmove-right)
(keymap-set mu4e-search-minor-mode-map "M-<left>" #'windmove-left)

(keymap-set mu4e-headers-mode-map "M-<down>" nil)
(keymap-set mu4e-headers-mode-map "M-<up>" nil)
(keymap-set mu4e-headers-mode-map "M-<right>" nil)
(keymap-set mu4e-headers-mode-map "M-<left>" nil)

(keymap-set mu4e-headers-mode-map "C-<down>" #'mu4e-headers-next)
(keymap-set mu4e-headers-mode-map "C-<up>" #'mu4e-headers-prev)
(keymap-set mu4e-headers-mode-map "C-<right>" #'mu4e-headers-query-next)
(keymap-set mu4e-headers-mode-map "C-<left>" #'mu4e-headers-query-prev)

(keymap-set mu4e-view-mode-map "M-<down>" nil)
(keymap-set mu4e-view-mode-map "M-<up>" nil)
(keymap-set mu4e-view-mode-map "M-<right>" nil)
(keymap-set mu4e-view-mode-map "M-<left>" nil)

(keymap-set mu4e-view-mode-map "C-<down>" #'mu4e-view-headers-next)
(keymap-set mu4e-view-mode-map "C-<up>" #'mu4e-view-headers-prev)
(keymap-set mu4e-view-mode-map "C-<right>" #'mu4e-headers-query-next)
(keymap-set mu4e-view-mode-map "C-<left>" #'mu4e-headers-query-prev)

;;;; keybindings for thread folding
(keymap-set mu4e-headers-mode-map "<tab>" #'mu4e-headers-toggle-at-point)
(keymap-set mu4e-headers-mode-map "S-<left>" #'mu4e-headers-fold-all)
(keymap-set mu4e-headers-mode-map "S-<right>" #'mu4e-headers-unfold-all)


(provide 'timu-mu4e)

;;; timu-mu4e.el ends here
