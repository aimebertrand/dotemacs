;;; timu-roam.el --- Org-roam mode configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-12-24
;; Keywords: org-mode org-roam org tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my org mode configuration.

;;; Code:


(require 'org-roam)


;;; DEFAULTS
(defgroup timu-roam ()
  "Customise group for the `timu-roam' Library."
  :group 'timu)

(defcustom timu-roam-path
  (expand-file-name "libraries/timu-roam.el" user-emacs-directory)
  "Variable for the path of the module `timu-roam'."
  :type 'file
  :group 'timu-roam)


;;; FUNCTIONS
(defun timu-roam-filter-by-tag (tag-name)
  "Find node with certain a TAG-NAME."
  (lambda (node)
    (member tag-name (org-roam-node-tags node))))

(defun timu-roam-find-by-tag ()
  "Find an `org-roam' article with selected tag using `org-roam-tag-completions'.
Create the article if it does not exist.
Reverse engineered from:
https://systemcrafters.net/build-a-second-brain-in-emacs/5-org-roam-hacks."
  (interactive)
  (let ((selected-tag (completing-read
                       "Select the tag: "
                       (org-roam-tag-completions))))
    (org-roam-node-find nil nil
                        (timu-roam-filter-by-tag selected-tag))))


;;; CONFIG
(customize-set-variable 'org-roam-directory (file-truename "~/roam/"))
(customize-set-variable 'org-roam-db-location
                        (file-truename "~/.config/org-roam.db"))
(customize-set-variable 'org-roam-dailies-directory "journal/")
(customize-set-variable 'org-roam-dailies-capture-templates
                        '(("d" "default" entry "* %<%H:%M>: %?"
                           :if-new (file+head "%<%Y-%m-%d>-journal.org"
                                              "#+TITLE: %<%Y-%m-%d> Journal\n"))))

(cl-defmethod org-roam-node-backlinkscount ((node org-roam-node))
  "Showing backlink count of the NODE in the completion."
  (let* ((count (caar (org-roam-db-query
                       [:select (funcall count source)
                                :from links
                                :where (= dest $s1)
                                :and (= type "id")]
                       (org-roam-node-id node)))))
    (format "[%d]" count)))

(customize-set-variable 'org-roam-node-display-template "${title:100} ${tags:50} ${backlinkscount:6}")

(add-to-list 'display-buffer-alist
             '("\\*org-roam\\*"
               (display-buffer-in-direction)
               (direction . right)
               (window-width . 0.33)
               (window-height . fit-window-to-buffer)))

;; activate org-roam-db-autosync-mode
(org-roam-db-autosync-enable)


;;; KEYBINDINGS
(keymap-global-set "C-M-c" #'org-roam-capture)
(keymap-global-set "C-M-f" #'timu-roam-find-by-tag)
(keymap-global-set "C-M-i" #'org-roam-node-insert)
(keymap-global-set "C-M-o" #'org-roam-node-find)
(keymap-global-set "C-M-r" #'org-roam-buffer-toggle)


(provide 'timu-roam)

;;; timu-roam.el ends here
