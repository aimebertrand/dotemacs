;;; timu-git.el --- Configuration for magit & git in general -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: git magit tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my config for git and magit.

;;; Code:


(require 'forge)
(require 'magit)
(require 'with-editor)


;;; DEFAULTS
(defgroup timu-git ()
  "Customise group for the `timu-git' Library."
  :group 'timu)

(defcustom timu-git-path
  (expand-file-name "libraries/timu-git.el" user-emacs-directory)
  "Variable for the path of the module `timu-git'."
  :type 'file
  :group 'timu-git)


;; FUNCTIONS
(defun timu-git-insert-silly-commit-message ()
  "Insert a silly commit message from `whatthecommit.com'."
  (interactive)
  (insert (shell-command-to-string
           "echo -n $(curl -s https://whatthecommit.com/index.txt)")))

(defun timu-git-insert-org-files-commit ()
  "Insert my custom commit message for my org files.
This contents the hostname and the current date in iso format."
  (interactive)
  (insert
   (format "ORG Files - State %s %s"
           (format-time-string "%F")
           (string-trim (shell-command-to-string "hostname")))))


;;; MAGIT
(customize-set-variable 'git-commit-summary-max-length 50)

(add-to-list 'display-buffer-alist
             '("^magit: *"
               (display-buffer-below-selected)
               (window-height . 0.7)))

(add-to-list 'display-buffer-alist
             '("\\(^magit-diff: *\\|^magit-process: *\\|^magit-revision: *\\)"
               (display-buffer-below-selected)
               (window-height . 0.4)))

(add-to-list 'display-buffer-alist
             '(".*new-comment.*"
               (display-buffer-below-selected)
               (window-height . 0.4)))

;; enter commit mode in evil insert state
(add-hook 'git-commit-mode-hook #'evil-insert-state)
;; (add-hook 'git-commit-mode-hook #'git-commit-turn-on-flyspell)


;;; EDIFF
(customize-set-variable 'ediff-split-window-function #'split-window-horizontally)
(customize-set-variable 'ediff-window-setup-function #'ediff-setup-windows-plain)
(customize-set-variable 'ediff-diff-options "-w")
(customize-set-variable 'ediff-use-long-help-message t)


;;; SMERGE
(transient-define-prefix timu-git-smerge-transient ()
  "Transient for SMERGE MODE."
  ["SMERGE MANUE\n"
   ["ACTIONS"
    ("r" "Smerge resolve" smerge-resolve)
    ("a" "Smerge keep-all" smerge-keep-all)
    ("b" "Smerge keep-base" smerge-keep-base)
    ("l" "Smerge keep-lower" smerge-keep-lower)
    ("u" "Smerge keep-upper" smerge-keep-upper)
    ("E" "Smerge ediff" smerge-ediff)]
   ["NAVIGATION"
    ("n" "Smerge next" smerge-next)
    ("p" "Smerge prev" smerge-prev)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set smerge-mode-map "M-#" #'timu-git-smerge-transient))
  ('windows-nt
   (keymap-set smerge-mode-map "M-$" #'timu-git-smerge-transient)))


;;; FORGE
(customize-set-variable
 'forge-database-file
 (expand-file-name "local/forge-database.sqlite" user-emacs-directory))
(customize-set-variable 'forge-bug-reference-hooks nil)

(add-to-list 'display-buffer-alist
             '("^\\*forge: *"
               (display-buffer-same-window)))

(keymap-set forge-post-mode-map "M-s" #'forge-post-submit)
(keymap-set forge-post-mode-map "M-w" #'forge-post-cancel)
(keymap-set forge-topic-mode-map "M-+" #'forge-create-post)
(keymap-set forge-post-section-map "RET" #'forge-edit-post)
(keymap-set forge-topic-state-section-map "RET" #'forge-edit-topic-state)
(keymap-set forge-topic-milestone-section-map "RET" #'forge-edit-topic-milestone)
(keymap-set forge-topic-labels-section-map "RET" #'forge-edit-topic-labels)
(keymap-set forge-topic-marks-section-map "RET" #'forge-edit-topic-marks)
(keymap-set forge-topic-assignees-section-map "RET" #'forge-edit-topic-assignees)


;;; DIFF-HL
(global-diff-hl-mode)
(diff-hl-dired-mode)


;;; GIT-MODES
(add-to-list 'auto-mode-alist '("\\.gitattributes\\'" . gitattributes-mode))
(add-to-list 'auto-mode-alist '("\\gitattributes\\'" . gitattributes-mode))

(add-to-list 'auto-mode-alist '("\\.gitconfig\\'" . gitconfig-mode))
(add-to-list 'auto-mode-alist '("\\gitconfig\\'" . gitconfig-mode))

(add-to-list 'auto-mode-alist '("\\.gitignore\\'" . gitignore-mode))
(add-to-list 'auto-mode-alist '("\\gitignore\\'" . gitignore-mode))


;;; WITH-EDITOR KEYBINDINGS
(keymap-set magit-section-mode-map "<normal-state> M-0" #'delete-window)
(keymap-set magit-section-mode-map "<normal-state> M-1" #'delete-other-windows)
(keymap-set magit-section-mode-map "<normal-state> M-2" #'ace-delete-window)
(keymap-set with-editor-mode-map "M-i" #'timu-git-insert-silly-commit-message)
(keymap-set with-editor-mode-map "M-s" #'with-editor-finish)
(keymap-set with-editor-mode-map "M-w" #'with-editor-cancel)


(provide 'timu-git)

;;; timu-git.el ends here
