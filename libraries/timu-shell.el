;;; timu-shell.el --- Config for many shells -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper shell
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for several Emacs shells.

;;; Code:


(require 'term)
(pcase system-type
  ((or'darwin 'gnu/linux)
   (require 'vterm)))


;;; DEFAULTS
(defgroup timu-shell ()
  "Customise group for the `timu-shell' Library."
  :group 'timu)

(defcustom timu-shell-path
  (expand-file-name "libraries/timu-shell.el" user-emacs-directory)
  "Variable for the path of the module `timu-shell'."
  :type 'file
  :group 'timu-shell)


;;; FUNCTIONS
(defun timu-shell-eshell-handle-ansi-color ()
  "Apply ansi colors to the shell output."
  (ansi-color-apply-on-region eshell-last-output-start
                              eshell-last-output-end))

(defun timu-shell-custom-eshell-prompt ()
  "Get prompt elements and apply face properties.
Uses the helper function`timu-ui-with-face'."
  (let* ((git-branch-unparsed
          (shell-command-to-string
           "git rev-parse --abbrev-ref HEAD 2>/dev/null")) ;; get git branch.
         (git-branch
          (if (string= git-branch-unparsed "") ""
            (substring git-branch-unparsed 0 -1)))) ;; remove trailing newline.
    (concat (unless (string= git-branch "")
              (timu-ui-with-face
               (concat git-branch " ")
               :inherit font-lock-constant-face)) ;; git branch.
            (timu-ui-with-face
             (concat (shell-command-to-string "printf $(hostname)") ":"
                     (car (last (split-string (eshell/pwd) "/"))) " ")
             :inherit font-lock-builtin-face) ;; directory.
            ;; Prompt.
            ;; NOTE: Need to keep " $" for the next/previous prompt regexp to work.
            (timu-ui-with-face "$" :inherit font-lock-preprocessor-face) " ")))

(defun timu-shell-eshell-ls-file-at-point ()
  "Get the full path of the Eshell listing at point."
  (get-text-property (point) 'file-name))

(defun timu-shell-eshell-ls-find-file ()
  "Open the Eshell listing at point."
  (interactive)
  (timu-dired-open-in-external-app (timu-shell-eshell-ls-file-at-point)))

(defun timu-shell-eshell-ls-delete-file ()
  "Delete the Eshell listing at point."
  (interactive)
  (let ((file (timu-shell-eshell-ls-file-at-point)))
    (when (yes-or-no-p (format "Delete file %s?" file))
      (delete-file file 'trash))))

(defun timu-shell-eshell-better-ls (file)
  "Add features to listings in `eshell/ls' output.

1. Make each listing into a clickable link to open the
   corresponding file or directory.
2. Add icons (requires `all-the-icons')
   This function is meant to be used as advice around
   `eshell-ls-annotate', where FILE is the cons describing the file.

Credit: https://github.com/Lambda-Emacs/lambda-emacs."
  (let* ((name (car file))
         (icon (if (eq (cadr file) t)
                   (all-the-icons-icon-for-dir name)
                 (all-the-icons-icon-for-file name))))
    (cons
     (concat " " icon " "
             (propertize name
                         'keymap timu-shell-eshell-ls-file-keymap
                         'mouse-face 'highlight
                         'file-name (expand-file-name
                                     (substring-no-properties (car file))
                                     default-directory))) (cdr file))))

(defun timu-send-to-vterm-right (arg)
  "Send either the current selection, line, or paragraph to Vterm.
ARG determines the behavior:
- No prefix argument: Send the current selection.
                      If there is no selection, send the current line.
-Prefix argument 1: Send the current paragraph."
  (interactive "P")
  (let ((start nil) (end nil)
        (original-window (selected-window))
        (original-point (point))
        (vterm-window nil))
    (cond ((use-region-p)
           (setq start (region-beginning)
                 end (region-end)))
          ((equal arg '(4))
           (setq start (save-excursion (backward-paragraph) (point))
                 end (save-excursion (forward-paragraph) (point))))
          (t (setq start (line-beginning-position)
                   end (line-end-position))))
    (kill-ring-save start end)
    (dolist (win (window-list))
      (with-current-buffer (window-buffer win)
        (when (equal major-mode 'vterm-mode)
          (setq vterm-window win))))
    (if vterm-window (select-window vterm-window)
      (setq vterm-window (split-window-right))
      (select-window vterm-window)
      (multi-vterm))
    (sleep-for 0.5)
    (vterm-yank)
    (vterm-send-return)
    (select-window original-window)
    (goto-char original-point)))


;;; GENERAL
(add-to-list 'display-buffer-alist
             '("\\*Async Shell Command\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.30)))


;;; ESHELL
(customize-set-variable
 'eshell-aliases-file
 (expand-file-name "local/eshell-aliases" user-emacs-directory))

(customize-set-variable
 'eshell-directory-name
 (expand-file-name "local/eshell/" user-emacs-directory))

(customize-set-variable 'eshell-prompt-function 'timu-shell-custom-eshell-prompt)

(add-to-list 'eshell-output-filter-functions 'timu-shell-eshell-handle-ansi-color)

(add-to-list 'display-buffer-alist
             '("\\*eshell\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.40)))

(keymap-set eshell-command-mode-map "M-." #'abort-recursive-edit)

;;;; nice icons for eshell
(advice-add #'eshell-ls-annotate :filter-return #'timu-shell-eshell-better-ls)

(defvar timu-shell-eshell-ls-file-keymap
  (let ((map (make-sparse-keymap)))
    (keymap-set map "RET" #'timu-shell-eshell-ls-find-file)
    (keymap-set map "<return>" #'timu-shell-eshell-ls-find-file)
    (keymap-set map "<mouse-1>" #'timu-shell-eshell-ls-find-file)
    (keymap-set map "d" #'timu-shell-eshell-ls-delete-file)
    map)
  "Keys in effect when point is over a file from `eshell/ls'.")

;;;; syntax highlighting
(eshell-syntax-highlighting-global-mode 1)

;;;; history autosuggestions
(add-hook 'eshell-mode-hook #'esh-autosuggest-mode)

;;;; run visual commands in vterm
(with-eval-after-load 'eshell
  (eshell-vterm-mode))


;;; VTERM
(add-to-list 'display-buffer-alist
             '("\\*vterm\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.40)))

(pcase system-type
  ((or 'darwin 'gnu/linux)
   ;; vterm config
   (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")
   (customize-set-variable 'vterm-max-scrollback 10000)
   (customize-set-variable 'vterm-shell "/bin/zsh")
   (customize-set-variable 'vterm-kill-buffer-on-exit t)
   (advice-add 'evil-collection-vterm-insert :before #'vterm-reset-cursor-point)

   ;; term keybindings
   (keymap-set term-raw-map "M-o" #'find-files)
   (keymap-set term-raw-map "M-t" #'tab-bar-switch-to-next-tab)
   (keymap-set term-raw-map "M-q" #'save-buffers-kill-terminal)
   (keymap-set term-raw-map "M-<left>" #'windmove-left)
   (keymap-set term-raw-map "M-<right>" #'windmove-right)
   (keymap-set term-raw-map "M-<up>" #'windmove-up)
   (keymap-set term-raw-map "M-<down>" #'windmove-down)
   (keymap-set term-mode-map "C-M-p" #'popper-toggle)

   ;; vterm keybindings
   (keymap-set vterm-mode-map "M-b" #'consult-buffer)
   (keymap-set vterm-mode-map "M-v" #'yank)
   (keymap-set vterm-mode-map "M-c" #'kill-ring-save)
   (keymap-set vterm-mode-map "M-n" #'evil-buffer-new)
   (keymap-set vterm-mode-map "M-w" #'kill-current-buffer)
   (keymap-set vterm-mode-map "M-t" #'tab-bar-switch-to-next-tab)
   (keymap-set vterm-mode-map "M-<left>" #'windmove-left)
   (keymap-set vterm-mode-map "M-<right>" #'windmove-right)
   (keymap-set vterm-mode-map "M-<up>" #'windmove-up)
   (keymap-set vterm-mode-map "M-<down>" #'windmove-down)
   (keymap-set vterm-mode-map "C-M-p" #'popper-toggle)
   (keymap-global-set "C-M-v" #'timu-send-to-vterm-right)))


;;; WORKING IN THE TTY
;; Keymap for arrow keys on the tty.
;; Issue of the arrow keys being sent as the escape codes in tty & co.
;; Credit: http://kb.mit.edu/confluence/x/HJ87
;; Credit: https://emacs.stackexchange.com/a/7152
(unless (display-graphic-p)
  (define-prefix-command 'timu-shell-arrow-keys-map)
  (keymap-set esc-map "O" #'timu-shell-arrow-keys-map)
  (keymap-set timu-shell-arrow-keys-map "A" #'previous-line)
  (keymap-set timu-shell-arrow-keys-map "B" #'next-line)
  (keymap-set timu-shell-arrow-keys-map "C" #'forward-char)
  (keymap-set timu-shell-arrow-keys-map "D" #'backward-char))

(unless (display-graphic-p)
  (keymap-unset evil-motion-state-map "TAB")
  (evil-define-key 'normal outline-minor-mode-map
    (kbd "TAB") #'outline-toggle-children)
  (evil-define-key 'normal org-mode-map
    (kbd "TAB") #'org-cycle))


;;;; cursor shape for tty
;; Credit: https://github.com/7696122/evil-terminal-cursor-changer/issues/19
(setq evil-motion-state-cursor 'box)  ; █
(setq evil-visual-state-cursor 'box)  ; █
(setq evil-normal-state-cursor 'box)  ; █
(setq evil-insert-state-cursor 'bar)  ; ⎸
(setq evil-emacs-state-cursor  'box) ; █

(unless (display-graphic-p)
  (evil-terminal-cursor-changer-activate))


(provide 'timu-shell)

;;; timu-shell.el ends here
