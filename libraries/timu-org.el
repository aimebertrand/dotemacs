;;; timu-org.el --- Org mode configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-02-14
;; Keywords: orgmode org tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my org mode configuration.

;;; Code:


(require 'noflet)
(require 'org-capture)
(require 'org-remark)
(require 'org-bullets)


;;; DEFAULTS
(defgroup timu-org ()
  "Customise group for the `timu-org' Library."
  :group 'timu)

(defcustom timu-org-path
  (expand-file-name "libraries/timu-org.el" user-emacs-directory)
  "Variable for the path of the module `timu-org'."
  :type 'file
  :group 'timu-org)

(defcustom timu-org-notes-file
  (expand-file-name "files/notes.org" org-directory)
  "Variable for the path of the `notes' org files."
  :type 'file
  :group 'timu-org)


;;; FUNCTIONS
(defun timu-org-go-to-heading (&optional heading)
  "Go to an outline HEADING with `consult-org-heading'.
Also move the heading to the top of the buffer with `evil-scroll-line-to-top'"
  (interactive)
  (consult-org-heading)
  (evil-scroll-line-to-top heading))

(defun timu-org-align-all-tags ()
  "Align the tags in all headings."
  (interactive)
  (org-align-tags))

(defun timu-org-tree-to-right ()
  "Version of `org-tree-to-indirect-buffer' that follows with the cursor."
  (interactive)
  (org-tree-to-indirect-buffer)
  (other-window 1))

(defun timu-org-ctrl-c-ctrl-c ()
  "Execute `org-ctrl-c-ctrl-c' with 2x `universal-argument'."
  (interactive)
  (let ((current-prefix-arg ' (16)))
    (call-interactively #'org-ctrl-c-ctrl-c)))

(defun timu-org-toggle-emphasis-markers ()
  "Togle the visibility of the Org-mode Emphasis markers.
Requires `org-mode-restart' to take effect in the current session."
  (interactive)
  (if (equal org-hide-emphasis-markers t)
      (customize-set-variable 'org-hide-emphasis-markers nil)
    (customize-set-variable 'org-hide-emphasis-markers t))
  (org-mode-restart))

(defalias 'timu-org-clean-header-macro
  (kmacro "0 y y <down> p c t [ - SPC U R L : : SPC <escape> $ F ]
<right> D <up> <up> 0 f [ d f ] x $ F ] <left> x x")
"Macro to clean captured headers in my org files.")

(defun timu-org-notes-header-clean ()
  "Function to cleanup Notes captured with `timu-org-safari-url-capture'.
It is recorded from a Keyboard macro.
Optional argument ARG."
  (interactive)
  (timu-org-clean-header-macro))

(defun timu-org-mode-pretty-symbols ()
  "Enable `prettify-symbols-mode' and set `prettify-symbols-alist'.
Apply this just to certain `org-mode' symbols."
  (pcase system-type
    ('darwin
     (setq prettify-symbols-alist
           `(("[#A]" . ?􀂔)
             ("[#B]" . ?􀂖)
             ("[#C]" . ?􀂘)
             ("[ ]" . ?􀏃)
             ("[X]" . ?􀏋)
             ("[-]" . ?􀏉))))
    ((or 'gnu/linux 'windows-nt)
     (setq prettify-symbols-alist
           `(("[#A]" . ?Ⓐ)
             ("[#B]" . ?Ⓑ)
             ("[#C]" . ?Ⓒ)
             ("[ ]" . ?)
             ("[X]" . ?)
             ("[-]" . ?)))))
  (prettify-symbols-mode 1))

(defun timu-org-make-capture-frame ()
  "Create a new frame and run `org-capture'."
  (interactive)
  (make-frame '((name . "capture")
                (top . 300)
                (left . 700)
                (width . 80)
                (height . 25)))
  (select-frame-by-name "capture")
  (delete-other-windows)
  (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
    (org-capture)))

;;;; capture and/or org-yank from macos clipboard
;; credit: http://www.howardism.org/Technical/Emacs/capturing-content.html
;; credit: https://gitlab.com/howardabrams/spacemacs.d/-/tree/master/layers
(defun timu-org-cmd-with-exit-code (program &rest args)
  "Run PROGRAM with ARGS and return the exit code and output in a list."
  (with-temp-buffer
    (list (apply #'call-process program nil (current-buffer) nil args)
          (buffer-string))))

(defun timu-org-convert-applescript-to-html (contents)
  "Return the Applescript's clipboard CONTENTS in a packed array.
Convert and return this encoding into a UTF-8 string."
  (cl-flet ((hex-pack-bytes (tuple) (string-to-number (apply #'string tuple) 16)))
    (let* ((data (-> contents
                     (substring 10 -2) ; strips off the =«data RTF= and =»\= bits
                     (string-to-list)))
           (byte-seq (->> data
                          (-partition 2)  ; group each two hex characters into tuple
                          (mapcar #'hex-pack-bytes))))
      (decode-coding-string
       (mapconcat #'byte-to-string byte-seq "") 'utf-8))))

(defun timu-org-get-os-clipboard ()
  "Return a list where the first entry is the either :html or :text.
The second is the clipboard contents."
  (if (eq system-type 'darwin)
      (timu-org-get-mac-clipboard)
    (timu-org-get-linux-clipboard)))

(defun timu-org-get-mac-clipboard ()
  "Return the clipbaard for a macOS system.
See `timu-org-get-os-clipboard'."
  (cl-destructuring-bind (exit-code contents)
      (timu-org-cmd-with-exit-code "/usr/bin/osascript" "-e" "the clipboard as \"HTML\"")
    (if (= 0 exit-code)
        (list :html (timu-org-convert-applescript-to-html contents))
      (list :text (shell-command-to-string "/usr/bin/osascript -e 'the clipboard'")))))

(defun timu-org-get-linux-clipboard ()
  "Return the clipbaard for a Linux system.
See `timu-org-get-os-clipboard'."
  (cl-destructuring-bind (exit-code contents)
      (timu-org-cmd-with-exit-code "xclip" "-o" "-sel" "clip" "-t" "text/html")
    (if (= 0 exit-code)
        (list :html contents)
      (list :text (shell-command-to-string "xclip -o")))))

(defun timu-org-clipboard ()
  "Return the contents of the clipboard in `org-mode' format."
  (cl-destructuring-bind (type contents) (timu-org-get-os-clipboard)
    (with-temp-buffer
      (insert contents)
      (if (eq :html type)
          (shell-command-on-region
           (point-min)
           (point-max) (concat (executable-find "pandoc") " -f html -t org --wrap=none") t t)
        (shell-command-on-region
         (point-min)
         (point-max) (concat (executable-find "pandoc") " -f markdown -t org --wrap=none") t t))
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun timu-org-yank-clipboard ()
  "Yank the contents of the Mac clipboard in an `org-mode' compatible format."
  (interactive)
  (insert (timu-org-clipboard)))

(defun timu-org-quick-outlook-capture ()
  "Call `org-capture-string' on the currently selected outlook msg."
  (org-capture-string (org-mac-link-outlook-message-get-links) "o")
  (ignore-errors)
  (org-capture-finalize))

(defun timu-org-safari-capture ()
  "Call `org-capture-string' on the contents of the Apple clipboard.
Use `org-mac-link-safari-get-frontmost-url' to capture content from Safari.
Triggered by a custom macOS Quick Action with keybinding."
  (org-capture-string (timu-org-clipboard) "s")
  (ignore-errors)
  (insert (org-mac-link-safari-get-frontmost-url))
  (org-capture-finalize))

(defun timu-org-safari-url-capture ()
  "Call `org-capture-string' on the current front most Safari window.
Use `org-mac-link-safari-get-frontmost-url' to capture url from Safari.
Triggered by a custom macOS Quick Action with a keyboard shortcut."
  (org-capture-string (org-mac-link-safari-get-frontmost-url) "u")
  (ignore-errors)
  (org-capture-finalize)
  (find-file timu-org-notes-file)
  (goto-char (point-max))
  (org-previous-visible-heading 1)
  (timu-org-notes-header-clean)
  (write-file timu-org-notes-file))

(defun timu-org-quick-safari-blog-idea-capture ()
  "Call `org-capture-string' on the current most front Safari window.
Use `org-mac-link-safari-get-frontmost-url' to capture blog ideas from Safari.
Triggered by a custom macOS Quick Action with keybinding."
  (org-capture-string (org-mac-link-safari-get-frontmost-url) "b")
  (ignore-errors)
  (org-capture-finalize))

(defun timu-org-qutebrowser-capture ()
  "Call `org-capture-string' on the contents of the Apple clipboard.
Use `org-mac-link-qutebrowser-get-frontmost-url' to capture qutebrowser content.
Triggered by a custom macOS Quick Action with keybinding."
  (org-capture-string (timu-org-clipboard) "q")
  (ignore-errors)
  (insert (org-mac-link-qutebrowser-get-frontmost-url))
  (org-capture-finalize))

;;;; org refiling subtree to a standalone file
(defun timu-org-get-subtree-tags (&optional props)
  "Given PROPS, from a call to `org-entry-properties', return a list of tags."
  (let ((tags-inherited t))
    (unless props
      (setq props (org-entry-properties)))
    (let ((tag-label (if tags-inherited "ALLTAGS" "TAGS")))
      (-some->> props
        (assoc tag-label)
        cdr
        substring-no-properties
        (s-split ":")
        (--filter (not (cl-equalp "" it)))))))

(defun timu-org-get-subtree-properties (attributes)
  "Return a list of tuples of a subtrees ATTRIBUTES where the keys are strings."
  (let ((timu-org-symbol-upcase-p
         (lambda (sym)
           (let ((case-fold-search nil))
             (string-match-p "^:[A-Z]+$" (symbol-name sym)))))
        (timu-org-convert-tuple
         (lambda (tup)
           (let ((key (cl-first tup))
                 (val (cl-second tup)))
             (list (substring (symbol-name key) 1) val)))))
    (->> attributes
         ;; Convert plist to list of tuples
         (-partition 2)
         ;; Remove lowercase tuples
         (--filter (funcall timu-org-symbol-upcase-p (cl-first it)))
         (-map timu-org-convert-tuple))))

(defun timu-org-get-subtree-content (attributes)
  "Return the contents and ATTRIBUTES of the current subtree as a string."
  (let ((header-components '(clock diary-sexp drawer headline inlinetask
                                   node-property planning property-drawer section)))
    (goto-char (plist-get attributes :contents-begin))
    ;; Walk down past the properties, etc.
    (while
        (let* ((cntx (org-element-context))
               (elem (cl-first cntx))
               (props (cl-second cntx)))
          (when (member elem header-components)
            (goto-char (plist-get props :end)))))
    ;; At this point, we are at the beginning of what we consider
    ;; the contents of the subtree, so we can return part of the buffer:
    (buffer-substring-no-properties (point) (org-end-of-subtree))))

(defun timu-org-subtree-metadata ()
  "Return a list of key aspects of an org-subtree.
Includes the following: header text, body contents, list of tags,
region list of the start and end of the subtree."
  (save-excursion
    ;; Jump to the parent header if not already on a header
    (when (not (org-at-heading-p))
      (org-previous-visible-heading 1))
    (let* ((context (org-element-context))
           (attrs   (cl-second context))
           (props   (org-entry-properties)))
      (list :region     (list (plist-get attrs :begin) (plist-get attrs :end))
            :header     (plist-get attrs :title)
            :tags       (timu-org-get-subtree-tags props)
            :properties (timu-org-get-subtree-properties attrs)
            :body       (timu-org-get-subtree-content attrs)))))

(defun timu-org-set-file-property (key value &optional spot)
  "Make sure file has a top-level, file-wide property.
KEY is something like \"TITLE\" or \"FILETAGS\".
This function makes sure that the property contains the contents of VALUE,
and if the file doesn't have the property, it is inserted at either SPOT,
or if nil,the top of the file."
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search t))
      (if (re-search-forward (format "^#\\+%s:\s*\\(.*\\)" key) nil t)
          (replace-match value nil nil nil 1)
        (cond
         ;; if SPOT is a number, go to it:
         ((numberp spot) (goto-char spot))
         ;; If SPOT is not given, jump to first blank line:
         ((null spot) (progn (goto-char (point-min))
                             (re-search-forward "^\s*$" nil t)))
         (t (goto-char (point-min))))
        (insert (format "#+%s: %s\n" (upcase key) value))))))

(defun timu-org-create-org-file (filepath header body tags properties)
  "Create a new Org file by FILEPATH.
The file content is pre-populated with the HEADER,
BODY, any associated TAGS and PROPERTIES."
  (find-file-other-window filepath)
  (timu-org-set-file-property "TITLE" header t)
  (when tags
    (timu-org-set-file-property "FILETAGS" (s-join " " tags)))
  ;; Insert any drawer properties as #+PROPERTY entries:
  (when properties
    (goto-char (point-min))
    (or (re-search-forward "^\s*$" nil t) (point-max))
    (--map (insert (format "#+PROPERTY: %s %s" (cl-first it) (cl-second it))) properties))
  ;; My auto-insert often adds an initial headline for a subtree, and in this
  ;; case, I don't want that... Yeah, this isn't really globally applicable,
  ;; but it shouldn't cause a problem for others.
  (when (re-search-forward "^\\* [0-9]$" nil t)
    (replace-match ""))
  (delete-blank-lines)
  (goto-char (point-max))
  (insert "\n")
  (insert body))

(defun timu-org-filename-from-title (title)
  "Create a useful filename based on a header string, TITLE.
For instance, given the string: \"What's all this then?\"
This function will return: `whats-all-this-then'."
  (let* ((no-letters (rx (one-or-more (not alphanumeric))))
         (init-try (->> title
                        downcase
                        (replace-regexp-in-string "'" "")
                        (replace-regexp-in-string no-letters "-"))))
    (string-trim init-try "-+" "-+")))

(defun timu-org-refile-subtree-to-file (dir)
  "Archive the `org-mode' subtree and create an entry in the directory DIR.
It attempts to move as many of the properties and features to the new file."
  (interactive "DESTINATION: ")
  (let* ((props      (timu-org-subtree-metadata))
         (head       (plist-get props :header))
         (body       (plist-get props :body))
         (tags       (plist-get props :tags))
         (properties (plist-get props :properties))
         (area       (plist-get props :region))
         (filename   (timu-org-filename-from-title head))
         (filepath   (format "%s/%s.org" dir filename)))
    (apply #'delete-region area)
    (timu-org-create-org-file filepath head body tags properties)))

(defun timu-org-refile-directly (file-dest &optional show-after)
  "Move the current subtree to the end of FILE-DEST.
If SHOW-AFTER is non-nil, show the destination window,
otherwise, this destination buffer is not shown."
  (interactive "fDestination: ")
  (let ((dump-it (lambda (file contents)
                   (find-file-other-window file-dest)
                   (goto-char (point-max))
                   (insert "\n" contents))))
    (save-excursion
      (let* ((region (save-excursion
                       (list (progn (org-back-to-heading) (point))
                             (progn (org-end-of-subtree)  (point)))))
             (contents (buffer-substring (cl-first region) (cl-second region))))
        (apply #'kill-region region)
        (if show-after
            (save-current-buffer (funcall dump-it file-dest contents))
          (save-window-excursion (funcall dump-it file-dest contents)))))))

(defun timu-org-insert-header-month ()
  "Insert a date as an org heading in the format `1970-01 January'."
  (interactive)
  (insert "** " (format-time-string "%Y-%m %B")))

(defun timu-org-insert-header-day ()
  "Insert a date as an org heading in the format `1970-01-01 Thursday'."
  (interactive)
  (insert "*** " (format-time-string "%Y-%m-%d %A")))

(defun timu-org-insert-org-datetree ()
  "Insert a datetree as org headings."
  (interactive)
  (insert "** " (format-time-string "%Y-%m %B") "\n"
          "*** " (format-time-string "%Y-%m-%d %A")))

;;;; ox-hugo
(defun timu-org-browse-current-hugo-article ()
  "Browser blog article from the current `ox-hugo' org file."
  (interactive)
  (browse-url
   (concat "https://macowners.club/posts/"
           (f-no-ext (f-filename (buffer-file-name))) "/")))


;;; ORG-CONFIG
;; org-mode variables
(customize-set-variable 'calendar-week-start-day 1)
(customize-set-variable 'org-agenda-start-on-weekday 1)
(customize-set-variable 'org-log-into-drawer t)
(customize-set-variable 'org-enforce-todo-checkbox-dependencies t)
(customize-set-variable 'org-id-link-to-org-use-id 1)
(customize-set-variable 'org-tags-column 0)
(customize-set-variable 'org-image-actual-width nil)
(customize-set-variable 'org-cycle-hook '(org-cycle-hide-archived-subtrees
                                          org-cycle-show-empty-lines
                                          org-optimize-window-after-visibility-change))
(customize-set-variable 'org-agenda-files (list "~/org/files"))
(customize-set-variable 'org-refile-allow-creating-parent-nodes t)
(customize-set-variable 'org-refile-active-region-within-subtree t)
(customize-set-variable 'org-refile-use-outline-path t)
(customize-set-variable 'org-refile-targets
                        '((nil :maxlevel . 3)
                          (org-agenda-files :maxlevel . 3)))
(customize-set-variable
 'org-id-locations-file
 (expand-file-name "local/.org-id-locations" user-emacs-directory))
(customize-set-variable 'org-directory "~/org")
(customize-set-variable 'org-publish-timestamp-directory
                        (expand-file-name "org-timestamps" user-emacs-directory))
(customize-set-variable 'org-return-follows-link t)
(customize-set-variable 'org-use-sub-superscripts nil)
(customize-set-variable 'org-src-window-setup 'split-window-below)
(customize-set-variable 'org-link-frame-setup '((file . find-file)))
(customize-set-variable 'org-log-done 'time)
(customize-set-variable 'org-confirm-babel-evaluate nil)
(customize-set-variable 'org-hide-emphasis-markers t)
(customize-set-variable 'org-html-validation-link nil)
(customize-set-variable 'org-emphasis-alist
                        '(("*" timu-macos-bold-face)
                          ("/" timu-macos-italic-face)
                          ("_" timu-macos-underline-face)
                          ("=" org-verbatim verbatim)
                          ("~" org-code verbatim)
                          ("+" timu-macos-strike-through-face)))

;; org-babel languages
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (org-babel-do-load-languages
    'org-babel-load-languages
    '((C . nil)
      (awk . t)
      (chatgpt-shell . t)
      (clojure . t)
      (css . t)
      (dall-e-shell . t)
      (emacs-lisp . t)
      (http . t)
      (java . t)
      (js . t)
      (latex . t)
      (lisp . t)
      (powershell . t)
      (python . t)
      (restclient . t)
      (shell . t)))))

;; org-babel error output window
(add-to-list 'display-buffer-alist
             '("\\*Org-Babel Error Output\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.30)))

;; org download
(customize-set-variable 'org-download-image-dir "~/Pictures")
;; drag-and-drop to `dired`
(add-hook 'dired-mode-hook #'org-download-enable)
;; annoying indentation after RET
(add-hook 'org-mode-hook (lambda () (electric-indent-mode -1)))

;; custom states
(customize-set-variable
 'org-todo-keywords
 '((sequence "TODO(t)" "PROGRESS(p)" "WAITING(w)"
             "|" "DONE(d@)" "CANCELLED(@c)")
   (sequence "MAIL(t)" "|" "DONE(@d)")))

;; `TODO' keywords faces
(customize-set-variable
 'org-todo-keyword-faces
 '(("BACKLOG" . (:inherit font-lock-comment-face :weight bold))
   ("TODO" . (:inherit error :weight bold))
   ("MAIL" . (:inherit error :weight bold))
   ("PROGRESS" . (:inherit font-lock-function-name-face :weight bold))
   ("WAITING" . (:inherit warning :weight bold))
   ("REVIEW" . (:inherit warning :weight bold))
   ("CLOSED" . (:inherit success :weight bold))
   ("DONE" . (:inherit success :weight bold))
   ("RESOLVED" . (:inherit success :weight bold))
   ("CANCELED" . (:inherit success :weight bold))))

;; colors for priorities
(customize-set-variable
 'org-priority-faces
 '((?A . (:inherit error :weight bold))
   (?B . (:inherit font-lock-function-name-face :weight bold))
   (?C . (:inherit success :weight bold))))

;; org refile & archive save advices
(defadvice org-revert-all-org-buffers
    (around auto-confirm compile activate)
  "Advice to revert all org buffers after refiling."
  (cl-letf (((symbol-function 'yes-or-no-p) (lambda (&rest args) t))
            ((symbol-function 'y-or-n-p) (lambda (&rest args) t))) ad-do-it))

(advice-add 'org-refile :after #'org-save-all-org-buffers)
(advice-add 'org-archive-subtree :after #'org-save-all-org-buffers)
(advice-add 'org-save-all-org-buffers :after #'org-revert-all-org-buffers)

;;;; prettify symbols
(pcase system-type
  ('darwin
   (customize-set-variable 'prettify-symbols-unprettify-at-point t)
   (add-hook 'org-mode-hook #'timu-org-mode-pretty-symbols)
   (add-hook 'org-agenda-mode-hook #'timu-org-mode-pretty-symbols)))


;;; ORG-CAPTURE
;;;; org-protocol
(add-to-list 'org-modules 'org-protocol)
(customize-set-variable 'org-blank-before-new-entry '((heading . nil)
                                                      (plain-list-item . nil)))
(add-hook 'org-capture-mode-hook
          (lambda ()
            (setq-local header-line-format nil)))

;; bind key to: emacsclient -ne "(timu-org-make-capture-frame)"
;; with a macos Automator.app created Service
(defadvice org-capture-finalize
    (after delete-capture-frame activate)
  "Advise capture-finalize to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

(defadvice org-capture-destroy
    (after delete-capture-frame activate)
  "Advise capture-destroy to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

;;;; org-capture templates
(add-to-list 'org-capture-templates
             '("s" "macOS Safari clipboard capture" entry
               (file+olp+datetree "~/org/files/notes.org")
               "* %?    :safari:note:\n%U\n\n%i\n"))

(add-to-list 'org-capture-templates
             '("q" "macOS qutebrowser clipboard capture" entry
               (file+olp+datetree "~/org/files/notes.org")
               "* %?    :qutebrowser:note:\n%U\n\n%i\n"))

(add-to-list 'org-capture-templates
             '("u" "URL capture from Safari" entry
               (file+olp+datetree "~/org/files/notes.org")
               "* %i    :safari:url:\n%U\n\n"))

(add-to-list 'org-capture-templates
             '("b" "Quick blog idea capture from Safari" entry
               (file+headline "~/org/files/ideas.org" "BLOG")
               "* %i    :blog:idea:\n%U\n\n"))

(add-to-list 'org-capture-templates
             '("l" "elfeed capture" entry
               (file+headline "~/org/files/gtd.org" "ELFEED")
               "* %?    :elfeed:\nCapture on %U\n%i\n"))

(add-to-list 'org-capture-templates
             '("p" "private templates"))

(add-to-list 'org-capture-templates
             '("pd" "diary" entry
               (file+olp+datetree "~/org/files/diary.org")
               "* %^{diary-title} %^G\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pb" "blog idea" entry
               (file+headline "~/org/files/ideas.org" "BLOG")
               "* %^{note-title} :blog:idea:\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pp" "programming idea" entry
               (file+headline "~/org/files/ideas.org" "PROGRAMMING")
               "* %^{note-title} :programming:idea:\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pn" "note" entry
               (file+olp+datetree "~/org/files/notes.org")
               "* %^{note-title} %^G
%U
[[%^C][with more info...]]
%?\n"))

(add-to-list 'org-capture-templates
             '("ps" "song" entry
               (file+olp+datetree "~/org/files/songs.org")
               "* %^{song-title} %^G
%U
- Artist :: %^{song-artist}
- Source :: [[%^C][YouTube URL...]]
%?\n"))

(add-to-list 'org-capture-templates
             '("pt" "todo" entry
               (file+headline "~/org/files/gtd.org" "TODOS")
               "** TODO [/][\%] - %^{task-title}
%U SCHEDULED: %^t DEADLINE: %^t
:PROPERTIES:
:END:
- [ ] %?\n\n"))

(add-to-list 'org-capture-templates
             '("pm" "mail" entry
               (file+headline "~/org/files/gtd.org" "MAILS")
               "** MAIL [/][\%] - %a
%U DEADLINE: %^t
:PROPERTIES:
:date: %:date
:from: %:from
:to: %:to
:msg-id: %:message-id
:END:
%?\n\n"))

(add-to-list 'org-capture-templates
             '("r" "reading templates"))

(add-to-list 'org-capture-templates
             '("rp" "post" entry
               (file+headline "~/org/files/reading.org" "POSTS")
               "* %^{post-title} :post:
- captured on: %U
[[%^C][URL to post...]]
  %?\n"))

(add-to-list 'org-capture-templates
             '("rb" "book" entry
               (file+headline "~/org/files/reading.org" "BOOKS")
               "* %^{book-title} :book:
- captured on: %U
[[%^C][URL to book...]]
%?\n"))


;;; ORG-BULLETS
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(pcase system-type
  ('darwin
   (customize-set-variable 'org-bullets-bullet-list
                           '("􀃊" "􀃌" "􀃎" "􀃐" "􀃒" "􀃔" "􀃖" "􀃘" "􀃚")))
  ((or 'gnu/linux 'windows-nt)
   (customize-set-variable 'org-bullets-bullet-list
                           '("①" "②" "③" "④" "⑤" "⑥" "⑦" "⑧" "⑨"))))


;;; ORG-AGENDA
(customize-set-variable 'org-agenda-remove-tags t)
(customize-set-variable 'org-agenda-prefix-format " %-12c %?-14t% s")
(customize-set-variable 'org-agenda-todo-keyword-format "%-10s")
(customize-set-variable 'org-agenda-scheduled-leaders
                        '("[S]        : " "[S] x%3d d.: "))
(customize-set-variable 'org-agenda-deadline-leaders
                        '("[D]        : " "[D] +%3d d.: " "[D] -%3d d.: "))
(customize-set-variable 'org-agenda-time-grid
                        '((today require-timed remove-match)
                          (000 1200 2400)
                          ":  " "┈┈┈┈┈┈┈┈┈┈┈┈┈"))
(customize-set-variable 'org-agenda-current-time-string "ᐊ┈┈┈┈┈┈┈┈ now")
(customize-set-variable 'org-agenda-block-separator (string-to-char "="))

(add-to-list
 'org-agenda-custom-commands
 '("w" "THIS WEEK"
   ((agenda ""
            ((org-agenda-overriding-header
              (concat "THIS WEEK (W" (format-time-string "%V") ")")))))))

(add-to-list
 'org-agenda-custom-commands
 '("d" "DAY'S AGENDA"
   ((agenda ""
            ((org-agenda-overriding-header
              (concat "TODAY (W" (format-time-string "%V") ")"))
             (org-agenda-span 'day)
             (org-agenda-sorting-strategy
              '((agenda time-up priority-down category-keep)))
             (org-agenda-show-log t)
             (org-agenda-log-mode-items '(clock)))))))

(add-to-list
 'org-agenda-custom-commands
 '("c" "CUSTOM OVERVIEW"
   ((tags-todo "+PRIORITY=\"A\""
               ((org-agenda-overriding-header "PRIO A")))
    (agenda ""
            ((org-agenda-overriding-header
              (concat "TODAY (W" (format-time-string "%V") ")"))
             (org-agenda-span 'day)
             (org-agenda-sorting-strategy
              '((agenda time-up priority-down category-keep)))
             (org-agenda-show-log t)
             (org-agenda-log-mode-items '(clock))))
    (agenda ""
            ((org-agenda-overriding-header
              (concat "FOLLOWING DAYS (W" (format-time-string "%V") ")"))
             (org-agenda-skip-function
              '(org-agenda-skip-entry-if 'unscheduled))
             (org-agenda-span 6)
             (org-agenda-start-day "+1d")
             (org-agenda-start-on-weekday nil)))
    (tags-todo "+private"
               ((org-agenda-overriding-header "PRIVATE TASKS")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'unscheduled))))
    (tags-todo "+work"
               ((org-agenda-overriding-header "WORK TASKS")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'unscheduled))))
    (tags "CLOSED>=\"<-7d>\"|DONE>=\"<-7d>\"|CANCELLED>=\"<-7d>\""
          ((org-agenda-overriding-header "Completed in the Last 7 Days\n"))))))

(add-to-list 'display-buffer-alist
             '("\\*Agenda Commands\\*"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.4)))

(add-to-list 'display-buffer-alist
             '("\\*Org Agenda\\*"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.5)))


;;; ORG-REMARK
(add-hook 'org-mode-hook (lambda () (org-remark-mode +1)))

(customize-set-variable 'org-remark-notes-file-path "~/org/org-remark/org-remark-notes.org")
(customize-set-variable 'org-remark-tracking-file "~/org/org-remark/.org-remark-tracking")

(org-remark-create "timu-darkcyan"
                   '(:foreground "#2b303b" :background "#5699af" :weight bold))
(org-remark-create "timu-magenta"
                   '(:foreground "#2b303b" :background "#b48ead" :weight bold))
(org-remark-create "timu-orange"
                   '(:foreground "#2b303b" :background "#d08770" :weight bold))
(org-remark-create "timu-yellow"
                   '(:foreground "#2b303b" :background "#ecbe7b" :weight bold))
(set-face-attribute 'org-remark-highlighter nil
                    :foreground "#2b303b" :background "#d08770" :underline nil)

;;;; org-remark-transient
(transient-define-prefix timu-org-remark-transient ()
  "Transient function to call `org-remark' commands/functions."
  ["ORG-REMARK TRANSIENT\n"
   ["MARKS"
    ("c" "Insert a cyan org remark" org-remark-mark-timu-darkcyan)
    ("m" "Insert a magenta org remark" org-remark-mark-timu-magenta)
    ("o" "Insert an orange org remark" org-remark-mark-timu-orange)
    ("y" "Insert a yellow org remark" org-remark-mark-timu-yellow)]
   ["OTHER COMMANDS"
    ("C" "Change the org remark at point" org-remark-change)
    ("n" "Go to next org remark" org-remark-next)
    ("p" "Go to previous org remark" org-remark-prev)
    ("O" "Open org remark margin buffer" org-remark-open)
    ("r" "Remove org remarks" org-remark-remove)
    ("t" "Toggle org remarks" org-remark-toggle)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set org-remark-mode-map "M-#" #'timu-org-remark-transient))
  ('windows-nt
   (keymap-set org-remark-mode-map "M-$" #'timu-org-remark-transient)))


;;; ORG-NOTER
(customize-set-variable 'org-noter-always-create-frame nil)
(customize-set-variable 'org-noter-notes-search-path '("~/Documents"))
(customize-set-variable 'org-noter-default-notes-file-names
                        '("notes.org"
                          "doc-archive-notes-aok.org"
                          "doc-archive-notes-air-france.org"))


;;; OX-PANDOC
(customize-set-variable
 'org-pandoc-menu-entry
 '(
   (?1 "to markdown_mmd and open." org-pandoc-export-to-markdown_mmd-and-open)
   (?3 "to html5 and open." org-pandoc-export-to-html5-and-open)
   (?5 "to html5-pdf and open." org-pandoc-export-to-html5-pdf-and-open)
   (?7 "to markdown_strict and open." org-pandoc-export-to-markdown_strict-and-open)
   (?j "to json and open." org-pandoc-export-to-json-and-open)
   (?k "to markdown and open." org-pandoc-export-to-markdown-and-open)
   (?l "to latex-pdf and open." org-pandoc-export-to-latex-pdf-and-open)
   (?o "to odt and open." org-pandoc-export-to-odt-and-open)
   (?p "to pptx and open." org-pandoc-export-to-pptx-and-open)
   (?w "to mediawiki and open." org-pandoc-export-to-mediawiki-and-open)
   (?x "to docx and open." org-pandoc-export-to-docx-and-open)))


;;; ORG-MODE KEYBINDINGS
(keymap-set org-mode-map "M-<left>" nil)
(keymap-set org-mode-map "M-<right>" nil)
(keymap-set org-mode-map "M-<up>" nil)
(keymap-set org-mode-map "M-<down>" nil)

(keymap-set org-mode-map "<normal-state> M-h" nil)
(keymap-set org-mode-map "<normal-state> M-l" nil)
(keymap-set org-mode-map "<normal-state> M-j" nil)
(keymap-set org-mode-map "<normal-state> M-k" nil)

(keymap-set org-mode-map "C-<left>" #'org-metaleft)
(keymap-set org-mode-map "C-<right>" #'org-metaright)
(keymap-set org-mode-map "C-<up>" #'org-metaup)
(keymap-set org-mode-map "C-<down>" #'org-metadown)

(keymap-set evil-normal-state-map "RET" nil)
(keymap-set evil-motion-state-map "RET" nil)
(keymap-set org-mode-map "S-<return>" #'timu-org-tree-to-right)
(keymap-set org-mode-map "M-r" #'timu-org-refile-subtree-to-file)

(keymap-global-set "M-R" #'org-revert-all-org-buffers)
(keymap-set org-mode-map "M-l" #'org-insert-link)
(keymap-set org-mode-map "<normal-state> M-l" #'org-insert-link)
(keymap-set org-mode-map "M-†" #'org-todo)
(keymap-set org-mode-map "†" #'org-set-tags-command)
(keymap-set org-mode-map "®" #'org-refile)

(keymap-set org-capture-mode-map "M-r" #'org-capture-refile)
(keymap-set org-capture-mode-map "M-s" #'org-capture-finalize)
(keymap-set org-capture-mode-map "M-w" #'org-capture-kill)

(pcase system-type
  ('darwin
   (keymap-global-set "M-©" #'org-agenda)
   (keymap-global-set "M-ø" #'timu-org-remark-transient))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "M-s-g" #'org-agenda)
   (keymap-global-set "M-s-o" #'timu-org-remark-transient)))

(pcase system-type
  ('darwin
   (keymap-set org-mode-map "M-≈" #'timu-org-ctrl-c-ctrl-c)
   (keymap-set org-mode-map "M-∂" #'org-next-visible-heading)
   (keymap-set org-mode-map "M-¨" #'org-previous-visible-heading)
   (keymap-set org-mode-map "M-@" #'org-toggle-link-display)
   (keymap-set org-mode-map "M-⁄" #'org-toggle-inline-images)
   (keymap-set org-mode-map "M-€" #'timu-org-toggle-emphasis-markers)
   (keymap-set org-mode-map "‘" #'org-edit-special)
   (keymap-set org-mode-map "M-ª" #'timu-org-go-to-heading)
   (keymap-set org-mode-map "C-ª" #'timu-org-notes-header-clean)
   (keymap-set org-mode-map "C-¥" #'timu-org-yank-clipboard)
   (keymap-set org-src-mode-map "‘" #'org-edit-src-exit))
  ('gnu/linux
   (keymap-set org-mode-map "M-s-x" #'timu-org-ctrl-c-ctrl-c)
   (keymap-set org-mode-map "C-s-d" #'org-next-visible-heading)
   (keymap-set org-mode-map "C-s-u" #'org-previous-visible-heading)
   (keymap-set org-mode-map "C-s-l" #'org-toggle-link-display)
   (keymap-set org-mode-map "s-TAB" #'org-toggle-inline-images)
   (keymap-set org-mode-map "C-s-e" #'timu-org-toggle-emphasis-markers)
   (keymap-set org-mode-map "s-#" #'org-edit-special)
   (keymap-set org-mode-map "C-s-h" #'timu-org-go-to-heading)
   (keymap-set org-mode-map "M-s-h" #'timu-org-notes-header-clean)
   (keymap-set org-mode-map "C-s-y" #'timu-org-yank-clipboard)
   (keymap-set org-src-mode-map "s-#" #'org-edit-src-exit))
  ('windows-nt
   (keymap-set org-mode-map "C-s-l" #'org-toggle-link-display)
   (keymap-set org-mode-map "C-s-i" #'org-toggle-inline-images)
   (keymap-set org-mode-map "C-s-e" #'timu-org-toggle-emphasis-markers)
   (keymap-set org-mode-map "s-$" #'org-edit-special)
   (keymap-set org-mode-map "C-s-h" #'timu-org-go-to-heading)
   (keymap-set org-mode-map "M-s-h" #'timu-org-notes-header-clean)
   (keymap-set org-mode-map "C-s-y" #'timu-org-yank-clipboard)
   (keymap-set org-src-mode-map "s-$" #'org-edit-src-exit)))

(pcase system-type
  ('darwin
   (evil-define-key 'normal org-mode-map
     (kbd "å") #'org-archive-subtree
     (kbd "Ø") #'timu-edit-insert-line-before
     (kbd "ø") #'timu-edit-insert-line-after
     (kbd "≈") #'org-ctrl-c-ctrl-c))
  ((or 'gnu/linux 'windows-nt)
   (evil-define-key 'normal org-mode-map
     (kbd "s-a") #'org-archive-subtree
     (kbd "s-O") #'timu-edit-insert-line-before
     (kbd "s-o") #'timu-edit-insert-line-after
     (kbd "s-x") #'org-ctrl-c-ctrl-c)))

(pcase system-type
  ('darwin
   (keymap-global-set "M-ç" #'org-capture) ; (alt-cmd-c)
   (keymap-global-set "C-M-ç" #'timu-org-make-capture-frame)) ; (ctrl-alt-cmd-c)
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "C-s-c" #'org-capture) ; (alt-cmd-c)
   (keymap-global-set "C-M-s-c" #'timu-org-make-capture-frame)))


(provide 'timu-org)

;;; timu-org.el ends here
