;;; timu-elfeed.el --- Elfeed configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: email tools helper elfeed
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my config for Elfeed.

;;; Code:


;;; DEFAULTS
(defgroup timu-elfeed ()
  "Customise group for the `timu-elfeed' Library."
  :group 'timu)

(defcustom timu-elfeed-path
  (expand-file-name "libraries/timu-elfeed.el" user-emacs-directory)
  "Variable for the path of the module `timu-elfeed'."
  :type 'file
  :group 'timu-elfeed)


;;; FUNCTIONS
(defun timu-elfeed-load-db-and-open ()
  "Wrapper to load the `elfeed' db from disk before opening.
`elfeed' will be opened in a new tab with `tab-bar-new-tab'."
  (interactive)
  (tab-bar-new-tab)
  (elfeed-db-load)
  (elfeed)
  (elfeed-update))

(defun timu-elfeed-search-other-window ()
  "Browse `elfeed' entry in the other window.
Credit: https://protesilaos.com/dotemacs"
  (interactive)
  (let* ((entry (if (eq major-mode 'elfeed-show-mode)
                    elfeed-show-entry
                  (elfeed-search-selected :ignore-region)))
         (win (selected-window)))
    (with-current-buffer (get-buffer "*elfeed-search*")
      (unless (one-window-p)              ; experimental
        (delete-other-windows win))
      (split-window-right)
      (other-window 1)
      (evil-window-increase-width 10)
      (elfeed-search-show-entry entry))))

(defun timu-elfeed-kill-buffer-and-window ()
  "Do-what-I-mean way to handle `elfeed' windows and buffers.
When in an entry buffer, kill the buffer and return to the Search view.
If the entry is in its own window, delete it as well.
When in the search view, close all other windows, else kill the buffer."
  (interactive)
  (let ((win (selected-window)))
    (cond ((eq major-mode 'elfeed-show-mode)
           (elfeed-kill-buffer)
           (unless (one-window-p) (delete-window win))
           (switch-to-buffer "*elfeed-search*"))
          ((eq major-mode 'elfeed-search-mode)
           (if (one-window-p)
               (progn
                 (elfeed-search-quit-window)
                 (kill-buffer "*elfeed-search*")
                 (kill-buffer "*elfeed-log*")
                 (tab-bar-close-tab))
             (delete-other-windows win))))))

(defun timu-elfeed-filter-include-tag ()
  "Use `completing-read' to select tags to include `+'.
The function reads the tags from the `elfeed' db."
  (interactive)
  (let ((filtered-tag (completing-read "Select Tags: " (elfeed-db-get-all-tags))))
    (progn
      (setq elfeed-search-filter (concat elfeed-search-filter " +" filtered-tag))
      (elfeed-search-update--force))))

(defun timu-elfeed-filter-exclude-tag ()
  "Use `completing-read' to select tags to exclude `-'.
The function reads the tags from the `elfeed' db."
  (interactive)
  (let ((filtered-tag (completing-read "Select Tags: " (elfeed-db-get-all-tags))))
    (progn
      (setq elfeed-search-filter (concat elfeed-search-filter " -" filtered-tag))
      (elfeed-search-update--force))))

(defun timu-elfeed-link-title (entry)
  "Copy the ENTRY title and URL as org link to the clipboard."
  (interactive)
  (let* ((link (elfeed-entry-link entry))
         (title (elfeed-entry-title entry))
         (titlelink (concat "[[" link "][" title "]]")))
    (when titlelink
      (kill-new titlelink)
      (gui-set-selection 'PRIMARY titlelink)
      (message "Yanked: %s" titlelink))))

(defun timu-elfeed-show-copy-link ()
  "Copy the current entry title and url as org link to the clipboard."
  (interactive)
  (timu-elfeed-link-title elfeed-show-entry))

(defun timu-elfeed-show-capture ()
  "Fastest way to capture entry link to org agenda from `elfeed' show mode.
Credit: http://heikkil.github.io/blog/2015/05/09/notes-from-elfeed-entries/"
  (interactive)
  (timu-elfeed-link-title elfeed-show-entry)
  (org-capture nil "l")
  (yank)
  (org-capture-finalize))

(defun timu-elfeed-search-copy-link ()
  "Copy the current entry title and url as org link to the clipboard."
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
             when (elfeed-entry-link entry)
             do (timu-elfeed-link-title entry))))

(defun timu-elfeed-search-capture ()
  "Capture the title and url for the selected entry or entries in org aganda.
Credit: http://heikkil.github.io/blog/2015/05/09/notes-from-elfeed-entries/"
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
             do (elfeed-untag entry 'unread)
             when (elfeed-entry-link entry)
             do (timu-elfeed-link-title entry)
             do (org-capture nil "l")
             do (yank)
             do (org-capture-finalize)
             (mapc #'elfeed-search-update-entry entries))
    (unless (use-region-p) (forward-line))))

(defun timu-elfeed-show-visit-xwidget (&optional generic)
  "Visit the current entry in Xwidget using `xwidget-webkit-browse-url'.
If there is a prefix argument, visit the current entry in the
GENERIC browser defined by `browse-url-generic-program'."
  (interactive "P")
  (let ((link (elfeed-entry-link elfeed-show-entry)))
    (when link
      (message "Sent to browser: %s" link)
      (if generic
          (browse-url-generic link)
        (xwidget-webkit-browse-url link)))))


;;; ELFEED CONFIG
(customize-set-variable 'elfeed-use-curl t)
(customize-set-variable 'elfeed-curl-max-connections 10)
(customize-set-variable 'elfeed-db-directory
                        (expand-file-name "elfeed/" user-emacs-directory))
(customize-set-variable 'elfeed-enclosure-default-dir "~/Downloads/")
(customize-set-variable 'elfeed-search-filter "@1-months-ago +unread")
(customize-set-variable 'elfeed-sort-order 'descending)
(customize-set-variable 'elfeed-show-truncate-long-urls t)
(customize-set-variable 'elfeed-show-unique-buffers t)
;; no need for the header line - info are in nano-modeline:
(add-hook 'elfeed-search-update-hook
          (lambda ()
            (setq header-line-format "")))


;;; ELFEED-ORG
(elfeed-org)
(customize-set-variable
 'rmh-elfeed-org-files
 (list (expand-file-name "elfeed/elfeed-list.org" user-emacs-directory)))


;;; ELFEED KEYBINDINGS
(keymap-set elfeed-search-mode-map "M-c" #'timu-elfeed-search-copy-link)
(keymap-set elfeed-search-mode-map "M-r" #'elfeed-update)
(keymap-set elfeed-search-mode-map "M-+" #'timu-elfeed-filter-include-tag)
(keymap-set elfeed-search-mode-map "M--" #'timu-elfeed-filter-exclude-tag)
(keymap-set elfeed-show-mode-map "C-<down>" #'elfeed-show-next)
(keymap-set elfeed-show-mode-map "C-<up>" #'elfeed-show-prev)
(keymap-set elfeed-show-mode-map "M-c" #'timu-elfeed-show-copy-link)
(keymap-set elfeed-show-mode-map "M-r" #'elfeed-update)

(pcase system-type
  ('darwin
   (keymap-set elfeed-search-mode-map "M-ç" #'timu-elfeed-search-capture)
   (keymap-set elfeed-show-mode-map "M-ç" #'timu-elfeed-show-capture))
  ((or 'gnu/linux 'windows-nt)
   (keymap-set elfeed-search-mode-map "C-s-c" #'timu-elfeed-search-capture)
   (keymap-set elfeed-show-mode-map "C-s-c" #'timu-elfeed-show-capture)))

(evil-define-key 'normal elfeed-search-mode-map
  (kbd "<escape>") #'elfeed-search-clear-filter
  (kbd "<return>") #'timu-elfeed-search-other-window
  (kbd "?") #'elfeed-search-tag-all-unread
  (kbd "!") #'elfeed-search-untag-all-unread
  (kbd "q") #'timu-elfeed-kill-buffer-and-window)
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "q") #'timu-elfeed-kill-buffer-and-window
  (kbd "a i") #'elfeed-show-visit
  (kbd "a x") #'timu-elfeed-show-visit-xwidget)


(provide 'timu-elfeed)

;;; timu-elfeed.el ends here
