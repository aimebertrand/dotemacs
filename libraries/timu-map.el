;;; timu-map.el --- Custom keybindings -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-10-17
;; Keywords: map tools helper keymap keybindings
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides custom bindings with cutom map-prefix `C-t' & `SPC'.

;;; Code:


;;; DEFAULTS
(defgroup timu-map ()
  "Customise group for the `timu-map' Library."
  :group 'timu)

(defcustom timu-map-path
  (expand-file-name "libraries/timu-map.el" user-emacs-directory)
  "Variable for the path of the module/variable `timu-map'."
  :type 'file
  :group 'timu-map)


;;; FREE THE BINDING FROM EVIL
(keymap-unset evil-insert-state-map "C-t")
(keymap-unset evil-motion-state-map "C-t")
(keymap-unset evil-normal-state-map "C-t")
(keymap-unset evil-operator-state-map "C-t")
(keymap-unset evil-replace-state-map "C-t")
(keymap-unset evil-visual-state-map "C-t")


;;; PREFIX & MAP
(define-prefix-command 'timu-map)
(keymap-global-set "C-t" #'timu-map)

(keymap-set evil-motion-state-map "C-t" #'timu-map)
(keymap-set evil-normal-state-map "C-t" #'timu-map)
(keymap-set evil-operator-state-map "C-t" #'timu-map)
(keymap-set evil-replace-state-map "C-t" #'timu-map)
(keymap-set evil-visual-state-map "C-t" #'timu-map)

(keymap-set evil-normal-state-map "SPC" #'timu-map)
(keymap-set evil-visual-state-map "SPC" #'timu-map)
(keymap-set evil-operator-state-map "SPC" #'timu-map)
(keymap-set evil-motion-state-map "SPC" #'timu-map)


;;; COMMAND
(define-key timu-map "x" #'execute-extended-command)


;;; GENERAL
(defvar-keymap timu-map-general-map
  :doc "Key prefix for general commands."
  "e" #'embark-act
  "f" #'consult-line
  "d" #'dired
  "g" #'magit-status
  "t" #'timu-nav-switch-or-new-tab
  "q" #'save-buffers-kill-terminal
  ","#'timu-base-visit-emacs-init
  ";"#'timu-base-visit-emacs-early-init)


;;; GENERAL SPECIAL
(defvar-keymap timu-map-general-special-map
  :doc "Key prefix for special general commands."
  "e" #'embark-dwim
  "t" #'timu-ui-load-theme
  "v" #'vterm
  "q" #'read-only-mode)


;;; BUFFERS
(defvar-keymap timu-map-buffer-map
  :doc "Key prefix for buffer commands."
  "b" #'consult-buffer
  "e" #'eval-buffer
  "i" #'ibuffer
  "w" #'kill-current-buffer
  "r" #'timu-edit-revert-buffer
  "s" #'save-some-buffers)


;;; FRAMES & WINDOWS
(defvar-keymap timu-map-frames-windows-map
  :doc "Key prefix for frames and windows commands."
  "Ä" #'timu-nav-toggle-split-direction
  "ä" #'timu-nav-split-and-follow-right
  "ö" #'timu-nav-split-and-follow-below
  "s" #'ace-swap-window
  "0" #'delete-window
  "1" #'delete-other-windows
  "2" #'ace-delete-window
  "<left>" #'windmove-left
  "<right>" #'windmove-right
  "<up>" #'windmove-up
  "<down>" #'windmove-down
  "." #'enlarge-window-horizontally
  "," #'shrink-window-horizontally
  "+" #'enlarge-window
  "–" #'shrink-window
  "=" #'balance-windows
  "w" #'writeroom-mode)


;;; FRAMES & WINDOWS RESIZING
(defvar-keymap timu-map-frames-windows-resizing-map
  :doc "Key prefix for frames and windows resizing commands."
  :repeat t
  "." #'enlarge-window-horizontally
  "," #'shrink-window-horizontally
  "+" #'enlarge-window
  "-" #'shrink-window)


;;; FILES
(defvar-keymap timu-map-file-map
  :doc "Key prefix for file commands."
  "O" #'timu-edit-find-file-as-root
  "o" #'find-file
  "m" #'timu-edit-move-buffer-file
  "r" #'timu-edit-rename-file-and-buffer
  "s" #'save-buffer
  "S" #'write-file)


;;; ORG
(defvar-keymap timu-map-org-map
  :doc "Key prefix for org commands."
  "c" #'org-capture
  "C" #'org-ctrl-c-ctrl-c
  "h" #'timu-modes-outline-go-to-heading
  "l" #'org-insert-link
  "r" #'timu-org-refile-subtree-to-file
  "s" #'org-capture-finalize
  "w" #'org-capture-kill
  "#" #'org-edit-special
  "'" #'org-edit-src-exit)


;;; ORG SPECIAL
(defvar-keymap timu-map-org-special-map
  :doc "Key prefix for special org commands."
  "c" #'timu-org-make-capture-frame
  "l" #'org-toggle-link-display
  "i" #'org-toggle-inline-images
  "e" #'timu-org-toggle-emphasis-markers)


;;; ORG-ROAM
(defvar-keymap timu-map-org-roam-map
  :doc "Key prefix for org roam commands."
  "c" #'org-roam-capture
  "f" #'timu-roam-find-by-tag
  "i" #'org-roam-node-insert
  "o" #'org-roam-node-find
  "r" #'org-roam-buffer-toggle)


;;; PROJECT
(defvar-keymap timu-map-project-map
  :doc "Key prefix for project commands."
  "p" #'timu-nav-project-switch-project
  "P" #'project-find-file)


;;; HELP
(defvar-keymap timu-map-help-map
  :doc "Key prefix for help commands."
  "b" #'describe-bindings
  "F" #'describe-face
  "f" #'helpful-callable
  "i" #'info
  "k" #'helpful-key
  "m" #'describe-bindings
  "p" #'describe-package
  "v" #'helpful-variable)


;;; MU4E
(defvar-keymap timu-map-mu4e-map
  :doc "Key prefix for mu4e commands."
  "r" #'mu4e-update-mail-and-index
  "s" #'message-send-and-exit
  "w" #'mu4e-message-kill-buffer)


;;; ELFEED
(defvar-keymap timu-map-elfeed-map
  :doc "Key prefix for elfeed commands."
  "e" #'timu-elfeed-load-db-and-open
  "c" #'timu-elfeed-search-capture
  "r" #'elfeed-update
  "+" #'timu-elfeed-filter-include-tag
  "-" #'timu-elfeed-filter-exclude-tag)


;;; KEYMAP PREFIXES
(defvar-keymap timu-map
  :doc "My custom timu keymap."
  "b" timu-map-buffer-map
  "e" timu-map-elfeed-map
  "f" timu-map-file-map
  "g" timu-map-general-map
  "G" timu-map-general-special-map
  "h" timu-map-help-map
  "m" timu-map-mu4e-map
  "o" timu-map-org-map
  "O" timu-map-org-special-map
  "p" timu-map-project-map
  "r" timu-map-org-roam-map
  "s" timu-map-frames-windows-resizing-map
  "w" timu-map-frames-windows-map
  "x" #'execute-extended-command)


;;; WHICH-KEY DESCRIPTIONS
(which-key-add-keymap-based-replacements timu-map
  "b" `("Buffers" . ,timu-map-buffer-map))
(which-key-add-keymap-based-replacements timu-map
  "e" `("Elfeed" . ,timu-map-elfeed-map))
(which-key-add-keymap-based-replacements timu-map
  "f" `("Files" . ,timu-map-file-map))
(which-key-add-keymap-based-replacements timu-map
  "g" `("General" . ,timu-map-general-map))
(which-key-add-keymap-based-replacements timu-map
  "G" `("General - Special" . ,timu-map-general-special-map))
(which-key-add-keymap-based-replacements timu-map
  "h" `("Help" . ,timu-map-help-map))
(which-key-add-keymap-based-replacements timu-map
  "m" `("Mu4e" . ,timu-map-mu4e-map))
(which-key-add-keymap-based-replacements timu-map
  "o" `("Org" . ,timu-map-org-map))
(which-key-add-keymap-based-replacements timu-map
  "O" `("Org - Special" . ,timu-map-org-special-map))
(which-key-add-keymap-based-replacements timu-map
  "p" `("Projects" . ,timu-map-project-map))
(which-key-add-keymap-based-replacements timu-map
  "r" `("Org Roam" . ,timu-map-org-roam-map))
(which-key-add-keymap-based-replacements timu-map
  "s" `("Org Roam" . ,timu-map-frames-windows-resizing-map))
(which-key-add-keymap-based-replacements timu-map
  "w" `("Frames & Windows" . ,timu-map-frames-windows-map))


(provide 'timu-map)

;;; timu-map.el ends here
