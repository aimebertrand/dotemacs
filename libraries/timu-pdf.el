;;; timu-pdf.el --- Config for working with pdf -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper pdf
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for all thing pdf.

;;; Code:


(require 'pdf-annot)
(require 'pdf-view)
(require 'pdfgrep)


;;; DEFAULTS
(defgroup timu-pdf ()
  "Customise group for the `timu-pdf' Library."
  :group 'timu)

(defcustom timu-pdf-path
  (expand-file-name "libraries/timu-pdf.el" user-emacs-directory)
  "Variable for the path of the module `timu-pdf'."
  :type 'file
  :group 'timu-pdf)


;;; FUNCTIONS
;;;; pdf tools
(defun timu-pdf-save-buffer ()
  "Save buffer ignoring arguments."
  (interactive)
  (save-buffer))

(defun timu-pdf-save-buffer-no-args ()
  "Save buffer ignoring arguments."
  (save-buffer))

(defun timu-pdf-revert-buffer-no-args ()
  "Revert buffer ignoring arguments."
  (revert-buffer t t))

(defun timu-pdf-open-in-external-app ()
  "Open the current PDF file in the default macOS program.
This command has a keybinding only in `pdf-view-mode'."
  (interactive)
  (if (eq major-mode 'pdf-view-mode)
      (let ((file (buffer-file-name)))
        (if file
            (progn
              (message "Opening %s in external application..." file)
              (timu-base-async-shell-command-no-window (concat "open " file)))
          (message "Buffer is not associated with a file.")))
    (message "Not in a pdf-view buffer.")))

(defun timu-pdf-password-protect ()
  "Password protect current pdf in buffer or `dired' file.
Needs the cli tool `qpdf' installed with `brew install qpdf'.
Credit: https://xenodium.com/emacs-password-protect-current-pdf/"
  (interactive)
  (unless (executable-find "qpdf")
    (user-error "Program qpdf not installed"))
  (unless
      (equal "pdf"
             (or
              (when (buffer-file-name)
                (downcase (file-name-extension (buffer-file-name))))
              (when (dired-get-filename nil t)
                (downcase (file-name-extension (dired-get-filename nil t))))))
    (user-error "No pdf to act on"))
  (let* ((user-password (read-passwd "user-password: "))
         (owner-password (read-passwd "owner-password: "))
         (input (or (buffer-file-name)
                    (dired-get-filename nil t)))
         (output (concat (file-name-sans-extension input)
                         "_enc.pdf")))
    (message
     (string-trim
      (shell-command-to-string
       (format "qpdf --verbose --encrypt '%s' '%s' 256 -- '%s' '%s'"
               user-password owner-password input output))))))


;;; PDFGREP
(pdfgrep-mode)


;;; PDF-TOOLS
;;;; install and configure pdf-tools
;; initialise
(pdf-tools-install)
;; open pdfs scaled to fit page
(customize-set-variable 'pdf-view-display-size 'fit-page)
;; automatically annotate highlights
(customize-set-variable 'pdf-annot-activate-created-annotations t)
;; more fine-grained zooming
(customize-set-variable 'pdf-view-resize-factor 1.1)

(add-hook 'pdf-view-mode-hook (lambda () (display-line-numbers-mode -1)))
(add-hook 'pdf-view-mode-hook #'pdf-tools-enable-minor-modes)

(setenv "PKG_CONFIG_PATH" "/usr/local/opt/qt/lib/pkgconfig")

(add-to-list 'auto-mode-alist '("\\.[pP][dD][fF]\\'" . pdf-view-mode))

;;;; install and configure org-pdftools
(org-pdftools-setup-link)

(advice-add 'pdf-annot-edit-contents-commit :after #'timu-pdf-save-buffer-no-args)
(advice-add 'pdf-annot-edit-contents-abort :after #'timu-pdf-revert-buffer-no-args)


;;; PDF-TOOLS KEYBINDINGS
;;;; pdf-tools transients
(transient-define-prefix timu-pdf-view-transient ()
  "Transient for PDF VIEW MODE."
  ["PDF VIEW MODE\n"
   ["LOOK"
    ("0" "Reset PDF to its default set" pdf-view-scale-reset)
    ("D" "PDF with dark background" pdf-view-dark-minor-mode)
    ("+" "Enlarge PDF by FACTOR" pdf-view-enlarge)
    ("M" "For past midnight reading" pdf-view-midnight-minor-mode)
    ("-" "Shrink PDF by FACTOR" pdf-view-shrink)
    ("p" "PDF as it would be printed" pdf-view-printer-minor-mode)
    ("P" "Fit page to window" pdf-view-fit-page-to-window)
    ("H" "Fit height to window" pdf-view-fit-height-to-window)
    ("W" "Fit width to window" pdf-view-fit-width-to-window)]
   ["ANNOTATIONS"
    ("h" "Highlight markup" pdf-annot-add-highlight-markup-annotation)
    ("m" "Markup" pdf-annot-add-markup-annotation)
    ("s" "Squiggly markup" pdf-annot-add-squiggly-markup-annotation)
    ("S" "Strikeout markup" pdf-annot-add-strikeout-markup-annotation)
    ("t" "Text annotation" pdf-annot-add-text-annotation)
    ("u" "Underline markup" pdf-annot-add-underline-markup-annotation)
    ("l" "List annotations" pdf-annot-list-annotations)
    ("d" "Delete annotations" pdf-annot-delete)
    ("a" "Description" pdf-annot-attachment-dired)]
   ["EXTRAS"
    ("B" "Go N-times backward in the history" pdf-history-backward)
    ("F" "Go N-times forward in the history" pdf-history-forward)
    ("C" "Context menu determined by EVENT" pdf-history-forward)
    ("A" "Follow link, depending on type" pdf-links-action-perform)
    ("I" "Search for links in PDF" pdf-links-isearch-link)
    ("O" "PDF occur" pdf-occur)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set pdf-view-mode-map "M-#" #'timu-pdf-view-transient))
  ('windows-nt
   (keymap-set pdf-view-mode-map "M-$" #'timu-pdf-view-transient)))

(add-hook 'pdf-view-mode-hook
          (lambda()
            (evil-collection-define-key 'normal 'pdf-view-mode-map (kbd "M-s") nil)
            (evil-collection-define-key 'normal 'pdf-view-mode-map (kbd "C-u") nil)))

(keymap-set pdf-view-mode-map "M-m" #'pdf-annot-add-markup-annotation) ; Markup
(keymap-set pdf-view-mode-map "M-s" #'timu-pdf-save-buffer)
(keymap-set pdf-view-mode-map "M-T" #'pdf-annot-add-text-annotation)
(keymap-set pdf-view-mode-map "M-l" #'pdf-annot-list-annotations) ; List annotations
(keymap-set pdf-view-mode-map "M-d" #'pdf-annot-delete) ; Delete annotations
(keymap-set pdf-view-mode-map "M-a" #'pdf-annot-attachment-dired) ; List attachments
(keymap-set pdf-view-mode-map "M-v" #'timu-pdf-open-in-external-app)

(keymap-set pdf-annot-edit-contents-minor-mode-map "M-s" #'pdf-annot-edit-contents-commit)
(keymap-set pdf-annot-edit-contents-minor-mode-map "M-w" #'pdf-annot-edit-contents-abort)


(provide 'timu-pdf)

;;; timu-pdf.el ends here
