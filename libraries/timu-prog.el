;;; timu-prog.el --- Programming languages config -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: languages tools helper python go swift
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my config for coding, languages & Co.

;;; Code:


;;; DEFAULTS
(defgroup timu-prog ()
  "Customise group for the `timu-prog' Library."
  :group 'timu)

(defcustom timu-prog-path
  (expand-file-name "libraries/timu-prog.el" user-emacs-directory)
  "Variable for the path of the module `timu-prog'."
  :type 'file
  :group 'timu-prog)


;;; FLYCHECK
;;;; flycheck
(customize-set-variable 'flycheck-display-errors-delay 0.2)

(add-to-list 'display-buffer-alist
             '("\\*Flycheck errors\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-width . 0.3)))

;;;; flycheck-posframe
(with-eval-after-load 'flycheck
  (require 'flycheck-posframe)
  (add-hook 'flycheck-mode-hook #'flycheck-posframe-mode))


;;; FLYMAKE
;;;; flymake for sh-mode
(add-hook 'sh-mode-hook #'flymake-mode)

;;;; display for diagnostic buffer
(add-to-list 'display-buffer-alist
             '("^\\*Flymake diagnostics.*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-width . 0.3)))


;;; TREE-SITTER
(add-hook 'go-mode-hook #'tree-sitter-hl-mode)
(add-hook 'js-mode-hook #'tree-sitter-hl-mode)
(add-hook 'json-mode-hook #'tree-sitter-hl-mode)
(add-hook 'lua-mode-hook #'tree-sitter-hl-mode)
(add-hook 'python-mode-hook #'tree-sitter-hl-mode)
(add-hook 'sh-mode-hook #'tree-sitter-hl-mode)
(global-tree-sitter-mode)


;;; CORFU
(customize-set-variable 'corfu-auto t)
(customize-set-variable 'corfu-auto-prefix 2)
(customize-set-variable 'corfu-auto-delay 0.1)
(customize-set-variable 'corfu-min-width 60)

(global-corfu-mode)

(keymap-set corfu-map "RET" nil)

;;;; corfu-popupinfo
(customize-set-variable 'corfu-popupinfo-delay '(0.1 . 0.1))
(corfu-popupinfo-mode 1)

;;;; kind-icons
(customize-set-variable 'kind-icon-use-icons t)
(customize-set-variable 'kind-icon-default-face 'corfu-default)
(customize-set-variable 'kind-icon-blend-background nil)

(add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter)


;;; CAPE
(add-to-list 'completion-at-point-functions #'cape-file)


;;; EGLOT
(add-hook 'sh-mode-hook #'eglot-ensure)
(add-hook 'typescript-mode-hook #'eglot-ensure)
(add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-at-point-mode)

(customize-set-variable
 'lsp-session-file
 (expand-file-name "local/.lsp-session-v1" user-emacs-directory))

;;;; eglot keybindings
(evil-define-key 'normal eglot-mode-map
  (kbd "g d") #'eglot-find-declaration
  (kbd "g i") #'eglot-find-implementations)


;;; PYTHON
;;;; python settings
;; Use IPython for REPL
(customize-set-variable 'python-shell-interpreter "python3")
(customize-set-variable 'python-shell-interpreter-args "-i")
(customize-set-variable 'python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters "python3")

(add-hook 'python-mode-hook #'eglot-ensure)
(add-hook 'python-mode-hook #'pyvenv-mode)

(add-to-list 'auto-mode-alist '("\\requirements.txt\\'" . pip-requirements-mode))

(add-to-list 'display-buffer-alist
             '("\\(\\*pylsp\\*\\|\\*pylsp.*stderr\\*\\)"
               (display-buffer-at-bottom)
               (window-height . 0.30)))

;;;; python keybindings
(keymap-set python-mode-map "M-<left>" #'windmove-left)
(keymap-set python-mode-map "M-<right>" #'windmove-right)
(keymap-set python-mode-map "M-<up>" #'windmove-up)
(keymap-set python-mode-map "M-<down>" #'windmove-down)

(keymap-set inferior-python-mode-map "M-<left>" #'windmove-left)
(keymap-set inferior-python-mode-map "M-<right>" #'windmove-right)
(keymap-set inferior-python-mode-map "M-<up>" #'windmove-up)
(keymap-set inferior-python-mode-map "M-<down>" #'windmove-down)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "∏") #'pyvenv-activate)
   (evil-global-set-key 'normal (kbd "π") #'pyvenv-deactivate))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-P") #'pyvenv-activate)
   (evil-global-set-key 'normal (kbd "s-p") #'pyvenv-deactivate)))


;;; LUA
(customize-set-variable 'lua-indent-level 2)
(add-hook 'lua-mode-hook #'eglot-ensure)


;;; GO
(add-hook 'go-mode-hook #'eglot-ensure)


;;; CSS
(add-hook 'css-mode-hook #'eglot-ensure)


;;; SWIFT
(add-hook 'swift-mode-hook #'eglot-ensure)


;;; KEYBINDINGS
(keymap-global-set "C-ç" #'compile)
(keymap-global-set "C-Ç" #'recompile)
(keymap-global-set "C-Ï" #'flycheck-mode)


(provide 'timu-prog)

;;; timu-prog.el ends here
