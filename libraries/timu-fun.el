;;; timu-fun.el --- Config for fun things -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper fun
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for fun stuff.

;;; Code:


(require 'xkcd)
(require 'ytel)


;;; DEFAULTS
(defgroup timu-fun ()
  "Customise group for the `timu-fun' Library."
  :group 'timu)

(defcustom timu-fun-path
  (expand-file-name "libraries/timu-fun.el" user-emacs-directory)
  "Variable for the path of the module `timu-fun'."
  :type 'file
  :group 'timu-fun)


;;; FUNCTIONS
(defun timu-fun-ytel-watch ()
  "Stream video at point in iina (macOS) or in mpv (Linux)."
  (interactive)
  (let* ((video (ytel-get-current-video))
         (id (ytel-video-id video))
         (player (pcase system-type
                   ('darwin "iina")
                   ('gnu/linux "mpv"))))
    (start-process (concat "ytel " player) nil
                   player
                   (concat "https://www.youtube.com/watch?v=" id)
                   "--ytdl-format=bestvideo[height<=?1080]+bestaudio/best")
    (message "Starting streaming...")))

(defun timu-fun-public-ip ()
  "Get your current public IP unsing `https://api.ipify.org'."
  (interactive)
  (message "IP: %s"
           (with-current-buffer
               (url-retrieve-synchronously "https://api.ipify.org")
             (buffer-substring (+ 1 url-http-end-of-headers) (point-max)))))

(defun timu-fun-create-xkcd-direcory ()
  "Create the `xkcd-cache-dir' if it does not exist."
  (unless (file-directory-p xkcd-cache-dir)
    (make-directory xkcd-cache-dir t)))


;;; GAMES
(customize-set-variable 'gamegrid-user-score-file-directory
                        (expand-file-name "local/games/" user-emacs-directory))


;;; YTEL
(setq ytel-invidious-api-url "https://invidious.snopyta.org")
(setq ytel-title-video-reserved-space 80)

(evil-define-key 'normal ytel-mode-map (kbd "q") #'ytel-quit)
(evil-define-key 'normal ytel-mode-map (kbd "s") #'ytel-search)
(keymap-set ytel-mode-map "S-<right>" #'ytel-search-next-page)
(keymap-set ytel-mode-map "S-<left>" #'ytel-search-previous-page)
(keymap-set ytel-mode-map "RET" #'timu-fun-ytel-watch)


;;; XKCD
(customize-set-variable 'xkcd-cache-dir
                        (expand-file-name "local/xkcd/" user-emacs-directory))
(customize-set-variable 'xkcd-cache-latest
                        (expand-file-name "local/xkcd/latest" user-emacs-directory))

(add-to-list 'evil-emacs-state-modes 'xkcd-mode)

(add-to-list 'after-init-hook
             (lambda ()
               (delete-directory
                (expand-file-name "xkcd/" user-emacs-directory))))

(advice-add 'xkcd :before #'timu-fun-create-xkcd-direcory)

(keymap-set xkcd-mode-map "r" #'xkcd-rand)
(keymap-set xkcd-mode-map "t" #'xkcd-alt-text)
(keymap-set xkcd-mode-map "<right>" #'xkcd-next)
(keymap-set xkcd-mode-map "<left>" #'xkcd-prev)


(provide 'timu-fun)

;;; timu-fun.el ends here
