;;; timu-bits.el --- Module to handle other modules -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-12-23
;; Keywords: modules tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my defaults to be loaded in the beginning.

;;; Code:


;;; DEFAULTS
(defgroup timu-bits ()
  "Customise group for the `timu-bits' Library."
  :group 'timu)

(defcustom timu-bits-path
  (expand-file-name "libraries/timu-bits.el" user-emacs-directory)
  "Variable for the path of the module `timu-bits'."
  :type 'file
  :group 'timu-bits)


;;; SETUP PACKAGE SOURCES
(customize-set-variable 'package-enable-at-startup nil)

(pcase system-type
  ('windows-nt
   (customize-set-variable 'package-check-signature nil)))

(customize-set-variable 'package-archives
                        '(("melpa" . "https://melpa.org/packages/")
                          ("gnu" . "https://elpa.gnu.org/packages/")
                          ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

(customize-set-variable 'package-archive-priorities
                        '(("melpa" . 9)
                          ("gnu" . 6)
                          ("nongnu" . 2)))

(package-initialize)


;;; SETUP QUELPA
(customize-set-variable 'quelpa-dir
                        (expand-file-name "local/quelpa" user-emacs-directory))
(customize-set-variable 'quelpa-checkout-melpa-p nil)

(unless (package-installed-p 'quelpa)
  (with-temp-buffer
    (url-insert-file-contents
     "https://raw.githubusercontent.com/quelpa/quelpa/master/quelpa.el")
    (eval-buffer)
    (quelpa-self-upgrade)))


;;; PACKAGES TO BE INSTALLED
(defcustom timu-bits-package-list
  '(ace-link ace-window all-the-icons all-the-icons-dired
    all-the-icons-completion all-the-icons-ibuffer ansible ansible-doc
    apples-mode applescript-mode auctex auctex-latexmk auto-dictionary avy
    browse-at-remote cape catppuccin-theme chatgpt-shell consult corfu
    corfu-prescient csv-mode dall-e-shell dash demap diff-hl
    dired-filetype-face dired-narrow dired-subtree docker docker-compose-mode
    docker-tramp dockerfile-mode dwim-shell-command edit-indirect eldoc-box
    elfeed elfeed-org elisp-demos emacs-everywhere emacsql-sqlite-module embark
    embark-consult eshell-syntax-highlighting esh-autosuggest eshell-vterm
    esxml evil evil-collection evil-surround evil-terminal-cursor-changer
    exec-path-from-shell expenses flycheck flycheck-posframe flyspell-correct
    free-keys forge git-modes gnu-elpa-keyring-update go-mode gptel
    grab-mac-link grip-mode helpful htmlize imenu-list json-mode kind-icon
    kql-mode lua-mode magit marginalia markdown-mode minions mixed-pitch
    monkeytype mu4e-column-faces mu4e-thread-folding multi-vterm mw-thesaurus
    neotree noflet ob-applescript ob-async ob-chatgpt-shell ob-dall-e-shell
    ob-http ob-restclient orderless org org-bullets org-contrib org-download
    org-jira org-mac-link org-noter org-noter-pdftools org-pdftools org-remark
    org-roam org-web-tools osx-dictionary osx-trash outline-minor-faces ox-hugo
    ox-pandoc package-lint paradox pdfgrep pdf-tools peep-dired
    pip-requirements popper powershell powerthesaurus prescient project
    python-mode quelpa rainbow-csv rainbow-delimiters rainbow-mode restclient s
    swift-mode syslog-mode timu-caribbean-theme timu-line timu-macos-theme
    timu-rouge-theme timu-spacegrey-theme tldr toml-mode transient tree-sitter
    tree-sitter-langs treemacs treemacs-all-the-icons treemacs-evil
    treemacs-icons-dired typescript-mode undo-tree vertico vertico-prescient
    vimrc-mode vterm web-mode websearch wgrep which-key writeroom-mode xkcd
    xwwp yaml-mode yasnippet ytel)
  "List of packages to be installed for the Emacs config to work as configured.
The packages will be installed with `timu-bits-install-packages'."
  :type '(repeat symbol)
  :group 'timu-bits)

(defcustom timu-bits-installed-packages (mapcar #'car package-alist)
  "List of all installed packages."
  :type '(repeat symbol)
  :group 'timu-bits)

(defcustom timu-bits-outdated-packages (package--upgradeable-packages)
  "List of Packages that are available to Upgrade."
  :type '(repeat symbol)
  :group 'timu-bits)

(defcustom timu-bits-removable-packages (package--removable-packages)
  "List of Packages that can be remove with `package-autoremove'."
  :type '(repeat symbol)
  :group 'timu-bits)


;;; SETUP PACKAGE INSTALLATION
(defun timu-bits-install-packages ()
  "Check if all packages in `timu-bits-package-list' are installed.
Install the missing packages and if not.

Credit: https://github.com/bbatsov/prelude
Credit: https://chat.openai.com/chat."
  (interactive)
  (let ((missing-packages
         (cl-remove-if #'package-installed-p timu-bits-package-list)))
    (when missing-packages
      ;; check for new packages (package versions)
      (message "%s" "Reloading packages DB...")
      (package-refresh-contents)
      (message "%s" " done.")
      ;; install the missing packages
      (mapc #'package-install missing-packages))))

(defun timu-bits-reinstall-package (package)
  "Unload and re-install an Emacs PACKAGE.
Uses the custom variable `timu-bits-installed-packages'.
Credit: https://emacsredux.com/blog/2020/09/12/reinstalling-emacs-packages/"
  (interactive
   (list (intern
          (completing-read "Reinstall package: "
                           timu-bits-installed-packages))))
  (unload-feature package)
  (package-reinstall package)
  (require package))

(defun timu-bits-remove-removable-package ()
  "Uninstall package from the `timu-bits-removable-packages' list."
  (interactive)
  (let
      ((pack (completing-read "Uninstall which package: "
                              timu-bits-removable-packages)))
    (package-delete
     (cadr (assq (intern-soft pack) package-alist)))))

(defmacro timu-bits-require (&rest modules)
  "Load MODULES, if they are not already loaded and are installed.
Skips any modules that are not found.

Displays a message in the minibuffer for any modules that are not found.
Displays a message in the minibuffer for any modules that are already loaded.

Credit: https://chat.openai.com/chat."
  `(progn
     ,@(mapcar (lambda (module)
                 `(when (not (featurep ',module))
                    (if (locate-library ,(symbol-name module))
                        (progn
                          (require ',module)
                          (message "Loaded module '%s'" ',module))
                      (message "Module '%s' not found" ',module))))
               modules)))

(defun timu-bits-load ()
  "Load all custom the modules using `require'.
The order in which the modules are loaded is important."
  (interactive)
  (require 'timu-evil)
  (require 'timu-base)
  (require 'timu-personal)
  (with-eval-after-load 'dired
    (require 'timu-dired))
  (require 'timu-ui)
  (require 'timu-nav)
  (with-eval-after-load 'org
    (require 'timu-org)
    (require 'timu-roam)
    (require 'timu-modes)
    (require 'timu-fun)
    (require 'timu-map)
    (require 'timu-edit))
  (with-eval-after-load 'eshell
    (require 'timu-shell))
  (require 'timu-scratch)
  (pcase system-type
    ((or 'darwin 'gnu/linux)
     (require 'timu-pdf)
     (with-eval-after-load 'magit
       (require 'timu-git))
     (require 'timu-mu4e)
     (require 'timu-elfeed)
     (with-eval-after-load 'prog-mode
       (require 'timu-prog))
     (with-eval-after-load 'latex
       (require 'timu-latex))
     (require 'timu-work)
     (require 'timu-try))))


(provide 'timu-bits)

;;; timu-bits.el ends here
