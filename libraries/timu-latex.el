;;; timu-latex.el --- Custom LaTex settings -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 1.0
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-10-30
;; Keywords: functions tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the settings for working with LaTex.

;;; Code:


(require 'auctex-latexmk)
(require 'tex)


;;; DEFAULTS
(defgroup timu-latex ()
  "Customise group for the `timu-latex' Library."
  :group 'timu)

(defcustom timu-latex-path
  (expand-file-name "libraries/timu-latex.el" user-emacs-directory)
  "Variable for the path of the module `timu-latex'."
  :type 'file
  :group 'timu-latex)


;;; CONFIG
(setq TeX-auto-save t)
(setq TeX-parse-self t)

(customize-set-variable 'TeX-view-program-selection
                        '((output-pdf "PDF Tools")
                          (output-dvi "open")
                          (output-html "open")))
(customize-set-variable 'TeX-view-program-list
                        '(("PDF Tools" TeX-pdf-tools-sync-view)))

(add-hook 'LaTeX-mode-hook #'eglot-ensure)
(add-hook 'LaTeX-mode-hook #'visual-line-mode)
(add-hook 'LaTeX-mode-hook #'flyspell-mode)
(add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)

(add-to-list 'display-buffer-alist
             '("\\*tex-shell\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.30)))


;;; LATEX KEYBINDINGS
(keymap-set LaTeX-mode-map "M-m" #'TeX-command-master)
(keymap-set LaTeX-mode-map "M-r" #'TeX-command-run-all)
(keymap-set LaTeX-mode-map "M-p" #'timu-nav-project-switch-project)
(keymap-set LaTeX-mode-map "M-p" #'project-find-file)

(pcase system-type
  ('darwin (keymap-set LaTeX-mode-map "M-®" #'TeX-next-error))
  ('gnu/linux (keymap-set LaTeX-mode-map "C-s-r" #'TeX-next-error)))


(provide 'timu-latex)

;;; timu-latex.el ends here
