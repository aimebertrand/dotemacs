;;; timu-try.el --- Custom evil keybindings config -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2023-01-09
;; Keywords: tools testing playground
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains random stuff for testing.

;;; Code:


;;; DEFAULTS
(defgroup timu-try ()
  "Customise group for the `timu-try' Library."
  :group 'timu)

(defcustom timu-try-path
  (expand-file-name "libraries/timu-try.el" user-emacs-directory)
  "Variable for the path of the module `timu-try'."
  :type 'file
  :group 'timu-try)


;;; SF PRO SYMBOLS
(load-file (expand-file-name "local/packages/sf-helper.el" user-emacs-directory))
(load-file (expand-file-name "local/packages/sf.el" user-emacs-directory))


(provide 'timu-try)

;;; timu-try.el ends here
