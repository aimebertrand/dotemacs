;;; timu-work.el --- Custom configuration for work matters -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-02-15
;; Keywords: jira confluence tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my configuration for packages and more used at work.

;;; Code:


;;; DEFAULTS
(defgroup timu-work ()
  "Customise group for the `timu-work' Library."
  :group 'timu)

(defcustom timu-work-path
  (expand-file-name "libraries/timu-work.el" user-emacs-directory)
  "Variable for the path of the module `timu-work'."
  :type 'file
  :group 'timu-work)


;;; JIRA
;; `TODO' keywords translations
(customize-set-variable
 'org-jira-jira-status-to-org-keyword-alist
 '(("Backlog" . "BACKLOG")
   ("Waiting" . "WAITING")
   ("Withdrawn" . "WITHDRAWN")
   ("Zurückgezogen" . "ZURUECKGEZOGEN")))

(customize-set-variable
 'org-jira-default-jql
 "(assignee = currentuser() or reporter = currentuser()) and (resolution = unresolved or resolution is empty) and status != backlog and type != epic and type != 'standard change template' and status != done order by updated desc")


;;; WORK CAPTURE TEMPLATES
(add-to-list 'org-capture-templates
             '("w" "Work templates"))

(add-to-list 'org-capture-templates
             `("wt" "Work todo" entry
               (file+olp+datetree ,timu-personal-work-gtd-file)
               "* TODO [/][\%] - %^{task-title}
SCHEDULED: %^t DEADLINE: %^t
%?\n"))

(add-to-list 'org-capture-templates
             `("wd" "Work Daily tasks" entry
               (file+headline ,timu-personal-work-gtd-file "Daily Tasks")
               "* %^u - Work Daily Tasks
** TODO [/][\%] - Morning
- [ ] Check Mails
- [ ] Check Teams Channels
- [ ] Check Jira issues
- [ ] Check Wiki Blogs
** TODO [/][\%] - Noon
- [ ] Check Jira issues
** TODO [/][\%] - Evening
- [ ] Review Documentation
- [ ] Check Mails
- [ ] Check Teams Channels
- [ ] Check Jira issues
\n\n"))

(add-to-list 'org-capture-templates
             `("ww" "Work Weekly tasks" entry
               (file+headline ,timu-personal-work-gtd-file "Weekly Tasks")
               "* %^u - Work Weekly Tasks
** TODO [/][\%] - Morning
- [ ] Joan
- [ ] Meeting Rooms
\n\n"))

(add-to-list 'org-capture-templates
             `("wn" "Work notes" entry
               (file+headline ,timu-personal-work-notes-file "NOTES")
               "* %^{note-title} %^G
%U
[[%^C][with more info...]]
%?\n"))

(add-to-list 'org-capture-templates
             `("o" "Quick Outlook email capture" entry
               (file+headline ,timu-personal-work-gtd-file "Mail Follow Up")
               "* %i    :outlook:
SCHEDULED: %^t
%U
\n"))


(provide 'timu-work)

;;; timu-work.el ends here
