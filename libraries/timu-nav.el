;;; timu-nav.el --- Config for navigation -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper navigation
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for all thing navigation.

;;; Code:


(require 'consult)
(require 'emacs-everywhere)
(require 'embark)
(require 'neotree)
(require 'popper)
(require 'prog-mode)
(require 'treemacs)
(require 'treemacs-all-the-icons)
(require 'vertico)
(require 'vertico-directory)
(require 'xwidget)


;;; DEFAULTS
(defgroup timu-nav ()
  "Customise group for the `timu-nav' Library."
  :group 'timu)

(defcustom timu-nav-path
  (expand-file-name "libraries/timu-nav.el" user-emacs-directory)
  "Variable for the path of the module `timu-nav'."
  :type 'file
  :group 'timu-nav)


;;; FUNCTIONS
(defun timu-nav-make-frame ()
  "Create a new frame and run `scratch-buffer'.
This is mainly to call from everywhere in macOS."
       (interactive)
       (let ((frame-name
              (format "Frame #%d"
                      (+ 1 (length
                            (seq-filter #'frame-visible-p (frame-list)))))))
         (make-frame `((name . ,frame-name)))
         (select-frame-by-name frame-name)
         (delete-other-windows)
         (scratch-buffer)))

(defun timu-nav-generate-new-buffer ()
  "Create a new unnamed buffer."
  (interactive)
  (let ((buffer (generate-new-buffer "*new*")))
    (set-window-buffer nil buffer)))

(defun timu-nav-split-and-follow-below ()
  "Split the selected window in two with the new window is bellow.
This uses `split-window-below' but follows with the cursor."
  (interactive)
  (split-window-below)
  (other-window 1))

(defun timu-nav-split-and-follow-right ()
  "Split the selected window in two with the new window is to the right.
This uses `split-window-right' but follows with the cursor."
  (interactive)
  (split-window-right)
  (other-window 1))

(defun timu-nav-find-file-below ()
  "Open file with `find-file' & `read-file-name' in a split window bellow."
  (interactive)
  (split-window-below)
  (other-window 1)
  (find-file (read-file-name "Find file: " nil) t))

(defun timu-nav-find-file-right ()
  "Open file with `find-file' & `read-file-name' in a split window to the right."
  (interactive)
  (split-window-right)
  (other-window 1)
  (find-file (read-file-name "Find file: " nil) t))

(defun timu-nav-toggle-split-direction ()
  "Toggle window split from vertical to horizontal.
This work the other way around as well.
Credit: https://github.com/olivertaylor/dotfiles/blob/master/emacs/init.el"
  (interactive)
  (if (> (length (window-list)) 2)
      (error "Can't toggle with more than 2 windows")
    (let ((was-full-height (window-full-height-p)))
      (delete-other-windows)
      (if was-full-height
          (split-window-vertically)
        (split-window-horizontally))
      (save-selected-window
        (other-window 1)
        (switch-to-buffer (other-buffer))))))

(defun timu-nav-find-file-as-root ()
  "Like `find-file', but automatically edit the file with root-privileges.
This uses Tramp to apend sudo to path, if the file is not writable by user."
  (interactive)
  (let ((file (read-file-name "Open file as root: ")))
    (if (file-writable-p file)
        (progn
          (find-file file)
          (message "File is writable, no need to open as root."))
      (find-file (concat "/sudo::" file)))))

(defun timu-nav-tab-bar-new-tab ()
  "Create a new tab an then switch to the scratch buffer.
If it has been closed, then create one."
  (interactive)
  (tab-bar-new-tab)
  (scratch-buffer))

(defun timu-nav-switch-or-new-tab ()
  "Switch to the next tab if there are more than 1 tab.
Create a new tab if there are just 1 tab."
  (interactive)
  (if (length> (frame-parameter nil 'tabs) 1)
      (tab-bar-switch-to-next-tab)
    (timu-nav-tab-bar-new-tab)))

(defun timu-nav-popper-toggle-all ()
  "Toggle all pupups at once with `popper.el'."
  (interactive)
  (popper-toggle 16))

(defun timu-nav-consult-rg ()
  "Function for `consult-ripgrep' with the `universal-argument'."
  (interactive)
  (consult-ripgrep (list 4)))

(defun timu-nav-consult-fd ()
  "Function for `consult-find' with the `universal-argument'."
  (interactive)
  (consult-find (list 4)))

(defun timu-nav-project-switch-project (dir)
  "\"Switch\" to another project by running an Emacs command.
Directly use `project-find-file' instead of getting prompted.

When called in a program, it will use the project corresponding
to directory DIR."
  (interactive (list (project-prompt-project-dir)))
  (let ((project-current-directory-override dir))
    (project-find-file)))

(defun timu-nav-emacs-everywhere-copy ()
  "Copy the contents of the text area to be edited."
  (interactive)
  (do-applescript
   (concat
    "set frontmostApplication to path to frontmost application\n"
    "tell application \"System Events\"\n"
    "	keystroke \"c\" using {command down}\n"
    "end tell\n")))

(defun timu-nav-browser-split-window (url)
  "Browse the URL in a NEW-WINDOW to the right with `xwidget-webkit-browse-url'.
Or use the default browser.
Credit: https://github.com/konrad1977/emacs."
  (interactive)
  (let ((ignore-window-parameters t)
        (browser-function
         (completing-read "Browser: "
                          '("default" "xwidget"))))
    (if (equal "default" browser-function)
        (browse-url-default-browser url)
      (delete-other-windows)
      (split-window-horizontally)
      (other-window 1)
      (xwidget-webkit-goto-url url))))

(defun timu-nav-switch-url-browser ()
  "Toggle `browse-url-browser-function'.
The options:
- `browse-url-default-browser'
- `timu-nav-browser-split-window'"
  (interactive)
  (if (eq browse-url-browser-function 'timu-nav-browser-split-window)
      (setq browse-url-browser-function #'browse-url-default-browser)
    (setq browse-url-browser-function #'timu-nav-browser-split-window)))


;;; BUFFERS & FILES
(setq kill-buffer-query-functions
      (delq 'process-kill-buffer-query-function kill-buffer-query-functions))

(keymap-global-set "M-i" #'insert-file)
(keymap-global-set "M-n" #'timu-nav-generate-new-buffer)
(keymap-global-set "M-o" #'find-file)
(keymap-global-set "M-O" #'timu-nav-find-file-as-root)
(keymap-global-set "M-s" #'save-buffer)
(keymap-global-set "M-S" #'write-file)
(keymap-global-set "M-w" #'kill-this-buffer)
(keymap-global-set "C-M-s" #'save-some-buffers)

(keymap-global-set "M-q" #'save-buffers-kill-terminal)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "œ") #'timu-nav-find-file-below)
   (evil-global-set-key 'normal (kbd "æ") #'timu-nav-find-file-right)
   (evil-global-set-key 'normal (kbd "∫") #'eval-buffer))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-ö") #'timu-nav-find-file-below)
   (evil-global-set-key 'normal (kbd "s-ä") #'timu-nav-find-file-right)
   (evil-global-set-key 'normal (kbd "s-b") #'eval-buffer)))


;;; IBUFFER
;;;; expert-mode
;; killing buffers or groups without confirmation
(customize-set-variable 'ibuffer-expert t)

;;;; groups
(customize-set-variable
 'ibuffer-saved-filter-groups
 (quote (("default"
          ("custom" (mode . Custom-mode))
          ("dired" (mode . dired-mode))
          ("docker" (name . "^*docker-*"))
          ("elisp" (mode . emacs-lisp-mode))
          ("org" (mode . org-mode))
          ("git" (or (name . "^magit*")
                     (name . "^*Ediff*")
                     (name . "^*ediff-*")
                     (name . "^\\*forge: *")))
          ("help" (or (mode . helpful-mode)
                      (mode . help-mode)))
          ("mu4e" (or (mode . mu4e-main-mode)
                      (mode . mu4e-headers-mode)
                      (mode . mu4e-view-mode)
                      (mode . mu4e-compose-mode)
                      (name . "*mu4e-last-update*")))
          ("markdown" (mode . markdown-mode))
          ("pdf" (mode . pdf-view-mode))
          ("programming" (or (mode . python-mode)
                             (mode . go-mode)
                             (mode . sh-mode)))
          ("shell" (or (mode . eshell-mode)
                       (mode . shell-mode)
                       (name . "*ansi-term*")
                       (name . "*shell*")
                       (mode . vterm-mode)))
          ("Slack" (name . "^*Slack*"))
          ("tramp" (name . "^*tramp*"))
          ("web" (or (mode . web-mode)
                     (mode . js2-mode)))
          ("emacs" (or (name . "*scratch*")
                       (name . "*Messages*")))
          ("eglot" (name . "^\\*EGLOT *"))
          ("elfeed" (or (name . "*elfeed-log*")
                        (name . "*elfeed-entry-<*")
                        (name . "*elfeed-search*")))
          ("errors/logs" (or (name . "*Async-native-c*")
                             (name . "*Backtrace*")
                             (name . "Log.*")
                             (name . "log.*")
                             (name . "*Warnings*")))
          ("xwidget" (name . "^*xwidget*"))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (all-the-icons-ibuffer-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))

;; Don't show filter groups if there are no buffers in that group
(customize-set-variable 'ibuffer-show-empty-filter-groups nil)

;;;; ibuffer keybindings
(with-eval-after-load 'ibuffer
  (keymap-set ibuffer-mode-map "<remap> <ibuffer-backward-filter-group>"
	          #'timu-nav-project-switch-project)
  (keymap-set ibuffer-mode-map "S-<return>" #'ibuffer-visit-buffer-other-window))


;;; AVY
(evil-global-set-key 'normal (kbd "s") #'avy-goto-char-timer)


;;; POPPER
(customize-set-variable 'popper-reference-buffers
                        '("\\*Agenda Commands\\*"
                          "\\*Async Shell Command\\*"
                          "\\*Ilist\\*"
                          "\\*info\\*"
                          "^magit: *"
                          "^magit-diff: *"
                          "^magit-process: *"
                          "\\*Messages\\*"
                          "\\*mu4e-update\\*"
                          "\\*lsp-log\\*"
                          "Output\\*$"
                          "\\*osx-dictionary\\*"
                          "\\*pylsp.*stderr\\*"
                          "\\*pylsp\\*"
                          compilation-mode
                          eshell-mode
                          flycheck-error-list-mode
                          flymake-diagnostics-buffer-mode
                          flymake-project-diagnostics-mode
                          help-mode
                          helpful-mode
                          tldr-mode
                          vterm-mode))

(customize-set-variable 'popper-mode-line
                        '(:eval (propertize " POP " 'face 'mode-line-emphasis)))

(customize-set-variable 'popper-display-control nil)
(customize-set-variable 'popper-group-function #'popper-group-by-project)

(popper-mode +1)

(keymap-global-set "C-M-p" #'popper-toggle)
(keymap-global-set "M-P" #'popper-toggle-type)

(pcase system-type
  ('darwin (keymap-global-set "C-π" #'popper-cycle))
  ('windows-nt (keymap-global-set "C-s-p" #'popper-cycle))
  ('gnu/linux (keymap-global-set "M-s-p" #'popper-cycle)))


;;; PROJECT
(keymap-global-set "M-p" #'timu-nav-project-switch-project)

(pcase system-type
  ('darwin (keymap-global-set "M-π" #'project-find-file))
  ('windows-nt (keymap-global-set "M-s-p" #'project-find-file))
  ('gnu/linux (keymap-global-set "C-s-p" #'project-find-file)))


;;; DEMAP
(customize-set-variable 'demap-minimap-window-side 'right)
(customize-set-variable 'demap-minimap-window-width 40)


;;; RIPGREP
(pcase system-type
  ('darwin
   (keymap-global-set "C-ƒ" #'timu-nav-consult-fd)
   (keymap-global-set "M-ƒ" #'timu-nav-consult-rg))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "M-s-f" #'timu-nav-consult-fd)
   (keymap-global-set "C-s-f" #'timu-nav-consult-rg)))


;;; IMENU-LIST
(customize-set-variable 'imenu-list-position 'left)
(customize-set-variable 'imenu-list-size 0.25)

(add-to-list 'display-buffer-alist
             '("\\*Ilist\\*"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.20)))

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "⁄") #'imenu-list-smart-toggle))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-i") #'imenu-list-smart-toggle)))


;;; TAB-BAR NAVIGATION
(customize-set-variable 'tab-bar-show nil)

(keymap-global-set "M-t" #'timu-nav-switch-or-new-tab)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "˝") #'tab-bar-close-tab))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-T") #'tab-bar-close-tab)))


;;; ACE
(customize-set-variable 'aw-keys '(?u ?i ?o ?p ?h ?j ?k ?l))

(keymap-set evil-normal-state-map "ö" #'ace-window)
(keymap-set evil-normal-state-map "S" #'ace-link)
(evil-define-key 'normal Info-mode-map (kbd "S") #'ace-link)


;;; NEOTREE
(customize-set-variable 'neo-theme (if (display-graphic-p) 'icons 'arrow))

(keymap-set neotree-mode-map "M-RET" #'neotree-change-root)
(keymap-set neotree-mode-map "M-<up>" #'neotree-select-up-node)
(keymap-set neotree-mode-map "M-…" #'neotree-stretch-toggle)
(keymap-set neotree-mode-map "M-ø" #'neotree-dir)
(keymap-set neotree-mode-map "S-<return>"
            (neotree-make-executor :file-fn #'neo-open-file-ace-window))
(evil-define-key 'normal neotree-mode-map (kbd "æ")
  (neotree-make-executor :file-fn #'neo-open-file-vertical-split))
(evil-define-key 'normal neotree-mode-map (kbd "œ")
  (neotree-make-executor :file-fn #'neo-open-file-horizontal-split))

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "¿") #'neotree-toggle))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-ß") #'neotree-toggle)))


;;; TREEMACS
(add-hook 'after-init-hook
          (lambda ()
            (progn
              (treemacs-load-theme "all-the-icons"))))

(setq lsp-treemacs-errors-position-params
      `((side . right)
        (window-width . 0.33)))

(customize-set-variable
 'treemacs-persist-file
 (expand-file-name "treemacs-persist" temporary-file-directory))

;;;; treemacs transients
(transient-define-prefix timu-nav-treemacs-transient ()
  "Transient for TREEMACS."
  ["TREEMACS TRANSIENT COMMANDS\n"
   ["OPENING"
    ("RET" "Open file" treemacs-RET-action)
    ("S-<return>" "Open file with ace" treemacs-visit-node-horizontal-split)
    ("ä" "Open file horiz. split" treemacs-visit-node-horizontal-split)
    ("ö" "Open file vert. split" treemacs-visit-node-vertical-split)
    ("TAB" "Open Subtree" treemacs-TAB-action)
    ("o" "Open with ext App" treemacs-visit-node-in-external-application)]
   ["EDITING"
    ("n" "Create new file" treemacs-create-file)
    ("+" "Create new directory" treemacs-create-dir)
    ("d" "Delete files(s)" treemacs-delete)
    ("r" "Rename files(s)" treemacs-rename)
    ("." "Show dotfile(s)" treemacs-toggle-show-dotfiles)
    ("R" "Refresh" treemacs-refresh)]
   ["COPY/RENAME"
    ("c" "Copy file" treemacs-copy-file)
    ("m" "Move file" treemacs-move-file)
    ("M" "Move project" treemacs-rename-project)]
   ["WORKSPACES"
    ("w" "Create workspace" treemacs-create-workspace)
    ("e" "Edit workspace" treemacs-edit-workspaces)
    ("W" "Remove workspace" treemacs-remove-workspace)
    ("S" "Rename workspace" treemacs-rename-workspace)
    ("s" "Switch workspace" treemacs-switch-workspace)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set treemacs-mode-map "M-#" #'timu-nav-treemacs-transient))
  ('windows-nt
   (keymap-set treemacs-mode-map "M-$" #'timu-nav-treemacs-transient)))

(keymap-set treemacs-mode-map "S-<return>" #'treemacs-visit-node-ace)
(keymap-set treemacs-mode-map "M-<left>" #'windmove-left)
(keymap-set treemacs-mode-map "M-<right>" #'windmove-right)
(keymap-set treemacs-mode-map "M-<up>" #'windmove-up)
(keymap-set treemacs-mode-map "M-<down>" #'windmove-down)

(evil-define-key 'normal prog-mode-map
  (kbd "g e") #'lsp-treemacs-errors-list
  (kbd "g s") #'lsp-treemacs-symbols)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "‚") #'treemacs))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-s") #'treemacs)))


;;; EMACS-EVERYWHERE
(customize-set-variable 'emacs-everywhere-frame-parameters
                        '((name . "emacs-everywhere")
                          (width . 120)
                          (height . 30)))

(keymap-set emacs-everywhere-mode-map "M-w"  #'emacs-everywhere-abort)
(keymap-set emacs-everywhere-mode-map "M-s"  #'emacs-everywhere-finish)

(customize-set-variable 'emacs-everywhere-init-hooks
                        `(timu-nav-emacs-everywhere-copy
                          emacs-everywhere-insert-selection
                          org-mode
                          evil-insert-state))

(customize-set-variable 'emacs-everywhere-final-hooks
                        '(emacs-everywhere-remove-trailing-whitespace))


;;; WINNER-MODE
(add-hook 'after-init-hook 'winner-mode)


;;; XWIDGET WEBKIT
(remove-hook 'kill-buffer-query-functions #'xwidget-kill-buffer-query-function)
(keymap-set xwidget-webkit-mode-map "M-c" #'xwidget-webkit-copy-selection-as-kill)
(keymap-set xwidget-webkit-mode-map "M-l" #'xwidget-webkit-browse-url)
(keymap-set xwidget-webkit-mode-map "<normal-state> <left>" #'xwidget-webkit-back)
(keymap-set xwidget-webkit-mode-map "<normal-state> <right>" #'xwidget-webkit-forward)
(keymap-set xwidget-webkit-mode-map "<up>" #'xwidget-webkit-scroll-top)
(keymap-set xwidget-webkit-mode-map "<down>" #'xwidget-webkit-scroll-bottom)
(keymap-set xwidget-webkit-mode-map "M-r" #'xwidget-webkit-reload)
(keymap-set xwidget-webkit-mode-map "i" #'xwidget-webkit-insert-string)
(keymap-set xwidget-webkit-mode-map "M-+" #'xwidget-webkit-zoom-in)
(keymap-set xwidget-webkit-mode-map "M--" #'xwidget-webkit-zoom-out)


;;; XWWP
(keymap-set xwidget-webkit-mode-map "M-#" #'xwwp-follow-link)


;;; FRAMES & WINDOWS KEYBINDINGS
(global-unset-key (kbd "M-<left>"))
(global-unset-key (kbd "M-<right>"))
(global-unset-key (kbd "M-<up>"))
(global-unset-key (kbd "M-<down>"))

(keymap-global-set "M-<left>" #'windmove-left)
(keymap-global-set "M-<right>" #'windmove-right)
(keymap-global-set "M-<up>" #'windmove-up)
(keymap-global-set "M-<down>" #'windmove-down)

(keymap-set package-menu-mode-map "M-<left>" #'windmove-left)
(keymap-set package-menu-mode-map "M-<right>" #'windmove-right)
(keymap-set package-menu-mode-map "M-<up>" #'windmove-up)
(keymap-set package-menu-mode-map "M-<down>" #'windmove-down)


(keymap-global-set "M-2" #'ace-delete-window)
(keymap-global-set "M-N" #'timu-nav-make-frame)
(keymap-global-set "M-0" #'delete-window)
(keymap-global-set "M-1" #'delete-other-windows)

(pcase system-type
  ('darwin
   (keymap-global-set "M-…" #'enlarge-window-horizontally)
   (keymap-global-set "M-∞" #'shrink-window-horizontally)
   (keymap-global-set "M-±" #'enlarge-window)
   (keymap-global-set "M-–" #'shrink-window))
  ('gnu/linux
   (keymap-global-set "C-s-." #'enlarge-window-horizontally)
   (keymap-global-set "C-s-," #'shrink-window-horizontally)
   (keymap-global-set "C-s-+" #'enlarge-window)
   (keymap-global-set "C-s--" #'shrink-window))
  ('windows-nt
   (keymap-global-set "C-s-." #'enlarge-window-horizontally)
   (keymap-global-set "C-s-," #'shrink-window-horizontally)
   (keymap-global-set "C-s-+" #'enlarge-window)
   (keymap-global-set "C-s--" #'shrink-window)))

(pcase system-type
  ('darwin
   (keymap-global-set "M-‚" #'ace-swap-window)
   (keymap-global-set "M-∑" #'delete-frame)
   (keymap-global-set "M-≠" #'balance-windows)
   (keymap-global-set "M-ö" #'timu-nav-split-and-follow-below)
   (keymap-global-set "M-Ö" #'split-window-below)
   (keymap-global-set "M-ä" #'timu-nav-split-and-follow-right)
   (keymap-global-set "M-Ä" #'split-window-right)
   (keymap-global-set "M-œ" #'split-root-window-below)
   (keymap-global-set "M-æ" #'timu-nav-toggle-split-direction)
   (keymap-global-set "M-µ" #'bookmark-set)
   (keymap-set evil-normal-state-map "µ" #'demap-toggle))
  ('gnu/linux
   (keymap-global-set "C-s-s" #'ace-swap-window)
   (keymap-global-set "C-s-w" #'delete-frame)
   (keymap-global-set "C-s-0" #'balance-windows)
   (keymap-global-set "C-ö" #'timu-nav-split-and-follow-below)
   (keymap-global-set "C-Ö" #'split-window-below)
   (keymap-global-set "C-ä" #'timu-nav-split-and-follow-right)
   (keymap-global-set "C-Ä" #'split-window-right)
   (keymap-global-set "C-s-ö" #'split-root-window-below)
   (keymap-global-set "C-s-ä" #'timu-nav-toggle-split-direction)
   (keymap-global-set "M-m" #'bookmark-set)
   (keymap-global-set "s-m" #'demap-toggle))
  ('windows-nt
   (keymap-global-set "C-s-s" #'ace-swap-window)
   (keymap-global-set "C-s-w" #'delete-frame)
   (keymap-global-set "C-s-0" #'balance-windows)
   (keymap-global-set "C-ö" #'timu-nav-split-and-follow-below)
   (keymap-global-set "C-Ö" #'split-window-below)
   (keymap-global-set "C-ä" #'timu-nav-split-and-follow-right)
   (keymap-global-set "C-Ä" #'split-window-right)
   (keymap-global-set "C-s-ö" #'split-root-window-below)
   (keymap-global-set "C-s-ä" #'timu-nav-toggle-split-direction)
   (keymap-global-set "C-s-m" #'bookmark-set)
   (keymap-global-set "s-m" #'demap-toggle)))


;;; VERTICO & CONSULT CONFIG
(vertico-mode)
(recentf-mode)

(setq completion-category-defaults nil)
(customize-set-variable 'completion-category-overrides '((file (styles partial-completion))))
(customize-set-variable 'consult-async-min-input 1)
(customize-set-variable 'consult-line-start-from-top t)
(customize-set-variable 'completion-styles '(orderless))
(customize-set-variable 'consult-project-root-function #'vc-root-dir)

(pcase system-type
  ('windows-nt
   (customize-set-variable 'consult-find-args
                           "wsl find . -not ( -wholename */.* -prune )")
   (customize-set-variable 'consult-git-grep-args
                           "wsl git --no-pager grep --null --color=never
--ignore-case --extended-regexp --line-number -I")
   (customize-set-variable 'consult-grep-args
                           "wsl grep --null --line-buffered --color=never
--ignore-case --exclude-dir=.git --line-number -I -r .")
   (customize-set-variable 'consult-locate-args
                           "wsl locate --ignore-case --existing")
   (customize-set-variable 'consult-man-args "wsl man -k")
   (customize-set-variable 'consult-ripgrep-args
                           "wsl rg --null --line-buffered --color=never
--max-columns=1000 --path-separator /
--smart-case --no-heading --line-number .")))

;;;; tidy shadowed file names
(add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)

;;;; vertico & consult keybindings
(keymap-global-set "M-f" #'consult-line)
(keymap-global-set "M-b" #'consult-buffer)
(keymap-global-set "M-V" #'consult-yank-pop)

(keymap-set vertico-map "M-v" #'yank)
(keymap-set vertico-map "M-e" #'embark-act)
(keymap-set vertico-map "M-E" #'embark-export)
(keymap-set vertico-map "<tab>" #'vertico-directory-enter)
(keymap-set vertico-map "<tab>" #'vertico-directory-enter)
(keymap-set vertico-map "<escape>" #'abort-recursive-edit)
(keymap-set minibuffer-local-filename-completion-map "<backspace>" #'vertico-directory-up)
(keymap-set minibuffer-local-map "<backspace>" #'vertico-directory-delete-char)
(keymap-set minibuffer-local-map "S-<backspace>" #'vertico-directory-delete-word)

(pcase system-type
  ('darwin
   (keymap-global-set "M-∫" #'consult-bookmark)
   (evil-global-set-key 'normal (kbd "ç") #'consult-recent-file))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "C-s-b" #'consult-bookmark)
   (keymap-global-set "s-c" #'consult-recent-file)))

;;;; consult live preview
;; https://github.com/minad/consult#live-previews:
(consult-customize
 consult-ripgrep consult-git-grep consult-grep consult-buffer
 consult-bookmark consult-recent-file consult-xref
 consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
 :preview-key nil)


;;; EMBARK
(customize-set-variable 'embark-prompter 'embark-completing-read-prompter)

(customize-set-variable 'embark-indicators
                        '(embark-minimal-indicator
                          embark-highlight-indicator
                          embark-isearch-highlight-indicator))

(keymap-set embark-symbol-map "h" #'helpful-symbol)
(keymap-set embark-collect-mode-map "M-<left>" #'windmove-left)
(keymap-set embark-collect-mode-map "M-<right>" #'windmove-right)
(keymap-set embark-collect-mode-map "M-<up>" #'windmove-up)
(keymap-set embark-collect-mode-map "M-<down>" #'windmove-down)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "€") #'embark-act)))


;;; MARGINALIA
(marginalia-mode)


;;; PRESCIENT
(customize-set-variable
 'prescient-save-file
 (expand-file-name "local/var/prescient-save.el" user-emacs-directory))
(corfu-prescient-mode 1)
(vertico-prescient-mode 1)


;;; WHICH-KEY
(which-key-mode)


;;; MINIBUFFER KEYBINDINGS
(keymap-set minibuffer-local-map "M-v" #'yank)
(keymap-set minibuffer-local-map "<escape>" #'abort-recursive-edit)
(keymap-set minibuffer-local-map "M-RET" #'newline)
(keymap-set minibuffer-mode-map "M-RET" #'newline)
(keymap-set minibuffer-mode-map "M-." #'abort-recursive-edit)


(provide 'timu-nav)

;;; timu-nav.el ends here
