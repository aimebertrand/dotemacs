;;; timu-modes.el --- Config for several major modes -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: languages tools helper major modes
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for several major modes
;; that do not warrant e separate module.

;;; Code:


(require 'chatgpt-shell)
(require 'conf-mode)
(require 'dall-e-shell)
(require 'edit-indirect)
(require 'elisp-demos)
(require 'expenses)
(require 'gptel)
(require 'markdown-mode)
(require 'monkeytype)
(require 'ob-applescript)
(require 'ob-chatgpt-shell)
(require 'ob-dall-e-shell)
(require 'toml-mode)
(require 'wgrep)


;;; DEFAULTS
(defgroup timu-modes ()
  "Customise group for the `timu-modes' Library."
  :group 'timu)

(defcustom timu-modes-path
  (expand-file-name "libraries/timu-modes.el" user-emacs-directory)
  "Variable for the path of the module `timu-modes'."
  :type 'file
  :group 'timu-modes)

(defcustom timu-modes-monkeytype-word-file
  (expand-file-name "monkeytype-words.txt" user-emacs-directory)
  "Variable for the `monkeytype' word list."
  :type 'file
  :group 'timu-modes)


;;; FUNCTIONS
(defun timu-modes-outline-go-to-heading (&optional heading)
  "Go to an outline HEADING with `consult-outline'.
Also move the heading to the top of the buffer with `evil-scroll-line-to-top'"
  (interactive)
  (consult-outline)
  (evil-scroll-line-to-top heading))

(defun timu-modes-outline-hide-sublevels ()
  "Go to the top of the file then `outline-hide-sublevels'.
This hides everything but the top LEVELS levels of headers, in whole buffer."
  (interactive)
  (goto-char (point-min))
  (call-interactively #'outline-hide-sublevels))

(defun timu-modes-toml-go-to-heading (&optional heading)
  "Go to an toml \"HEADING\" with `consult-line and regexp'.
Also move the heading to the top of the buffer with `evil-scroll-line-to-top'"
  (interactive)
  (consult-line "^\[:blank:\]?\\[.*\\]")
  (evil-scroll-line-to-top heading))

(defun timu-modes-expenses-add-expense (user)
  "Add expense for an USER."
  (interactive (list ()))
  (expenses-user-dir user)
  (let* ((date (org-read-date nil nil nil "Date: "))
         (amount (read-number "Amount: "))
         (category (completing-read "Category: " expenses-category-list))
         (details (completing-read "Details: " (expenses--get-frequently-used-details-list date user)))
         (file-name (expenses--get-file-name date user)))
    (when (string-blank-p category)
      (setq category "Others"))
    (when (not (file-exists-p file-name))
      (expenses--create-initial-file date user))
    (with-temp-buffer
      (insert (format "|%s |%.2f |%s |%s |\n" date amount category details))
      (when expenses-add-hline-in-org (insert "|--|--|--|--|\n"))
      (append-to-file (point-min) (point-max) file-name))
    (when (string-equal (completing-read "Add another expense: " '("no" "yes")) "yes")
      (expenses-add-expense user))
    (with-current-buffer (find-file-noselect file-name)
      (goto-char (point-max))
      (forward-line -1)
      (org-table-align)
      (write-file file-name))))

(defun timu-modes-chatgpt-in-new-tab ()
  "Open a ChatGPT chat window in a new tab with `tab-bar-new-tab'."
  (interactive)
  (tab-bar-new-tab)
  (chatgpt-arcana-start-chat (read-string "Your prompt for ChatGPT: ")))

(defun timu-modes-view-text-file-as-info-manual ()
  "View `info’, `texi’, `org’ and `md’ files as `Info’ manual.
This command uses the `pandoc' executable and org‘s texinfo exporter.
Credit: https://emacsnotes.wordpress.com."
  (interactive)
  (require 'ox-texinfo)
  (let ((org-export-with-broken-links 'mark))
    (pcase (file-name-extension (buffer-file-name))
      (`"info"
       (info (buffer-file-name)))
      (`"texi"
       (info (org-texinfo-compile (buffer-file-name))))
      (`"org"
       (info (org-texinfo-export-to-info)))
      (`"md"
       (let ((org-file-name (concat (file-name-sans-extension (buffer-file-name)) ".org")))
         (apply #'call-process "pandoc" nil standard-output nil
                `("-f" "markdown"
                  "-t" "org"
                  "-o" , org-file-name
                  , (buffer-file-name)))
         (with-current-buffer (find-file-noselect org-file-name)
           (info (org-texinfo-export-to-info)))))
      (_ (user-error "Don't know how to convert `%s' to an `info' file"
                     (file-name-extension (buffer-file-name)))))))

(defun timu-modes-monkeytype-load-and-start ()
  "Use `emacs-monkeytype' with random words from `Wiktionary' Frequency lists.
- Credit: https://drewsh.com/monkeytype-emacs-workflow."
  (interactive)
  (let ((res '())
        (final-buffer "*Monkeytype-words*")
        (true-num-words 20)
        (num-buffer-words nil)
        (indices nil))
    (with-temp-buffer
      (insert-file-contents
       (expand-file-name timu-modes-monkeytype-word-file))
      (setq num-buffer-words (count-words (point-min) (point-max)))
      (setq indices
            (sort (cl-loop for i from 0 below true-num-words
                           collect (random (- num-buffer-words i))) '<))
      (setq res
            (cl-loop repeat true-num-words
                     for idx in indices
                     collect (progn
                               (goto-char (point-min))
                               (forward-word idx)
                               (let ((word-to-return
                                      (string-trim
                                       (buffer-substring-no-properties
                                        (point)
                                        (progn (forward-word) (point))))))
                                 (kill-word -1)
                                 word-to-return)))))
    (with-current-buffer (get-buffer-create final-buffer)
      (erase-buffer)
      (insert (mapconcat 'identity res " ")))
    (switch-to-buffer final-buffer)
    (monkeytype-buffer)
    (kill-buffer "*Monkeytype-words*")))


;;; EMACS LISP MODE
(pcase system-type
  ('darwin
   (evil-define-key 'normal emacs-lisp-mode-map (kbd "≈") #'eval-defun)
   (evil-define-key 'normal lisp-interaction-mode-map (kbd "≈") #'eval-defun))
  ('gnu/linux
   (keymap-set emacs-lisp-mode-map "s-x" #'eval-defun)
   (keymap-set lisp-interaction-mode-map "s-x" #'eval-defun)))

(add-to-list 'auto-mode-alist '("\\.el\\.gz\\'" . emacs-lisp-mode))


;;; ELISP-DEMOS
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)


;;; INFO MODE
(evil-define-key 'normal Info-mode-map
  (kbd "/") #'Info-search
  (kbd "b") #'Info-backward-node
  (kbd "f") #'Info-forward-node
  (kbd "h") #'Info-history-back
  (kbd "l") #'Info-history-forward
  (kbd "t") #'Info-top-node
  (kbd "u") #'Info-up)


;;; OUTLINE MINOR MODE
(keymap-set outline-mode-map "<normal-state> M-h" nil)
(keymap-set outline-mode-map "<normal-state> M-j" nil)
(keymap-set outline-mode-map "<normal-state> M-k" nil)
(keymap-set outline-mode-map "<normal-state> M-l" nil)

(add-hook 'emacs-lisp-mode-hook #'outline-minor-mode)
(evil-define-key 'normal outline-minor-mode-map
  (kbd "<tab>") #'outline-toggle-children
  (kbd "S-<tab>") #'outline-cycle-buffer
  (kbd "<backtab>") #'outline-cycle-buffer
  (kbd "z s") #'outline-show-all
  (kbd "z h") #'timu-modes-outline-hide-sublevels
  (kbd "z c") #'outline-hide-subtree
  (kbd "z o") #'outline-show-subtree)
(keymap-set outline-minor-mode-map "C-c C-n" #'outline-next-heading)
(keymap-set outline-minor-mode-map "M-∂" #'outline-next-heading)
(keymap-set outline-minor-mode-map "C-c C-p" #'outline-previous-heading)
(keymap-set outline-minor-mode-map "M-¨" #'outline-previous-heading)
(keymap-set outline-minor-mode-map "<remap> <outline-demote>" #'org-insert-link)

(add-hook 'emacs-lisp-mode-hook (lambda ()
                                  (setq-local outline-regexp "^;;;.*")))

(pcase system-type
  ('darwin
   (keymap-set emacs-lisp-mode-map "M-ª" #'timu-modes-outline-go-to-heading))
  ('gnu/linux
   (keymap-set emacs-lisp-mode-map "C-s-h" #'timu-modes-outline-go-to-heading))
  ('windows-nt
   (keymap-set emacs-lisp-mode-map "C-s-h" #'timu-modes-outline-go-to-heading)))


;;; OUTLINE MINOR FACES
(add-hook 'outline-minor-mode-hook #'outline-minor-faces-mode)


;;; MARKDOWN
;;;; markdown-mode install and setup
(customize-set-variable 'markdown-command "multimarkdown")

(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))

(pcase system-type
  ('darwin
   (keymap-set markdown-mode-map "‘" #'markdown-edit-code-block)
   (keymap-set edit-indirect-mode-map "‘" #'edit-indirect-commit))
  ((or 'gnu/linux 'windows-nt)
   (keymap-set markdown-mode-map "s-$" #'markdown-edit-code-block)
   (keymap-set edit-indirect-mode-map "s-$" #'edit-indirect-commit)))


;;; EMACS-MONKEYTYPE
(customize-set-variable
 'timu-modes-monkeytype-word-file
 (expand-file-name "local/monkeytype-words.txt" user-emacs-directory))
(customize-set-variable 'timu-line-show-monkeytype-stats t)

(add-hook 'monkeytype-mode-hook (lambda ()
                                  (text-scale-adjust +4)
                                  (evil-insert 1)))


;;; TOML
(pcase system-type
  ('darwin
   (keymap-set conf-toml-mode-map "M-ª" #'timu-modes-toml-go-to-heading)
   (keymap-set toml-mode-map "M-ª" #'timu-modes-toml-go-to-heading))
  ('gnu/linux
   (keymap-set conf-toml-mode-map "C-s-h" #'timu-modes-toml-go-to-heading)
   (keymap-set toml-mode-map "C-s-h" #'timu-modes-toml-go-to-heading))
  ('windows-nt
   (keymap-set conf-toml-mode-map "C-s-h" #'timu-modes-toml-go-to-heading)
   (keymap-set toml-mode-map "C-s-h" #'timu-modes-toml-go-to-heading)))


;;; WEB-MODE
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode))


;;; REQUEST
(customize-set-variable
 'request-storage-directory
 (expand-file-name "local/request" user-emacs-directory))


;;; WGREP
(keymap-set wgrep-mode-map "M-s" #'wgrep-finish-edit)
(keymap-set wgrep-mode-map "M-w" #'wgrep-abort-changes)


;;; OCCUR
(keymap-set occur-edit-mode-map "M-s" #'occur-cease-edit)


;;; ANSIBLE
(add-hook 'yaml-mode-hook #'ansible-doc-mode)


;;; CSV
(add-to-list 'auto-mode-alist '("\\.csv\\'" . csv-mode))
(add-hook 'csv-mode-hook #'csv-align-mode)

;;;; rainbow-csv
(quelpa '(rainbow-csv
          :fetcher github
          :repo "emacs-vs/rainbow-csv"))

(add-hook 'csv-mode-hook #'rainbow-csv-mode)
(add-hook 'tsv-mode-hook #'rainbow-csv-mode)


;;; POWERSHELL
(add-to-list 'auto-mode-alist '("\\.ps[dm]?1\\'" . powershell-mode))

(customize-set-variable 'org-babel-powershell-os-command "pwsh")


;;; YAML
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-mode))


;;; SYSLOG-MODE
(add-to-list 'auto-mode-alist '("\\.log\\'" . syslog-mode))


;;; VIMRC-MODE
(add-to-list 'auto-mode-alist '("\\.vim\\(rc\\)?\\'" . vimrc-mode))


;;; SHELL-SCRIPT-MODE
(add-to-list 'auto-mode-alist '("\\.zsh\\'" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zshrc" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zshenv" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zprofile" . sh-mode))


;;; DOCKER
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))

(add-to-list 'display-buffer-alist
             '("*docker-*"
               (display-buffer-below-selected)
               (window-height . 0.5)))


;;; EXPENSES
(customize-set-variable 'expenses-currency "CHF")
(customize-set-variable 'expenses-directory "~/org/expenses/")
(customize-set-variable
 'expenses-category-list
 '("Crafting" "Electronics" "Entertainment" "Food" "Grocery" "Health"
   "Others" "Rent" "Salary" "Shopping" "Subscription" "Travel"))
(customize-set-variable
 'expenses-month-names
 '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))


;;; OSX-DICTIONARY
(add-to-list 'display-buffer-alist
             '("\\*osx-dictionary\\*"
               (display-buffer-below-selected)
               (window-width . 0.4)))


;;; TLDR
(customize-set-variable 'tldr-directory-path
                        (expand-file-name "local/tldr/" user-emacs-directory))

(add-to-list 'display-buffer-alist
             '("\\*tldr\\*"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.4)))


;;; ERC
(customize-set-variable 'erc-server "irc.libera.chat:6697")
(customize-set-variable 'erc-nick "aimebertrand")
(customize-set-variable 'erc-kill-buffer-on-part t)
(customize-set-variable 'erc-prompt-for-password nil)
(customize-set-variable 'erc-auto-query 'bury)
(customize-set-variable 'erc-track-shorten-start 8)


;;; GPTEL
(setq gptel-default-mode #'org-mode)

(customize-set-variable 'gptel-playback t)
(customize-set-variable 'gptel-model "gpt-4o")

(customize-set-variable 'gptel-prompt-prefix-alist
                        '((markdown-mode . "### ")
                          (org-mode . "* ")
                          (text-mode . "### ")))

(add-hook 'gptel-mode-hook #'evil-insert-state)

(customize-set-variable
 'gptel-api-key
 (shell-command-to-string
  "echo -n $(security find-generic-password -s ChatGPT-token -a ChatGPT -w)"))

(keymap-set gptel-mode-map "S-<return>" #'newline)
(keymap-set gptel-mode-map "<return>" #'gptel-send)


;;; CHATGPT-SHELL & DALL-E-SHELL
(customize-set-variable 'dall-e-shell-image-output-directory "~/Downloads/")
(customize-set-variable 'chatgpt-shell-root-path
                        (expand-file-name "local/chatgpt/" user-emacs-directory))
(customize-set-variable 'chatgpt-shell-model-version 0)

(customize-set-variable
 'chatgpt-shell-openai-key
 (shell-command-to-string
  "echo -n $(security find-generic-password -s ChatGPT-token -a ChatGPT -w)"))
(customize-set-variable
 'dall-e-shell-openai-key
 (shell-command-to-string
  "echo -n $(security find-generic-password -s ChatGPT-token -a ChatGPT -w)"))


;;; KQL-MODE
(add-to-list 'auto-mode-alist '("\\.kql\\'" . kql-mode))


(provide 'timu-modes)

;;; timu-modes.el ends here
