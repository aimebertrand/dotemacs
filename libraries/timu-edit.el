;;; timu-edit.el --- Config for writing -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper writing editor
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for all thing writing.

;;; Code:


(require 'tree-sitter)
(require 'tree-sitter-langs)
(require 'flyspell)
(require 'writeroom-mode)


;;; DEFAULTS
(defgroup timu-edit ()
  "Customise group for the `timu-edit' Library."
  :group 'timu)

(defcustom timu-edit-path
  (expand-file-name "libraries/timu-edit.el" user-emacs-directory)
  "Variable for the path of the module `timu-edit'."
  :type 'file
  :group 'timu-edit)


;;; FUNCTIONS
(defun timu-edit-rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew file/buffer name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

(defun timu-edit-delete-file-and-buffer ()
  "Delete the current file, and kill the buffer."
  (interactive)
  (unless (buffer-file-name)
    (error "No file is currently being edited"))
  (delete-file (buffer-file-name) t)
  (kill-this-buffer))

(defun timu-edit-move-buffer-file (dir)
  "Move both current buffer and file it's visiting to DIR."
  (interactive "DMove to directory: ")
  (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (dir
          (if (string-match dir "\\(?:/\\|\\\\)$")
              (substring dir 0 -1) dir))
         (newname (concat dir "/" name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (progn
        (copy-file filename newname 1)
        (delete-file filename)
        (set-visited-file-name newname)
        (set-buffer-modified-p nil) t))))

(defun timu-edit-open-current-file-with ()
  "Open the file in the variable `buffer-file-name' with a macOS App.
This uses `completing-read' to select the App."
  (interactive)
  (let* ((apps-list
          (append (directory-files "/System/Applications" nil "\\.app$")
                  (directory-files "/Applications" nil "\\.app$")))
         (selected-app (completing-read "Choose an application: " apps-list)))
    (shell-command (format "open %s -a '%s'" (buffer-file-name) selected-app))))

(defun timu-edit-insert-line-before (lines)
  "Insert a newline above the line containing the cursor.
The number of LINES is controlled by the `prefix-arg'.
Credit: https://gist.github.com/onimenotsuki/adb1725ceddaeac026fc47bed30f64f6."
  (interactive "p")
  (save-excursion
    (move-beginning-of-line 1)
    (newline lines)))

(defun timu-edit-insert-line-after (lines)
  "Insert a newline below the line containing the cursor.
The number of LINES is controlled by the `prefix-arg'.
Credit: https://gist.github.com/onimenotsuki/adb1725ceddaeac026fc47bed30f64f6."
  (interactive "p")
  (save-excursion
    (move-end-of-line 1)
    (open-line lines)))

(defun timu-edit-wrap-selected-lines-at-79 ()
  "Wrap selected text to 79 characters per line.
Do this without splitting words or adding extra spaces."
  (interactive)
  (save-excursion
    (let ((fill-column 79))
      (fill-region (region-beginning) (region-end)))))

(defun timu-edit-wrap-selected-lines ()
  "Wrap selected text to a width of 70-120 characters.
without splitting words or adding extra spaces."
  (interactive)
  (let* ((widths (number-sequence 70 120))
         (default-width 79)
         (fill-column
          (completing-read "Wrap at width: "
                           (mapcar 'number-to-string widths)
                           nil t nil nil (number-to-string default-width))))
    (setq fill-column (string-to-number fill-column))
    (save-excursion
      (fill-region (region-beginning) (region-end) nil fill-column))))

(defun timu-edit-snake-case (&optional txt)
  "Replace spaces in selected TXT into underscores."
  (interactive)
  (kmacro-exec-ring-item
   '([?\C-x ?r ?e ?p ?l ?a ?c ?e ?- ?r ?e ?g ?e ?x ?p
            return ?  return ?_ return] 0 "%d") txt))

(defun timu-edit-load-if-exists (file)
  "Load FILE only if it exists and is readable using `file-readable-p'.
Credit: https://github.com/zamansky/using-emacs/blob/master/myinit.org."
  (if (file-readable-p file)
      (load-file file)))

(defun timu-edit-revert-buffer ()
  "Call `revert-buffer' with the NOCONFIRM argument set."
  (interactive)
  (revert-buffer nil t))

(defun timu-edit-toggle-line-number-display ()
  "Toggle line numbering between absolute and relative.
Credit: https://emacs.stackexchange.com/a/63148/30874."
  (interactive)
  (if (eq display-line-numbers 'relative)
      (setq display-line-numbers t)
    (setq display-line-numbers 'relative)))

(defun timu-edit-insert-date ()
  "Insert current date in either ISO, long or short format.
Possible forms:
- 1996-12-26
- 1996-12-26T14:30:30+0100
- December 26, 1996
- December 26, 1996 - 14:30
- 26.12.1996
- 1996.12.26 14:30
"
  (interactive)
  (let ((date-format
         (completing-read
          "Which shell:"
          '("1996-12-26" "1996-12-26T14:30:30+0100"
            "December 26, 1996" "December 26, 1996 - 14:30"
            "26.12.1996" "1996.12.26 14:30"))))
    (cond
     ((equal date-format "1996-12-26")
      (insert (format-time-string "%F")))
     ((equal date-format "1996-12-26T14:30:30+0100")
      (insert (format-time-string "%FT%T%z")))
     ((equal date-format "December 26, 1996")
      (insert (format-time-string "%B %d, %Y")))
     ((equal date-format "December 26, 1996 - 14:30")
      (insert (format-time-string "%B %d, %Y - %H:%M")))
     ((equal date-format "26.12.1996")
      (insert (format-time-string "%d.%m.%Y")))
     ((equal date-format "1996.12.26 14:30")
      (insert (format-time-string "%d.%m.%Y %H:%M"))))))

(defun timu-edit-yas-insert-snippet ()
  "Switch to `evil-insert-state' and insert a yasnippet."
  (interactive)
  (evil-insert-state)
  (yas-insert-snippet))

(defun timu-edit-insert-macos-keyboard-symbol  ()
  "Insert a macOS keyboard symbol using `completing-read'.
See https://support.apple.com/en-us/HT201236."
  (interactive)
  (insert (completing-read "Choose macOS Keyboard Symbol: "
                           '("⌘" "⌥" "⌃" "⇧" "⇪"))))

(defun timu-edit-make-buffer-file-executable ()
  "Make current buffer file executable.
A Variation of `executable-make-buffer-file-executable-if-script-p'."
  (interactive)
  (let* ((current-mode (file-modes (buffer-file-name)))
         (add-mode (logand ?\111 (default-file-modes))))
    (or (/= (logand ?\111 current-mode) 0)
        (zerop add-mode)
        (set-file-modes (buffer-file-name)
                        (logior current-mode add-mode)))))


;;; EDITOR IN WINDOW SETTINGS
;;;; custom settings for the editor part
(customize-set-variable 'line-spacing 0.3)
(global-hl-line-mode 1)
(customize-set-variable 'scroll-conservatively 100)
(customize-set-variable 'scroll-preserve-screen-position 1)

;;;; no tabs
(customize-set-variable 'indent-tabs-mode nil)
(customize-set-variable 'tab-width 4)

;;;; UTF-8 encoding
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;;;; preserve contents of system clipboard
;; Puts System clipboard stuff in the kill ring
(customize-set-variable 'save-interprogram-paste-before-kill t)

;;;; visual-line-mode
(add-hook 'prog-mode-hook #'visual-line-mode)
(add-hook 'text-mode-hook #'visual-line-mode)

;;;; line numbers
(global-display-line-numbers-mode)
(setq display-line-numbers-type t)

;;;; line width
(setq-default fill-column 79)

;;;; delete trailing whitespace
(add-hook 'before-save-hook #'delete-trailing-whitespace)


;;; FLYSPELL
(customize-set-variable 'ispell-program-name (executable-find "enchant-2"))
(customize-set-variable 'flyspell-correct-interface #'flyspell-correct-completing-read)

(add-hook 'flyspell-mode-hook
          (lambda ()
            (auto-dictionary-mode 1)))

(add-hook 'prog-mode-hook #'flyspell-prog-mode)

(keymap-global-set "C-M-l"  #'ispell-change-dictionary)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "ƒ") #'flyspell-correct-wrapper))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "s-f" #'flyspell-correct-wrapper)))


;;; POWERTHESAURUS

;;; YASNIPPET
(yas-global-mode 1)

(customize-set-variable
 'yas-snippet-dirs
 (list (expand-file-name "snippets" user-emacs-directory)
       (expand-file-name "snippets" timu-personal-directory)))

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "¥") #'timu-edit-yas-insert-snippet))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "s-y" #'timu-edit-yas-insert-snippet)))


;;; WRITEROOM-MODE
(customize-set-variable 'writeroom-mode-line t)
(customize-set-variable 'writeroom-width 120)
(customize-set-variable 'writeroom-fullscreen-effect 'maximized)
(customize-set-variable 'writeroom-mode-line nil)

(pcase system-type
  ('darwin (keymap-global-set "C-∑" #'writeroom-mode))
  ('gnu/linux (keymap-global-set "M-s-w" #'writeroom-mode))
  ('windows-nt (keymap-global-set "C-s-w" #'writeroom-mode)))

(pcase system-type
  ('darwin
   (keymap-set writeroom-mode-map "M-…" #'writeroom-increase-width)
   (keymap-set writeroom-mode-map "M-∞" #'writeroom-decrease-width))
  ((or 'gnu/linux 'windows-nt)
   (keymap-set writeroom-mode-map "C-s-." #'writeroom-increase-width)
   (keymap-set writeroom-mode-map "C-s-," #'writeroom-decrease-width)))


;;; EDITING KEYBINDINGS
(pcase system-type
  ('darwin
   (keymap-global-set "C-∞" #'comment-line)
   (keymap-global-set "C-@" #'timu-edit-toggle-line-number-display)
   (keymap-global-set "C-µ" #'timu-edit-move-buffer-file)
   (keymap-global-set "C-ø" #'timu-edit-open-current-file-with)
   (keymap-global-set "C-®" #'timu-edit-rename-file-and-buffer)
   (keymap-global-set "C-≈" #'timu-edit-make-buffer-file-executable)
   (keymap-global-set "C-†" #'powerthesaurus-transient)
   (keymap-global-set "M-®" #'timu-edit-revert-buffer))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "C-s-;" #'comment-line)
   (keymap-global-set "C-s-l" #'timu-edit-toggle-line-number-display)
   (keymap-global-set "C-s-m" #'timu-edit-move-buffer-file)
   (keymap-global-set "C-s-o" #'timu-edit-open-current-file-with)
   (keymap-global-set "C-s-r" #'timu-edit-rename-file-and-buffer)
   (keymap-global-set "C-s-x" #'timu-edit-make-buffer-file-executable)
   (keymap-global-set "C-s-r" #'timu-edit-revert-buffer)))

(keymap-global-set "M-a" #'mark-whole-buffer)
(keymap-global-set "M-DEL" #'timu-edit-delete-file-and-buffer)

(pcase system-type
  ('darwin
   (evil-define-key 'normal prog-mode-map
     (kbd "Ø") #'timu-edit-insert-line-before
     (kbd "ø") #'timu-edit-insert-line-after)
   (evil-define-key 'normal text-mode-map
     (kbd "Ø") #'timu-edit-insert-line-before
     (kbd "ø") #'timu-edit-insert-line-after))
  ((or 'gnu/linux 'windows-nt)
   (evil-define-key 'normal prog-mode-map
     (kbd "s-O") #'timu-edit-insert-line-before
     (kbd "s-o") #'timu-edit-insert-line-after)
   (evil-define-key 'normal text-mode-map
     (kbd "s-O") #'timu-edit-insert-line-before
     (kbd "s-o") #'timu-edit-insert-line-after)))


(provide 'timu-edit)

;;; timu-edit.el ends here
