;;; timu-base.el --- Defaults for my configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-03-27
;; Keywords: defaults tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my defaults to be loaded in the beginning.

;;; Code:


(require 'server)
(require 'transient)


;;; DEFAULTS
(defgroup timu-base ()
  "Customise group for the `timu-base' Library."
  :group 'timu)

(defcustom timu-base-path
  (expand-file-name "libraries/timu-base.el" user-emacs-directory)
  "Variable for the path of the module `timu-base'."
  :type 'file
  :group 'timu-base)

(defcustom timu-base-gdm-session (shell-command-to-string "printf $GDMSESSION")
  "Variable to store the Desktop environment on GNU/Linux."
  :type 'string
  :group 'timu-base)

(defcustom timu-base-wsl-string nil
  "Variable to store the string of the Hypervisor.
This useful in case we are on a wsl GNU/Linux distro."
  :type 'string
  :group 'timu-base)

(defcustom timu-base-wsl-p nil
  "Variable to store the info whether we are in a wsl GNU/Linux distro."
  :type 'boolean
  :group 'timu-base)

(defcustom timu-base-interactive-functions nil
  "Variable to store all custom interactive functions in the configuration."
  :type '(repeat symbol)
  :group 'timu-base)

(defcustom timu-base-non-interactive-functions nil
  "Variable to store all custom non interactive functions in the configuration."
  :type '(repeat symbol)
  :group 'timu-base)


;;; FUNCTIONS
(defmacro timu-base-setv (&rest args)
  "Handle ARGS like `setq' using `customize-set-variable'.
Credit: https://www.reddit.com/r/emacs/comments/qg8tga/comment/hi4xp8d."
  (let (body)
    (while args
      (let* ((var (pop args)) (val (pop args)))
        (push `(customize-set-variable ',var ,val) body)))
    (macroexp-progn (nreverse body))))

(defun timu-base-packages-refresh-on-friday ()
  "`package-refresh-contents' if today is Friday."
  (let ((day (format-time-string "%A")))
    (if (string= day "Friday")
        (package-refresh-contents t)
      (message "no"))))

(defun timu-base-yes-or-no-p (prompt)
  "Ask user a yes-or-no question using a `completing-read' PROMPT.
Credit: https://github.com/DarwinAwardWinner/ido-yes-or-no/tree/master."
  (let* ((yes-or-no-prompt (concat prompt " "))
         (choices '("yes" "no"))
         (answer (completing-read
                  yes-or-no-prompt choices nil 'require-match)))
    (while (string= answer "")
      (message "Please answer yes or no.")
      (setq answer (completing-read
                    (concat "Please answer yes or no: "
                            yes-or-no-prompt)
                    choices nil 'require-match)))
    (string= answer "yes")))

(defadvice yes-or-no-p (around use-ido activate)
  "Advice every `yes-or-no-p' prompt to use completion."
  (setq ad-return-value (timu-base-yes-or-no-p prompt)))

(defadvice y-or-n-p (around use-ido activate)
  "Advice every `y-or-n-p' prompt to use completion."
  (setq ad-return-value (timu-base-yes-or-no-p prompt)))

(defun timu-base-disable-yes-or-no-p (orig-fun &rest args)
  "Advice to answer yes or no automatically in Emacs.
Use around any ORIG-FUN to \"skip\" `yes-or-no-p' and `y-or-n-p' with ARGS t.
Credit: https://stackoverflow.com/a/35263420."
  (advice-add 'yes-or-no-p :around (lambda (&rest _) t))
  (advice-add 'y-or-n-p :around (lambda (&rest _) t))
  (unwind-protect
      (apply orig-fun args)
    (advice-remove 'yes-or-no-p (lambda (&rest _) t))
    (advice-remove 'y-or-n-p (lambda (&rest _) t))))

(defun timu-base-async-shell-command-no-window (command)
  "Do not display the `async-shell-command' COMMAND output buffer.
Credit: https://stackoverflow.com/a/60333836
Credit: https://stackoverflow.com/a/47910509."
  (interactive)
  (let ((display-buffer-alist
         (list (cons
                "\\*Async Shell Command\\*.*"
                (cons #'display-buffer-no-window nil)))))
    (async-shell-command command)))

(defun timu-base-visit-emacs-init ()
  "Load `init.el' file into a buffer."
  (interactive)
  (find-file user-init-file))

(defun timu-base-visit-emacs-early-init ()
  "Load `early-init.el' file into a buffer."
  (interactive)
  (find-file early-init-file))

(defun timu-base-load-init-files ()
  "Load `early-init.el' & `init.el' file."
  (interactive)
  (progn
    (load-file early-init-file)
    (load-file user-init-file)))

(defun timu-base-visit-libraries-directory ()
  "Load the directory with custom libraries."
  (interactive)
  (dired (expand-file-name "libraries" user-emacs-directory)))

(defun timu-base-empty-trash ()
  "Empty macOS Trash using the trash command."
  (interactive)
  (timu-base-async-shell-command-no-window "trash -ey"))

(defun timu-base-shell-open-dir ()
  "Open current directory at point with shell command \"open\".
This will open \"Finder.app\" at current location."
  (interactive)
  (timu-base-async-shell-command-no-window (concat "open " default-directory)))


;;;; swap ctrl & alt on windows & linux
(defun timu-base-make-key-string (modsymbol basic-event)
  "Convert the combination of MODSYMBOL and BASIC-EVENT.
BASIC-EVENT can be a character or a function-key symbol.  The
return value can be used with `define-key'.
Credit: https://gist.github.com/mmarshall540/8db9b5bac8dc5670cb9323e387de1317"
  (vector (event-convert-list `(,modsymbol ,basic-event))))

(pcase system-type
  ((or'windows-nt 'gnu/linux)
   ;; Escaped chars are:
   ;; tab return space del backspace (typically translated to del)
   (dolist
       (char
        (append
         '(up down left right menu print scroll pause
              insert delete home end prior next
              tab return space backspace escape
              f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12)
         ;; Escape gets translated to `C-\[' in `local-function-key-map'
         ;; We want that to keep working, so we don't swap `C-\[' with `M-\['.
         (remq ?\[ (number-sequence 33 126))))
     ;; Changing this to use `input-decode-map', as it works for more keys.
     (define-key input-decode-map
                 (timu-base-make-key-string 'control char)
                 (timu-base-make-key-string 'meta char))
     (define-key input-decode-map
                 (timu-base-make-key-string 'meta char)
                 (timu-base-make-key-string 'control char)))))

(defun timu-base-get-random-quote ()
  "Get a random quote from a file.
The file is `~/.emacs.d/local/quotes.json'.
Return the quote and the author as a string."
  (interactive)
  (let* ((json-object-type 'alist)
         (json-array-type 'list)
         (json-false nil)
         (json-null nil)
         (json-key-type 'string)
         (quotes (json-read-file "~/.emacs.d/local/quotes.json"))
         (quote (elt quotes (random (length quotes))))
         (text (cdr (assoc "text" quote)))
         (author (cdr (assoc "author" quote))))
    (format ";; %s\n;; -- %s\n" (upcase text) author)))


;;; AFTER INIT HOOKS
(add-hook 'after-init-hook #'timu-base-packages-refresh-on-friday)


;;; OS KEY CONFIGURATION
(pcase system-type
  ('darwin
   ;; keyboard modifiers
   (customize-set-variable 'mac-command-modifier 'meta)
   ;; emacs-plus or emacsmacport:
   (if
       (custom-variable-p 'mac-effective-appearance-change-hook)
       (customize-set-variable 'mac-function-modifier 'super)
     (customize-set-variable 'mac-option-modifier 'none))
   (customize-set-variable 'mac-control-modifier 'control)
   (customize-set-variable 'ns-function-modifier 'super)
   (customize-set-variable 'ns-pop-up-frames nil))
  ;; windows keys
  ('windows-nt
   (setq w32-pass-lwindow-to-system nil)
   (setq w32-lwindow-modifier 'super)))


;;; SERVER MODE
(unless (server-running-p)
  (server-start))


;;; ENVIRONMENT
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (exec-path-from-shell-initialize)
   (exec-path-from-shell-copy-envs '("PATH" "LC_ALL" "SHELL" "SSH_AUTH_SOCK"))))


;;; VARIABLES
(customize-set-variable 'timu-base-interactive-functions
                        (cl-remove-if-not
                         (lambda (symbol)
                           (and (fboundp symbol)
                                (commandp symbol)
                                (string-prefix-p "timu-" (symbol-name symbol))))
                         (mapcar #'intern (all-completions "timu-" obarray))))

(customize-set-variable 'timu-base-non-interactive-functions
                        (cl-remove-if-not
                         (lambda (symbol)
                           (and (fboundp symbol)
                                (not (commandp symbol))
                                (string-prefix-p "timu-" (symbol-name symbol))
                                (not (string-suffix-p "-map" (symbol-name symbol)))))
                         (mapcar #'intern (all-completions "timu-" obarray))))

(customize-set-variable 'timu-base-wsl-string
                        (shell-command-to-string
                         "printf $(lscpu | grep Hypervisor | awk '{print $3}')"))

(if (equal "Microsoft" timu-base-wsl-string)
    (customize-set-variable 'timu-base-wsl-p t)
  (customize-set-variable 'timu-base-wsl-p nil))

(setq default-directory "~/")

(if (boundp 'use-short-answers)
    (customize-set-variable 'use-short-answers t)
  (advice-add 'yes-or-no-p :override #'y-or-n-p))

(customize-set-variable
 'bookmark-file
 (expand-file-name "local/bookmarks" user-emacs-directory))
(customize-set-variable
 'project-list-file
 (expand-file-name "local/projects" user-emacs-directory))
(customize-set-variable
 'tramp-persistency-file-name
 (expand-file-name "local/tramp" user-emacs-directory))
(customize-set-variable
 'transient-history-file
 (expand-file-name "local/history.el" user-emacs-directory))
(customize-set-variable
 'url-configuration-directory
 (expand-file-name "local/url/" user-emacs-directory))
(customize-set-variable
 'url-cache-directory
 (expand-file-name "local/url/cache" user-emacs-directory))
(customize-set-variable
 'org-persist-directory
 (expand-file-name "local/org-persist/" user-emacs-directory))

(customize-set-variable 'global-auto-revert-non-file-buffers t)
(customize-set-variable 'load-prefer-newer t)
(customize-set-variable 'confirm-kill-processes nil)
(customize-set-variable 'kill-do-not-save-duplicates t)
(customize-set-variable 'tab-always-indent 'complete)
(customize-set-variable 'display-fill-column-indicator-column 79)
(customize-set-variable 'warning-minimum-level :error)
(customize-set-variable 'help-window-select t)
(customize-set-variable 'display-buffer-base-action
                        '((display-buffer-in-previous-window
                           display-buffer-reuse-window
                           display-buffer-use-some-window
                           display-buffer--maybe-pop-up-frame-or-window
                           display-buffer-in-side-window
                           display-buffer-pop-up-frame)))

(put 'narrow-to-region 'disabled nil)


;;; RECENT FILES
(customize-set-variable 'recentf-save-file
                        (expand-file-name "local/recentf" user-emacs-directory))
(customize-set-variable 'recentf-max-saved-items 20)
(customize-set-variable 'recentf-exclude
                        (list "COMMIT_EDITMSG"
                              "~$"
                              "/tmp/"
                              "/ssh:"
                              "/sudo:"
                              "/scp:"
                              ;; binary
                              "\\.mkv$"
                              "\\.mp[34]$"
                              "\\.avi$"
                              "\\.pdf$"
                              "\\.docx?$"
                              "\\.xlsx?$"))


;;; BACKUPS AND AUTO-SAVES
;; store all backup and auto-save files in the tmp dir
(customize-set-variable 'backup-directory-alist
                        `((".*" . ,temporary-file-directory)))

(customize-set-variable 'auto-save-file-name-transforms
                        `((".*" ,temporary-file-directory t)))

(customize-set-variable
 'auto-save-list-file-prefix
 (expand-file-name "emacs-auto-save-list/.saves-" temporary-file-directory))


;;; UNDO-TREE
(global-undo-tree-mode)
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (customize-set-variable 'undo-tree-history-directory-alist
                           `(("." . ,temporary-file-directory))))
  ('windows-nt
   (customize-set-variable
    'undo-tree-history-directory-alist
    '(("." . "~/.emacs.d/undo-tree-history")))))


;;; AUTH-SOURCES
(add-to-list 'auth-sources "~/projects/pdotemacs/emacs-authinfo.gpg")


;;; MY INFO
(customize-set-variable 'user-full-name "Aimé Bertrand")
(customize-set-variable 'user-mail-address "aime.bertrand@macowners.club")


;;; MACOS TRASH
(when (eq system-type 'darwin)
  (osx-trash-setup))
(customize-set-variable 'delete-by-moving-to-trash t)


;;; GENERAL COMMAND & FUNCTION KEYBINDINGS
(keymap-global-set "C-h f" #'helpful-callable)
(keymap-global-set "C-h v" #'helpful-variable)
(keymap-global-set "C-h k" #'helpful-key)
(keymap-global-set "M-B" #'ibuffer)
(keymap-global-set "M-g" #'magit-status)
(keymap-global-set "M-e" #'timu-mu4e-in-new-tab)
(keymap-global-set "M-E" #'timu-elfeed-load-db-and-open)
(keymap-global-set "M-d" #'dired)
(keymap-global-set "M-," #'timu-base-visit-emacs-init)
(keymap-global-set "M-;" #'timu-base-visit-emacs-early-init)
(keymap-global-set "M-v" #'yank)
(keymap-global-set "M-c" #'kill-ring-save)
(keymap-global-set "C-M-+" #'timu-modes-expenses-add-expense)
(keymap-global-set "<remap> <describe-syntax>" #'shortdoc-display-group)
(keymap-global-set "M-." #'keyboard-quit)
(keymap-global-set "A-M-<backspace>" #'timu-base-empty-trash)

(pcase system-type
  ('darwin
   (keymap-global-set "C-±" #'text-scale-increase)
   (keymap-global-set "C-–" #'text-scale-decrease)
   (keymap-global-set "C-≠" (lambda () (interactive) (text-scale-adjust 0)))
   (keymap-global-set "C-∂" #'timu-dired-shell-open-dir)
   (keymap-global-set "M-√" #'vterm)
   (keymap-global-set "C-√" #'multi-vterm)
   (keymap-set evil-normal-state-map "√" #'eshell)
   (keymap-global-set "M-€" #'embark-dwim)
   (keymap-set evil-normal-state-map "∞" #'timu-base-visit-libraries-directory)
   (keymap-global-set "M-«" #'read-only-mode)
   (keymap-global-set "C-M-∞" #'timu-base-load-init-files)
   (keymap-set evil-normal-state-map "∂" #'osx-dictionary-search-pointer))
  ((or 'gnu/linux 'windows-nt)
   (keymap-global-set "C-s-+" #'text-scale-increase)
   (keymap-global-set "C-s--" #'text-scale-decrease)
   (keymap-global-set "C-s-0" (lambda () (interactive) (text-scale-adjust 0)))
   (keymap-global-set "C-s-d" #'timu-dired-shell-open-dir)
   (keymap-global-set "C-s-v" #'vterm)
   (keymap-global-set "M-s-v" #'multi-vterm)
   (keymap-global-set "s-v" #'eshell)
   (keymap-global-set "s-e" #'embark-act)
   (keymap-set evil-normal-state-map "s-," #'timu-base-visit-libraries-directory)
   (keymap-global-set "C-s-e" #'embark-dwim)
   (keymap-global-set "M-e" #'timu-mu4e-in-new-tab)
   (keymap-global-set "C-M-s-," #'timu-base-load-init-files)
   (keymap-global-set "C-s-q" #'read-only-mode)))


;;; TRANSIENTS FOR GENERAL GLOBAL COMMANDS
(transient-define-prefix timu-base-main-transient ()
  "Main menu for general global commands."
  ["SECTIONS\n"
   [("a" "APPS" timu-base-apps-transient)]
   [("b" "BUFFERS" timu-base-buffers-transient)]
   [("c" "CONFIG" timu-base-config-transient)]
   [("f" "FILES" timu-base-files-transient)]
   [("g" "GENERAL" timu-base-general-transient)]
   [("h" "HELP" timu-base-help-transient)]
   [("n" "NAVIGATION" timu-base-navigation-transient)]
   [("o" "ORG" timu-base-org-transient)]
   [("r" "ORG ROAM" timu-base-org-roam-transient)]
   [("t" "TERMINALS" timu-base-terminals-transient)]
   [("w" "WINDOWS & FRAMES" timu-base-frames-windows-transient)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-apps-transient ()
  "Apps related commands."
  ["APPS\n"
   [("d" "Open Dired" dired)]
   [("e" "Open Elfeed" timu-elfeed-load-db-and-open)]
   [("i" "Open Ibuffer" ibuffer)]
   [("m" "Open Magit" magit-status)]
   [("M" "Open Mu4e" timu-mu4e-in-new-tab)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-buffers-transient ()
  "Buffer related commands."
  ["BUFFERS\n"
   [("e" "Evaluate buffer" eval-buffer)]
   [("n" "New buffer" evil-buffer-new)]
   [("r" "Revert Buffer" timu-edit-revert-buffer)]
   [("w" "Close Buffer" kill-this-buffer)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-config-transient ()
  "Config related commands."
  ["CONFIG\n"
   [("e" "Go to early init file" timu-base-visit-emacs-early-init)]
   [("i" "Go to init file" timu-base-visit-emacs-init)]
   [("l" "Go to my libraries" timu-base-visit-libraries-directory)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-files-transient ()
  "File related commands."
  ["FILES\n"
   [("o" "Open file" find-file)]
   [("s" "Save file" save-buffer)]
   [("S" "Save file as" write-file)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-general-transient ()
  "General miscellaneous commands."
  ["COMMANDS\n"
   [("e" "Embark Act" embark-act)]
   [("d" "Embark Act" embark-dwim)]
   [("r"  "Read only mode" read-only-mode)]
   [("t" "Switch theme" timu-ui-load-theme)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-help-transient ()
  "Help related commands."
  ["HELP\n"
   [("b" "Show Bindings for mode" describe-bindings)]
   [("f" "Show help for function" helpful-callable)]
   [("i" "Display Info window" info)]
   [("k" "Show help for key-sequence" helpful-key)]
   [("p" "Show help for package" describe-package)]
   [("t" "Show help for a face" describe-face)]
   [("v" "Show help for variable" helpful-variable)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-navigation-transient ()
  "Navigation related commands."
  ["NAVIGATION\n"
   [("b" "Switch Buffer" consult-buffer)]
   [("f" "Find by Line" consult-line)]
   [("p" "Switch project" timu-nav-project-switch-project)]
   [("P" "Visit project file" project-find-file)]
   [("q" "Quit Emacs" save-buffers-kill-terminal)]
   [("t" "Switch to the next tab" timu-nav-switch-or-new-tab)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-org-transient ()
  "Org related commands."
  ["ORG\n"
   [("a" "Open Org Agenda" org-agenda)]
   [("c" "Open Org Capture" org-capture)]
   [("e" "Toggle emphasis markers visibility" timu-org-toggle-emphasis-markers)]
   [("i" "Toggle image visibility" org-toggle-inline-images)]
   [("l" "Toggle link visibility" org-toggle-link-display)]
   [("L" "Insert Link" timu-org-refile-subtree-to-file)]
   [("r" "Refile Subtree to file" timu-org-refile-subtree-to-file)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-org-roam-transient ()
  "Org Roam related commands."
  ["ORG ROAM\n"
   [("c" "Org Roam Capture" org-roam-capture)]
   [("f" "Find node" org-roam-node-find)]
   [("i" "Insert node" org-roam-node-insert)]
   [("r" "Toggle Org Roam buffer" org-roam-buffer-toggle)]
   [("t" "Find node by Tag" timu-roam-find-by-tag)]]
  ["CMD"
   [("<escape>" "Quit Transient\n" transient-quit-one)]])

(transient-define-prefix timu-base-terminals-transient ()
  "Terminal related commands."
  ["TERMINALS\n"
   [("e" "Open Eshell" eshell)]
   [("m" "Open Multi-vterm" multi-vterm)]
   [("v" "Open vterm" vterm)]]
  ["CMD"
   ("<escape>" "Quit Transient\n" transient-quit-one)])

(transient-define-prefix timu-base-frames-windows-transient ()
  "Frames and windows related commands."
  ["WINDOWS & FRAMES\n"
   ["SPLITTING"
    ("Ä" "Toggle split direction" timu-nav-toggle-split-direction)
    ("ä" "Split to the right and follow" timu-nav-split-and-follow-right)
    ("ö" "Split bellow and follow" timu-nav-split-and-follow-below)
    ("s" "Swap windows" ace-swap-window)]
   ["CLOSING"
    ("0" "Delete current window" delete-window)
    ("1" "Delete other window" delete-other-windows)
    ("2" "Delete a window by selection" ace-delete-window)]
   ["SELECTING"
    ("<left>" "Go to left window" windmove-left)
    ("<right>" "Go to right window" windmove-right)
    ("<up>" "Go to window above" windmove-up)
    ("<down>" "Go to window bellow" windmove-down)]
   ["RESIZING"
    ("." "Enlarge window horizontally" enlarge-window-horizontally)
    ("," "Shrink window horizontally" shrink-window-horizontally)
    ("+" "Enlarge window vertically" enlarge-window)
    ("–" "Shrink window vertically" shrink-window)
    ("=" "Balance window sizes" balance-windows)]]
  ["CMD"
   ("<escape>" "Quit Transient\n" transient-quit-one)])

(keymap-global-set "M-#" #'timu-base-main-transient)
(keymap-global-set "M-'" #'timu-base-main-transient)
(keymap-set transient-map "<escape>" #'transient-quit-one)


(provide 'timu-base)

;;; timu-base.el ends here
