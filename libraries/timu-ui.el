;;; timu-ui.el --- Config for the look and feel -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: tools ui ux look feel
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the customization of the Emacs interface.

;;; Code:


(require 'mixed-pitch)


;;; DEFAULTS
(defgroup timu-ui ()
  "Customise group for the `timu-ui' Library."
  :group 'timu)

(defcustom timu-ui-path
  (expand-file-name "libraries/timu-ui.el" user-emacs-directory)
  "Variable for the path of the module `timu-ui'."
  :type 'file
  :group 'timu-ui)

(defcustom timu-ui-custom-dark-theme nil
  "Variable dark theme to be used in to store the theme I want to use.
Used in `timu-ui-theme-hook'."
  :type 'symbol
  :group 'timu-ui)

(defcustom timu-ui-custom-light-theme nil
  "Variable light theme to be used in to store the theme I want to use.
Used in `timu-ui-theme-hook'."
  :type 'symbol
  :group 'timu-ui)


;;; FUNCTIONS
(defun timu-ui-with-face (str &rest face-plist)
  "Propertize STR with FACE-PLIST."
  (propertize str 'face face-plist))

(defun timu-ui-toggle-face-size ()
  "Toggle the height of the default face."
  (interactive)
  (pcase timu-ui-face-height
    (140 (setq timu-ui-face-height 180))
    (180 (setq timu-ui-face-height 140)))
  (set-face-attribute 'default nil :height timu-ui-face-height)
  (set-face-attribute 'fixed-pitch nil :height timu-ui-face-height)
  (set-face-attribute 'variable-pitch nil :height timu-ui-face-height))

(defun timu-ui-load-theme ()
  "`load-theme' without confirmation and with completion.
Disables the `custom-enabled-themes' first to start with a blank canvas."
  (interactive)
  (let ((next-theme
         (completing-read "Load custom theme: "
                          (custom-available-themes))))
    (mapc #'disable-theme custom-enabled-themes)
    (load-theme (intern next-theme) t)))

(pcase system-type
  ('darwin
   (defun timu-ui-theme-hook (&optional appearance)
     "Hook to change the theme to light or dark.

Check the system's APPEARANCE in `ns-system-appearance'.
Or get by getting the APPEARANCE value of (`mac-application-state').

Set the variable `timu-macos-flavour' to \"light\" or \"dark\" accordingly.
This is for the case the `timu-macos-theme' is used.

Disable all `custom-enabled-themes' and load the theme in:
either `timu-ui-custom-light-theme' or `timu-ui-custom-dark-theme'."
     (let* ((is-light (if (boundp 'ns-system-appearance-change-functions)
                          (eq appearance 'light)
                        (equal (plist-get (mac-application-state) :appearance)
                               "NSAppearanceNameAqua"))))
       (customize-set-variable 'timu-macos-flavour (if is-light "light" "dark"))
       (mapc #'disable-theme custom-enabled-themes)
       (load-theme (if is-light timu-ui-custom-light-theme timu-ui-custom-dark-theme) t)))

   (defun timu-ui-load-theme-hook ()
     "Function to add `timu-ui-theme-hook' as a hook.
Change theme with `ns-system-appearance-change-functions' for emacs-plus.
Or change theme with `mac-effective-appearance-change-hook' for emacsmacport."
     (if (boundp 'ns-system-appearance-change-functions)
         (add-to-list 'ns-system-appearance-change-functions 'timu-ui-theme-hook)
       (add-hook 'mac-effective-appearance-change-hook #'timu-ui-theme-hook)
       (add-hook 'after-init-hook #'timu-ui-theme-hook)))))

;;;; mode line
(defun timu-ui-flash-mode-line ()
  "Flash the modeline on error or warning instead of the bell."
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil #'invert-face 'mode-line))


;;; LOOK AND FEEL
;;;; fullscreen for linux
(pcase system-type ('gnu/linux (add-hook 'after-init-hook #'toggle-frame-maximized)))

;;;; no header line for sway
(pcase timu-base-gdm-session
  ("sway" (add-hook 'after-init-hook
                    (lambda ()
                      (setq-default header-line-format "")))))

;;;; do not just resize frame
(customize-set-variable 'frame-inhibit-implied-resize t)


;;; THEME CONFIG
;;;; custom themes
(add-to-list 'custom-theme-load-path (expand-file-name "themes" user-emacs-directory))
(add-to-list 'custom-theme-load-path "~/projects/pdotemacs/themes")

(customize-set-variable 'timu-caribbean-mode-line-border "border")
(customize-set-variable 'timu-macos-mode-line-border-type "border")
(customize-set-variable 'timu-rouge-mode-line-border "border")
(customize-set-variable 'timu-spacegrey-mode-line-border "border")

;;;; dark & light theme for macos
;; emacs-plus or emacsmacport:
(pcase system-type
  ('darwin
   (customize-set-variable 'timu-ui-custom-dark-theme 'timu-macos)
   (customize-set-variable 'timu-ui-custom-light-theme 'timu-macos))
  ('gnu/linux
   (customize-set-variable 'timu-ui-custom-dark-theme 'timu-spacegrey)
   (customize-set-variable 'timu-ui-custom-light-theme 'timu-spacegrey)))

(pcase system-type
  ('darwin (if (or (boundp 'ns-system-appearance-change-functions)
                   (boundp 'mac-effective-appearance-change-hook))
               (timu-ui-load-theme-hook)
             (message "no custom theme function")))
  ((or 'gnu/linux 'windows-nt) (load-theme timu-ui-custom-dark-theme t)))

;;;; theme keybindings
(pcase system-type
  ('darwin
   (keymap-global-set "C-M-†" #'timu-ui-load-theme))
  ('gnu/linux
   (keymap-global-set "C-M-s-t" #'timu-ui-load-theme))
  ('windows-nt
   (keymap-global-set "C-M-s-t" #'timu-ui-load-theme)))


;;; SET FACES
(pcase system-type
  ('darwin (setq timu-ui-face-height-fixed 160)
           (setq timu-ui-face-height-variable 140))
  ('gnu/linux
   (if timu-base-wsl-p
       (setq timu-ui-face-height 110)
     (setq timu-ui-face-height 130)))
  ('windows-nt (setq timu-ui-face-height 110)))

(set-face-attribute 'default nil
                    :family "Iosevka Term"
                    :weight 'regular
                    :height timu-ui-face-height-fixed)

(set-face-attribute 'fixed-pitch nil
                    :family "Iosevka Term"
                    :weight 'regular
                    :height timu-ui-face-height-fixed)

(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Aile"
                    :weight 'medium
                    :height timu-ui-face-height-variable)

;;;; mixed-pitch-mode
;;(add-hook 'org-mode-hook #'mixed-pitch-mode)
(add-hook 'mu4e-view-mode-hook #'mixed-pitch-mode)

(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-from)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-name)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-content)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-subject)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-newsgroups)


;;; TIMU MODE LINE
(customize-set-variable 'timu-line-show-evil-state t)
(customize-set-variable 'timu-line-show-eglot-indicator t)
(customize-set-variable 'timu-line-show-lsp-indicator t)
(customize-set-variable 'timu-line-show-mu4e-index-update-indicator t)
(customize-set-variable 'timu-line-show-tramp-host t)
(customize-set-variable 'timu-line-show-wdired-keys t)

(timu-line-mode 1)


;;; WINDOWS BEHAVIOR
;;;; display buffer
(add-to-list 'display-buffer-alist
             '("\\*Messages\\*"
               (display-buffer-in-side-window)
               (side . bottom)
               (window-height . 0.30)))

(add-to-list 'display-buffer-alist
             '("\\(\\*Help\\*\\|\\*helpful .*\\|\\*info*\\)"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.4)))

;;;; prefer vertical split direction
(customize-set-variable 'split-height-threshold 1000)


;;; ALL-THE-ICONS-COMPLETION
(all-the-icons-completion-mode)
(add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)


;;; RAINBOW-DELIMITERS
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)


;;; OTHER MINOR CHANGES
;;;; alarm bell
(customize-set-variable 'visible-bell nil)
(customize-set-variable 'ring-bell-function 'timu-ui-flash-mode-line)


(provide 'timu-ui)

;;; timu-ui.el ends here
