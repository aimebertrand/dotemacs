;;; timu-dired.el --- Dired file manager config -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: tools filemanagement dired
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the customization of Dired.

;;; Code:


(require 'dired-filetype-face)
(require 'peep-dired)
(require 'wdired)


;;; DEFAULTS
(defgroup timu-dired ()
  "Customise group for the `timu-dired' Library."
  :group 'timu)

(defcustom timu-dired-path
  (expand-file-name "libraries/timu-dired.el" user-emacs-directory)
  "Variable for the path of the module `timu-dired'."
  :type 'file
  :group 'timu-dired)


;;; FUNCTIONS
(defun timu-dired-up-directory ()
  "Go up a directory in `dired'."
  (interactive)
  (find-alternate-file ".."))

(defun timu-dired-open-all-in-emacs ()
  "Open the current FILE or Dired marked files in Emacs."
  (interactive)
  (let ((file-list
           (dired-get-marked-files)))
    (mapc (lambda (file-path)
            (let ((process-connection-type nil))
              (start-process "" nil "emacsclient" file-path))) file-list)))

(defun timu-dired-open-in-external-app ()
  "Open the current FILE or Dired marked files in external app.
The app is chosen from your OS's preference.
Credit: http://xahlee.info/emacs/emacs/emacs_dired_open_file_in_ext_apps.html."
  (interactive)
  (let ((file-list (dired-get-marked-files)))
    (pcase system-type
      ('windows-nt
       (mapc (lambda (file-path)
               (w32-shell-execute
                "open"
                (replace-regexp-in-string "/" "\\" file-path t t))) file-list))
      ('darwin
       (mapc (lambda (file-path)
               (shell-command (format "open \"%s\"" file-path))) file-list))
      ('gnu/linux
       (mapc (lambda (file-path)
               (let ((process-connection-type nil))
                 (start-process "" nil "xdg-open" file-path))) file-list)))))

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set dired-mode-map "M-#" #'timu-dired-transient))
  ('windows-nt
   (keymap-set dired-mode-map "M-$" #'timu-dired-transient)))

(defun timu-dired-shell-quicklook ()
  "Open the files at point with shell command \"qlmanage\".
This will display a Quicklook of the file at point in macOS."
  (interactive)
  (let ((file (dired-get-file-for-visit)))
    (timu-base-async-shell-command-no-window
     (concat "qlmanage -p \"" file "\" > /dev/null 2>&1"))))

(defun timu-dired-copy-path-at-point ()
  "Copy the full path of the at `point' to the `kill-ring'.
Credit: https://emacs.stackexchange.com/a/36851/30874"
  (interactive)
  (dired-copy-filename-as-kill 0))

(defun timu-dired-search-and-enter ()
  "Search file or directory with `consult-line' and then visit it."
  (interactive)
  (consult-line)
  (dired-find-alternate-file))


;;; DIRED CONFIG
(customize-set-variable 'dired-recursive-copies 'always)
(customize-set-variable 'dired-isearch-filenames 'dwim)
(customize-set-variable 'dired-dwim-target t)
(customize-set-variable 'dired-recursive-deletes 'always)
(customize-set-variable 'dired-no-confirm t)
(customize-set-variable 'dired-do-revert-buffer t)
(customize-set-variable 'dired-listing-switches "-Ahlp")
(put 'dired-find-alternate-file 'disabled nil)
(setq insert-directory-program "ls")
(advice-add 'dired-do-delete
            :after #'(lambda ()
                       (interactive)
                       (timu-edit-revert-buffer)))
(advice-add 'dired-do-flagged-delete
            :after #'(lambda ()
                       (interactive)
                       (timu-edit-revert-buffer)))

(advice-add 'dired-do-flagged-delete :around #'timu-base-disable-yes-or-no-p)
(advice-add 'dired-do-delete :around #'timu-base-disable-yes-or-no-p)

(customize-set-variable 'wdired-allow-to-change-permissions t)
(customize-set-variable 'wdired-create-parent-directories t)

(add-hook 'dired-mode-hook #'hl-line-mode)
(add-hook 'dired-mode-hook #'dired-hide-details-mode)

(dired-async-mode 1)
(turn-on-gnus-dired-mode)

(when (string= system-type "darwin")
  (customize-set-variable 'dired-use-ls-dired nil))


;;; DIRED-ICONS
(add-hook 'dired-mode-hook #'treemacs-icons-dired-mode)


;;; DIRED-FILETYPE-FACE
;;(deffiletype-face-regexp omit3
;;                         :type-for-docstring hidden :regexp "^XXXXX")


;;; DIRED KEYBINDINGS
;;;; dired transients
(transient-define-prefix timu-dired-transient ()
  "Transient for DIRED BUFFERS."
  ["DIRED TRANSIENT COMMANDS\n"
   ["FILES"
    ("c" "Compress files(s) to" dired-do-compress-to) ; compress files into a separate files
    ("C" "Copy files(s)" dired-do-copy) ; compress files in place to gz
    ("D" "Delete files(s)" dired-do-delete)
    ("R" "Rename files(s)" dired-do-rename)
    ("S" "Symlink files(s)" dired-do-symlink)
    ("T" "Touch files(s)" dired-do-touch)
    ("Z" "Compress files(s)" dired-do-compress)]
   ["ACTIONS"
    ("+" "Create directory" dired-create-directory)
    ("a" "Attach to Mail" gnus-dired-attach)
    ("o" "Open with" timu-dired-open-in-external-app)
    ("ö" "Open marked" timu-dired-open-all-in-emacs)]
   ["PERMISSIONS"
    ("G" "Chmod" dired-do-chgrp)
    ("M" "Chmod" dired-do-chmod)
    ("O" "Chown" dired-do-chown)]
   ["MARKS"
    ("/" "Mark directories" dired-mark-directories)
    ("%" "Mark regexp" dired-mark-files-regexp)
    ("@" "Mark symlinks" dired-mark-symlinks)
    ("m" "Set mark" dired-mark)
    ("u" "Unset mark" dired-unmark)
    ("U" "Unset all mark" dired-unmark-all-marks)
    ("t" "Toggle mark" dired-toggle-marks)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])

(pcase system-type
  ((or 'darwin 'gnu/linux)
   (keymap-set dired-mode-map "M-#" #'timu-dired-transient))
  ('windows-nt
   (keymap-set dired-mode-map "M-$" #'timu-dired-transient)))

(evil-define-key 'normal dired-mode-map
  (kbd "/") #'dired-narrow
  (kbd "G") #'evil-goto-line
  (kbd "g g") #'evil-goto-first-line
  (kbd "v") #'evil-visual-char
  (kbd "<tab>") #'dired-subtree-cycle)

(keymap-set dired-mode-map "M-c" #'timu-dired-copy-path-at-point)
(keymap-set dired-mode-map "M-f" #'timu-dired-search-and-enter)
(keymap-set dired-mode-map "M-i" #'dired-show-file-type)
(keymap-set dired-mode-map "M-r" #'revert-buffer)
(keymap-set dired-mode-map "M-ƒ" #'timu-nav-consult-rg)
(keymap-set dired-mode-map "M-<up>" #'timu-dired-up-directory)
(keymap-set dired-mode-map "M-+" #'dired-create-directory)
(keymap-set dired-mode-map "<remap> <dired-next-line>" #'timu-dired-shell-quicklook)
(keymap-set dired-mode-map "<remap> <dired-sort-toggle-or-edit>" #'timu-dired-open-in-external-app)
(keymap-set dired-mode-map "<remap> <dired-prev-subdir>" #'popper-toggle)
(keymap-set dired-mode-map "<remap> <dired-find-file>" #'dired-find-alternate-file)

(pcase system-type
  ('darwin
   (keymap-set dired-mode-map "ø" #'timu-dired-open-all-in-emacs))
  ((or 'gnu/linux 'windows-nt)
   (keymap-set dired-mode-map "s-o" #'timu-dired-open-all-in-emacs)))

(keymap-set wdired-mode-map "M-s" #'wdired-finish-edit)
(keymap-set wdired-mode-map "M-w" #'wdired-abort-changes)

;;;; peep-dired
(evil-define-key 'normal dired-mode-map
  (kbd "p") #'peep-dired)

(keymap-set peep-dired-mode-map "<remap> <evil-previous-line>" #'peep-dired-prev-file)
(keymap-set peep-dired-mode-map "<remap> <evil-next-line>" #'peep-dired-next-file)

(provide 'timu-dired)

;;; timu-dired.el ends here
