;;; init.el --- Custom settings -*- lexical-binding: t -*-

;; Author: Aimé Bertrand
;; Version: 4.9
;; Package-Requires: ((emacs "28.1"))
;; Created: 2021-08-11
;; Keywords: emacs init
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2023 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; `init' to load my settings into Emacs.
;; This includes declarations of `package-archives', installation of the
;; packages and the loading of custom modules with `timu-bits-load'.

;;; Code:


(require 'package)
(require 'cl-lib)


;;; PATHS
(add-to-list 'load-path timu-libraries-path)
(add-to-list 'load-path (expand-file-name "local/packages" user-emacs-directory))
(add-to-list 'load-path "~/projects/pdotemacs/libraries")

(pcase system-type
  ('darwin
   (add-to-list
    'load-path
    (shell-command-to-string
     "printf ${$(which brew)%/bin/brew}/share/emacs/site-lisp/mu/mu4e")))
  ('gnu/linux
   (add-to-list
    'load-path "/home/linuxbrew/.linuxbrew/share/emacs/site-lisp/mu/mu4e")))

(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory))


;;; LOAD THE CONFIG
(toggle-debug-on-error)

;; run package installation
(require 'timu-bits)
(timu-bits-install-packages)

;; Because: This variable is used by `package-autoremove' to decide.
(customize-set-variable 'package-selected-packages timu-bits-package-list)

;; load custom libraries
(let ((default-directory user-emacs-directory)
      (file-name-handler-alist nil)
      (gc-cons-percentage .6)
      (gc-cons-threshold most-positive-fixnum)
      (read-process-output-max (* 1024 1024)))
  (timu-bits-load)
  (garbage-collect))

(toggle-debug-on-error)


;;; init.el ends here
